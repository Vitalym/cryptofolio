/**
 * Created by Виталий on 23.07.2017.
 */
import { createStore, combineReducers, applyMiddleware, compose  } from 'redux';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import rootReducer from '../reducers';
import thunk from 'redux-thunk';
//import { trackEvent } from '../middleware/tracker';
import {reducer as notificationsReducer} from 'reapop';
import { reducer as formReducer } from 'redux-form';
const history = createHistory();

const middleware = [
        //trackEvent,
    routerMiddleware(history),
    thunk
];

const enhancers = compose(
    applyMiddleware(
        ...middleware
    ),
    window.devToolsExtension && process.env.NODE_ENV === 'development' ? window.devToolsExtension() : f => f
);

export default function configureStore() {

    return createStore(
        combineReducers({
            notifications: notificationsReducer(),
            form: formReducer,
            rootReducer,
            router: routerReducer
        }),
        enhancers
    );
}