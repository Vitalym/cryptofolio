/**
 * Created by Виталий on 10.08.2017.
 */
import React from 'react';
import {Link} from 'react-router-dom';
import Button from 'react-toolbox/lib/button/Button';
import './RebalanceButton.css';

const RebalanceButton = props => {
    return (
        <div className="buttonContainer">
            <Link to="/portfolio/rebalance">
                <Button label='Rebalance portfolio' raised primary className="rebalanceButton actionButton"/>
            </Link>
        </div>
    );
};

export default RebalanceButton;