/**
 * Created by dev on 3.8.17.
 */
import React from 'react';
import './PortfolioDistribution.css';
import * as ReactHighcharts from 'react-highcharts';

const PortfolioDistribution = props => {
    const mobileDataLabels = {
        verticalAlign: 'top',
            enabled: true,
            color: '#000000',
            connectorWidth: 1,
            distance: -30,
            connectorColor: '#000000',
            formatter: function() {
            return Math.round(this.percentage) + ' %';
        }
    };

    const dataLabels = {
        enabled: true
    };
        const config = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Portfolio Distribution'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    //showInLegend: true,
                   /* dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: 'black'
                        }
                    }*/
                }
            },
            series: [{
                name: 'Share',
                colorByPoint: true,
                data: props.holdings.map(asset => {
                    return {
                        y: asset.totalUsdValue,
                        name: asset.coin.name,
                        dataLabels: props.isMobile ? mobileDataLabels : dataLabels
                    };
                })
            }]
        };


        return (
            <div data-flex data-layout="row" data-layout-align="center center">
                <ReactHighcharts config={config}/>
            </div>
        )
};

// Use connect to put them together
export default PortfolioDistribution;