/**
 * Created by Виталий on 10.08.2017.
 */
import React, { Component } from 'react'
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import Typography from 'material-ui/Typography';
import {getInvestment} from '../../../actions/investments';

//component to get transaction and pass it to child components / or redirect to 404
export function fetchTransaction(WrappedComponent) {

    class FetchTransactionComponent extends Component {

        componentWillMount () {
            this.props.getInvestment(this.props.match.params.id);

        }

        componentWillReceiveProps(nextProps) {

        }

        render() {

            const NotFound = (
                <div className="appContainer viewMessageContainer" data-flex data-layout="column" data-layout-align="center center">
                    <Helmet>
                        <title>Coin Overwatch | Not found</title>
                    </Helmet>
                    <Typography type="headline" className="messageHeading notFoundHeading">404</Typography>
                    <Typography type="subheading" className="messageSubHeading">We didn't find anything related to this coin in your history</Typography>
                </div>
            );

            return (
                <div>
                    {this.props.selectedTransaction
                        ? <WrappedComponent {...this.props}/>
                        : NotFound
                    }
                </div>
            )

        }
    }

    const mapStateToProps = (state) => ({
        selectedTransaction: state.rootReducer.investments.selectedTransaction
    });


    // Maps actions to props
    const mapDispatchToProps = (dispatch) => {
        return {
            // You can now say this.props.createBook
            getInvestment: (id) => dispatch(getInvestment(id))
        }
    };

    return connect(mapStateToProps, mapDispatchToProps)(FetchTransactionComponent);
}