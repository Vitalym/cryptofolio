/**
 * Created by dev on 1.9.17.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import HoldingMobileCard from './holdingMobileCard/HoldingMobileCard'
import SortingDropdown from '../../sortingDropdown/SortingDropdown';
import {sortHoldings} from '../../../actions/investments';
import './HoldingsMobile.css';

class HoldingsMobile extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);

        this.state = {
            selected: null
        };

        this.sortingOptions = [
            {value: 'coin.name', label: 'Currency'},
            {value: this.props.currency === 'USD' ? 'totalUsdValue' : 'totalBtcValue', label: 'Amount'},
            {value: this.props.currency === 'USD' ? 'totalUsdSpent' : 'totalBtcSpent', label: 'Acquisition cost'},
            {value: this.props.currency === 'USD' ? 'currentPriceUsd' : 'currentPriceBtc', label: 'Current price'},
            {value: 'day_change', label: '24h change'},
            {value: this.props.currency === 'USD' ? 'totalUsdProfitPercentage' : 'totalBtcProfitPercentage', label: 'Profit / loss'},
        ]
    }

    handleClick (event, selected) {
        this.state.selected === selected ? this.setState( { selected: null }) : this.setState( { selected });

    };

    handleRequestSort (orderBy) {
        //props.sortHoldings({orderBy});
    };

    handleSort (orderBy) {
        this.props.sortHoldings({orderBy})
    }



    render() {
        return (
            <div>
                <SortingDropdown onChange={(orderBy) => {this.props.sortHoldings({orderBy})}} options={this.sortingOptions} value={this.props.sorting.orderBy}/>
                <div className="tableContainer">
                    <div className="mobileCardHeader" data-flex data-layout="row">
                        <div className="mobileCardCell" data-flex="35" data-layout="column" data-layout-align="center start">
                            Currency
                        </div>
                        <div className="mobileCardCell" data-flex="35" data-layout="column" data-layout-align="center center">
                            Amount
                        </div>
                        <div className="mobileCardCell" data-flex="25" data-layout="column" data-layout-align="center end">
                            Profit
                        </div>
                    </div>
                    {this.props.holdings.map(asset => {
                        return (
                            <div data-flex="100" data-layout="column"  key={asset._id} className="mobileCardContainer"  onClick={event => this.handleClick(event, asset.coin.id)}>
                                <HoldingMobileCard asset={asset}
                                                   currency={this.props.currency}
                                                   selected={this.state.selected}/>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    holdings: state.rootReducer.investments.holdings,
    sorting: state.rootReducer.investments.holdingsSorting,
    isSorting: state.rootReducer.investments.isSorting,
    user: state.rootReducer.auth.user
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    sortHoldings: payload => dispatch(sortHoldings(payload)),
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HoldingsMobile));