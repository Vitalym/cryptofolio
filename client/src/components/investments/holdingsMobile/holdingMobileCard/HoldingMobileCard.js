/**
 * Created by dev on 1.9.17.
 */
import React from 'react';
import TableTrendIcon from '../../../tableTrendIcon/TableTrendIcon';
import Button from 'material-ui/Button';
import './HoldingMobileCard.css';
import history from './../../../../history';

const HoldingMobileCard = props => {

    const handleClick = (event, name) => {
        //history.push(`/portfolio/history/${name}`);
    };

    const selected = props.selected;
    const asset = props.asset;
    const currency = props.currency || 'USD';
    const currencySymbol = (currency === 'USD') ? '$' : 'B';
    let imgUrl;

    //build image url
    try {
        imgUrl = require(`../../../../assets/coins/${asset.coin.id}.png`);
    } catch (e) {
        imgUrl = require(`../../../../assets/coins/question-icon.png`);
    }

    const details = [
        {
            title: 'Acquisition cost',
            data: currency === 'USD' ? currencySymbol + +asset.totalUsdSpent.toFixed(2) : +asset.totalBtcSpent.toFixed(4) + currencySymbol,
            className: 'coinDetailsData'
        },
        {
            title: 'Average price per coin',
            data: currency === 'USD' ? currencySymbol + +asset.averageUsdAcquisitionCost.toFixed(2) : +asset.averageBtcAcquisitionCost.toFixed(4) + currencySymbol,
            className: 'coinDetailsData'
        },
        {
            title: 'Current Price',
            data: currency === 'USD' ? currencySymbol + +asset.currentPriceUsd : asset.currentPriceBtc + currencySymbol,
            className: 'coinDetailsData'
        },
        {
            title: 'Day Change %',
            data: asset.day_change,
            className: Number(asset.day_change) > 0 ? 'positive coinDetailsData' : 'negative coinDetailsData',
            trendIcon: true,
            trend: asset.day_change
        }
    ];

    const coinDetails = (
        <div className="coinDetailsContainer">
            {details.map(info => {
                return (
                    <div className="coinDetails" data-flex data-layout="row" data-layout-align="space-between">
                        <div className="coinDetailsHeading">
                            {info.title}
                        </div>
                        <div className={info.className}>
                            {info.data}
                            {info.trendIcon ? <TableTrendIcon trend={info.trend}/> : null}
                        </div>
                    </div>
                )
            })}
            <div className="coinDetailsAction" data-flex data-layout="row" data-layout-align="center center">
                <Button raised color="primary"
                        href={"/portfolio/history/" + asset.coin.id}
                        className='actionButton'>
                    View history
                </Button>
            </div>
        </div>
    );

    return (
        <div>
            <div className="mobileCard" data-flex data-layout="row"
                 onClick={event => handleClick(event, asset.coin.id)}>
                <div className="mobileCardCell" data-flex="35">
                    <div className="tablePrimaryData fullHeight" data-flex data-layout="row" data-layout-align="start center">
                        <img src={imgUrl} alt={asset.coin.name} className="coinThumb"/>
                        {asset.coin.name}
                    </div>
                </div>
                <div className="mobileCardCell" data-flex="40" data-layout="column" data-layout-align="center center">
                    <div className="tablePrimaryData">
                        {+asset.totalAmount.toFixed(4)} {asset.coin.symbol}
                    </div>
                    <div className="tableSecondaryData">
                        {currency === 'USD' ? currencySymbol + +asset.totalUsdValue.toFixed(2) : +asset.totalBtcValue.toFixed(4) + currencySymbol}
                    </div>
                </div>
                <div className="mobileCardCell" data-flex="25" data-layout="column" data-layout-align="center end">
                        <div className={Number(asset.totalUsdProfitPercentage) > 0 ? 'positive tablePrimaryData' : 'negative tablePrimaryData'}>
                            {currency === 'USD' ? Number(asset.totalUsdProfitPercentage).toFixed(2) : Number(asset.totalBtcProfitPercentage).toFixed(2)}
                            % &nbsp;
                            <TableTrendIcon trend={asset.totalUsdProfitPercentage}/>
                        </div>
                        <div className="tableSecondaryData">
                            {currency === 'USD' ? currencySymbol + +asset.totalProfitUSD.toFixed(2) : +asset.totalProfitBTC.toFixed(4) + currencySymbol}
                        </div>

                </div>
            </div>
            {selected === asset.coin.id ? coinDetails : null}
        </div>

    );

};

export default HoldingMobileCard;