/**
 * Created by dev on 3.8.17.
 */
import React from 'react';
import {GridTile} from 'material-ui/GridList';
import Card, {CardContent} from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import TableTrendIcon from '../../tableTrendIcon/TableTrendIcon';
import './PortfolioValue.css';

const PortfolioValue = props => {

    const currency = props.currency || 'USD';
    const stats = props.totalValue;
    const totalProfit = (currency === 'USD') ? '$' + +stats.totalUSDProfit.toFixed(2) : stats.totalBTCProfit.toFixed(4) + ' BTC';
    const totalProfitPercentage = (currency === 'USD') ? stats.totalUsdProfitPercentage : stats.totalBtcProfitPercentage;

    return (
        <div className="root">
            <div className="portfolioValue"
                 data-flex
                 data-layout='column'
                 data-layout-gt-md='column'
                 data-layout-align-lt-md='space-between center'
                 data-layout-gt-xs="row">
                <Card className='portfolioCard' data-flex="33" data-flex-gt-md="100" data-layout='column'>
                    <div className="cardHeaderContainer">
                        <Typography type="title">
                            Current value
                        </Typography>
                    </div>
                    <CardContent>

                        <div className="primaryData primaryColor">
                            {currency === 'USD' ? '$' + +stats.totalUSDValue.toFixed(2) : +stats.totalBTCValue.toFixed(4) + ' BTC'}
                        </div>
                        <div className="valueContainer primaryColor">
                            <div className="secondaryData">
                                {currency === 'USD' ? +stats.totalBTCValue.toFixed(2) + ' BTC' : '$' + +stats.totalUSDValue.toFixed(2)}
                            </div>
                            <div className="secondaryData">
                                {+props.totalValue.totalETHValue.toFixed(2)} ETH
                            </div>
                        </div>
                    </CardContent>
                </Card>

                <Card className='portfolioCard' data-flex="33" data-flex-gt-md="100" data-layout='column'>
                    <div className="cardHeaderContainer">
                        <Typography type="title">
                            Acquisition cost
                        </Typography>
                    </div>
                    <CardContent>
                        <div className="primaryData primaryColor">
                            {currency === 'USD' ? '$' + +stats.totalUSDSpent.toFixed(2) : +stats.totalBTCSpent.toFixed(6) + ' BTC'}
                        </div>
                        <div className="secondaryData">
                            {currency === 'USD' ? +stats.totalBTCSpent.toFixed(6) + ' BTC' : '$' + +stats.totalUSDSpent.toFixed(2)}
                        </div>
                    </CardContent>
                </Card>

                <Card className='portfolioCard' data-flex="33" data-flex-gt-md="100" data-layout='column'>
                    <div className="cardHeaderContainer">
                        <Typography type="title">
                            Total profit
                        </Typography>
                    </div>
                    <CardContent>

                        <div className="primaryData">
                            <div className={Number(totalProfitPercentage) > 0 ? 'positive' : 'negative'}>
                                {+totalProfitPercentage.toFixed(2)}%
                                <TableTrendIcon trend={totalProfitPercentage}/>
                            </div>
                        </div>
                        <div className="secondaryData">
                            {totalProfit}
                        </div>
                    </CardContent>
                </Card>

            </div>
        </div>
    );
};

export default PortfolioValue;