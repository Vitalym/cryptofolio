/**
 * Created by dev on 3.8.17.
 */
import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Table, {
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import TableLoadingIcon from '../../tableLoadingIcon/TableLoadingIcon';
import HoldingRow from './HoldingRow/HoldingRow';
import {sortHoldings} from '../../../actions/investments';
import './Holdings.css';

const Holdings = props => {

    const currency = props.currency;

    const handleRequestSort = (orderBy) => {
        props.sortHoldings({orderBy});
    };

    const tableHeadData = [
        {id: 'coin.name', numeric: false, disablePadding: false, label: 'Currency'},
        {
            id: currency === 'USD' ? 'totalUsdValue' : 'totalBtcValue',
            numeric: false,
            disablePadding: false,
            className: 'investmentsTableHead',
            label: 'Amount'
        },
        {
            id: currency === 'USD' ? 'totalUsdSpent' : 'totalBtcSpent',
            numeric: false,
            disablePadding: false,
            className: 'investmentsTableHead investmentsTableSpent',
            label: 'Acquisition cost'
        },
        {
            id: currency === 'USD' ? 'currentPriceUsd' : 'currentPriceBtc',
            numeric: false,
            disablePadding: false,
            className: 'investmentsTableHead investmentsTableCurrentPrice',
            label: 'Current price'
        },
        {
            id: 'day_change',
            numeric: false,
            disablePadding: false,
            className: 'investmentsTableHead investmentsTableDayChange',
            label: '24h change'
        },
        {
            id: currency === 'USD' ? 'totalUsdProfitPercentage' : 'totalBtcProfitPercentage',
            numeric: false,
            disablePadding: false,
            className: 'investmentsTableHead',
            label: 'Profit / loss'
        }
    ];
    return (
        <div className="tableContainer">
            <Table>
                <TableHead>
                    <TableRow>
                        {tableHeadData.map(column => {
                            return (
                                <TableCell
                                    className={column.className}
                                    key={column.id}
                                    numeric={column.numeric}
                                    disablePadding={column.disablePadding}>
                                    <TableSortLabel
                                        active={props.sorting.orderBy === column.id}
                                        direction={props.sorting.order}
                                        onClick={() => handleRequestSort(column.id)}>
                                        {column.label}
                                    </TableSortLabel>
                                </TableCell>
                            );
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.holdings.map(investment => {
                        return (
                            <HoldingRow asset={investment} key={investment._id} currency={currency}/>
                        );
                    })}
                </TableBody>
            </Table>
            {props.isSorting ? <TableLoadingIcon/> : null}
        </div>
    );

};

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    holdings: state.rootReducer.investments.holdings,
    sorting: state.rootReducer.investments.holdingsSorting,
    isSorting: state.rootReducer.investments.isSorting,
    user: state.rootReducer.auth.user
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    sortHoldings: payload => dispatch(sortHoldings(payload)),
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Holdings));