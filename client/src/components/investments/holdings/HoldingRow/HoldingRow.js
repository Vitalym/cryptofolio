/**
 * Created by dev on 10.8.17.
 */
import React from 'react';
import Table, {
    TableCell,
    TableRow
} from 'material-ui/Table';
import BitcoinSymbol from '../../../bitcoinSymbol/BitcoinSymbol';
import TableTrendIcon from '../../../tableTrendIcon/TableTrendIcon';
import history from './../../../../history';
import './HoldingRow.css';

const HoldingRow = props => {

    const handleClick = (event, name) => {
        history.push(`/portfolio/history/${name}`);
    };

    const asset = props.asset;
    const currency = props.currency || 'USD';
    const currencySymbol = (currency === 'USD') ? '$' : 'B';
    let imgUrl;

    //build image url
    try {
        imgUrl = require(`../../../../assets/coins/${asset.coin.id}.png`);
    } catch (e) {
        imgUrl = require(`../../../../assets/coins/question-icon.png`);
    }

    return (
        <TableRow hover key={asset._id} onClick={event => handleClick(event, asset.coin.id)}>
            <TableCell>
                <div className="tablePrimaryData" data-flex data-layout="row" data-layout-align="start center">
                    <img src={imgUrl} alt={asset.coin.name} className="coinThumb"/>
                    {asset.coin.name}
                </div>
            </TableCell>
            <TableCell>
                <div className="tablePrimaryData">
                    {+asset.totalAmount.toFixed(4)} {asset.coin.symbol}
                </div>
                <div className="tableSecondaryData">
                    {currency === 'USD' ? currencySymbol + +asset.totalUsdValue.toFixed(2) : +asset.totalBtcValue.toFixed(4)}
                </div>
            </TableCell>
            <TableCell className='investmentsTableSpent'>
                <div className="tablePrimaryData">
                    {currency === 'USD' ? currencySymbol + +asset.totalUsdSpent.toFixed(2) : +asset.totalBtcSpent.toFixed(4) + currencySymbol}
                </div>
                <p className="tableSecondaryData">
                    {currency === 'USD' ? currencySymbol + +asset.averageUsdAcquisitionCost.toFixed(2) : +asset.averageBtcAcquisitionCost.toFixed(4) + currencySymbol}
                    &nbsp; per coin
                </p>
            </TableCell>
            <TableCell className="investmentsTableCurrentPrice">
                <div className="tablePrimaryData investmentsTableDayChange">
                    {currency === 'USD' ? currencySymbol + parseFloat(Number(asset.currentPriceUsd).toFixed(7)) : parseFloat(Number(asset.currentPriceBtc).toFixed(7)) + currencySymbol}
                </div>

                <div className="investmentsTableDayChangeSmallScreen">
                        <div className={Number(asset.day_change) > 0 ? 'positive tablePrimaryData' : 'negative tablePrimaryData'}>
                            {currency === 'USD' ? currencySymbol + +asset.currentPriceUsd : asset.currentPriceBtc + currencySymbol}
                            &nbsp;<TableTrendIcon trend={asset.day_change}/>
                        </div>
                        <div className="tableSecondaryData">
                            {Number(asset.day_change)}%
                        </div>
                </div>
            </TableCell>
            <TableCell
                className={Number(asset.day_change) > 0 ? 'positive investmentsTableDayChange' : 'negative investmentsTableDayChange'}>
                        <p className="tablePrimaryData">
                            {Number(asset.day_change)}%
                            &nbsp; <TableTrendIcon trend={asset.day_change}/>
                        </p>
            </TableCell>
            <TableCell className={Number(asset.totalUsdProfitPercentage) > 0 ? 'positive' : 'negative'}>
                        <div className="tablePrimaryData">
                            {currency === 'USD' ? Number(asset.totalUsdProfitPercentage).toFixed(2) : Number(asset.totalBtcProfitPercentage).toFixed(2)} %
                            &nbsp; <TableTrendIcon trend={asset.totalUsdProfitPercentage}/>
                        </div>
                        <div className="tableSecondaryData">
                            {currency === 'USD' ? currencySymbol + +asset.totalProfitUSD.toFixed(2) : +asset.totalProfitBTC.toFixed(4) + currencySymbol}
                        </div>
            </TableCell>
        </TableRow>
    );
};

export default HoldingRow;
