/**
 * Created by Виталий on 01.08.2017.
 */
import React, {Component} from 'react'
import {connect} from 'react-redux';
import Typography from 'material-ui/Typography';
import LoadingScreen from '../../components/loadingScreen/LoadingScreen';
import PortfolioActionButton from '../../components/headingActionButton/HeadingActionButton';
import {getInvestments} from '../../actions/investments';

import '../../containers/investments/Investments.css';

export function investmentsComponent(WrappedComponent) {

    class InvestmentsComponent extends Component {

        componentWillMount() {
            this.props.getInvestments();
        }

        componentWillReceiveProps(nextProps) {

        }

        render() {

            if(this.props.isFetching){
                return (
                    <LoadingScreen/>
                )
            }
            
            const EmptyPortfolio = (
            <div className="appContainer viewMessageContainer" data-flex data-layout="column" data-layout-align="center center">
                <Typography type="headline" className="messageHeading">Your portfolio is empty</Typography>
                <PortfolioActionButton text='Add Transaction' color='accent' raised={true}/>
            </div>
            );
            return (
                <div>
                    {this.props.investments && this.props.investments.length
                        ? <WrappedComponent {...this.props}/>
                        : EmptyPortfolio
                    }
                </div>
            )

        }
    }

    const mapStateToProps = (state) => ({
        investments: state.rootReducer.investments.investments,
        isFetching: state.rootReducer.investments.isFetching
    });


    // Maps actions to props
    const mapDispatchToProps = (dispatch) => ({
        getInvestments: () => dispatch(getInvestments())
    });

    return connect(mapStateToProps, mapDispatchToProps)(InvestmentsComponent);

}