/**
 * Created by dev on 9.8.17.
 */
export default function(values) {
    const errors = {};
    const requiredFields = [
        'coin',
        'amount',
        'price',
        'priceType',
        'currency',
        'dateBought'
    ];
    const numericFields = [
        'amount',
        'price'
    ];
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required';
        }
    });
    numericFields.forEach(field => {
        if (!isNumber(values[field])) {
            errors[field] = 'This must be a number';
        }
    });
    if (values.note && values.note.length > 500) {
        errors.note = 'This field can be no longer than 500 characters';
    }
    return errors;
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}