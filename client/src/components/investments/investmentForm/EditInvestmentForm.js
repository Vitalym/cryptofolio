/**
 * Created by Ð’Ð¸Ñ‚Ð°Ð»Ð¸Ð¹ on 01.08.2017.
 */
import React from 'react';
import {reduxForm, Field, formValueSelector} from 'redux-form';
import { connect } from 'react-redux';
import Input from 'react-toolbox/lib/input/Input';
import Dropdown from 'react-toolbox/lib/dropdown/Dropdown';
import DatePicker from 'react-toolbox/lib/date_picker/DatePicker';
import Autocomplete from 'react-toolbox/lib/autocomplete/Autocomplete';
import Button from 'react-toolbox/lib/button/Button';
import Switch from 'material-ui/Switch';
import {FormControlLabel} from 'material-ui/Form';

import validate from './validate';

import './InvestmentForm.css';

const currencyOptions = [
    {value: 'USD', label: 'USD'},
    {value: 'BTC', label: 'BTC'},
];

const priceTypeOptions = [
    {value: 'per coin', label: 'per coin'},
    {value: 'total', label: 'total'}
];

const actionOptions = [
    {value: 'buy', label: 'buy'},
    {value: 'sell', label: 'sell'}
];

const renderTextField = ({ input: { onBlur, ...inputForm }, meta: { touched, error }, ...custom},) => (
    <Input
        error={touched && error}
        {...inputForm}
        {...custom}
    />
);

const renderDropdown = ({ input: { onBlur, ...inputForm }, meta: { touched, error }, source, ...custom}) => (
    <Dropdown
        error={touched && error}
        {...inputForm}
        {...custom}
        source={source}  />
);

const renderAutocomplete = ({ input: { onBlur, ...inputForm }, meta: { touched, error }, source, ...custom }) => (
    <Autocomplete
        error={touched && error}
        multiple={false}
        {...inputForm}
        {...custom}
        source={source}  />
);

const renderDatePicker = ({ input: { onBlur, ...inputForm }, meta: { touched, error }, label, ...custom }) => (
    <DatePicker
        error={touched && error}
        label={label}
        {...inputForm}
        {...custom}
    />
);

const renderSwitch = ({input: {onBlur, ...inputForm}, meta: {touched, error}, label, checked, ...custom}) => (
    <FormControlLabel
        control={
            <Switch
                checked={checked}
                {...inputForm}
                {...custom}/>
        }
        label={label}
    />
);

let EditInvestmentForm = props => {

    const {handleSubmit, coins, pristine, reset, submitting, action, coin, connectTransaction} = props;

    return (
        <form onSubmit={handleSubmit}>
            <div data-flex="100" className="formGroup">
                    <Field component={renderAutocomplete}
                           multiple={false}
                           name="coin"
                           source={coins.map(coin => (coin.label))}
                           label="Pick a coin"/>
            </div>
            <div data-flex data-layout="row" className="formGroup">
                <div className="formControl" data-flex="33">
                    <Field component={renderDropdown}
                           name="action"
                           source={actionOptions}
                           label="Action"/>
                </div>
                <div className="formControl" data-flex="33">
                    <Field name="amount"
                           type="text"
                           component={renderTextField}
                           label="Amount"/>
                </div>
                <div className="formControl" data-flex="33">
                    <Field name="price"
                           type="text"
                           component={renderTextField}
                           label="Price"/>
                </div>
            </div>
            {
                coin && coin !== 'Bitcoin (BTC)' ? <div data-flex="100" className="formGroup">
                    <Field name="connectTransaction"
                           checked={connectTransaction}
                           component={renderSwitch}
                           label={action === 'buy' ?
                               'Withdraw BTC from your holdings (select this options if you are buying with Bitcoins and use Bitcoins from your holdings)' :
                               'Add BTC to your holdings (select this option if you are selling for Bitcoins and you want to add Bitcoins to your holdings'}/>
                </div> : null
            }
            <div data-flex data-layout="row" className="formGroup">
                <div className="formControl" data-flex="33">
                    <Field component={renderDropdown}
                           name="currency"
                           source={currencyOptions}
                           label="Currency"/>
                </div>
                <div className="formControl" data-flex="33">
                    <Field component={renderDropdown}
                           name="priceType"
                           source={priceTypeOptions}
                           label="Price Type"/>
                </div>
                <div className="formControl" data-flex="33">
                    <Field type="text" name="dateBought" label="Date" component={renderDatePicker} />
                </div>
            </div>
            <div data-flex="100" className="formGroup">
                <Field name="note"
                       multiline
                       maxLength={500}
                       type="text"
                       component={renderTextField}
                       label="Note"/>
            </div>
            <div data-flex="100" className="formAction" data-layout="row" data-layout-align="end center">
                <Button type="submit" disabled={pristine || submitting} label='Save' raised primary />
            </div>
        </form>
    );
};

// Decorate with reduxForm(). It will read the initialValues prop provided by connect()
EditInvestmentForm = reduxForm({
    form: 'EditInvestmentForm',  // a unique identifier for this form
    enableReinitialize: true,
    validate
})(EditInvestmentForm);

const selector = formValueSelector('EditInvestmentForm');

EditInvestmentForm = connect(state => {
    const action = selector(state, 'action');
    const coin = selector(state, 'coin');
    const connectTransaction = selector(state, 'connectTransaction');
    return {
        initialValues: state.rootReducer.investments.selectedTransactionFormState,
        action,
        connectTransaction,
        coin
    }
})(EditInvestmentForm);


export default EditInvestmentForm;