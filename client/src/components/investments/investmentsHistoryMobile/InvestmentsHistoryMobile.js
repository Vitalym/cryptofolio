/**
 * Created by dev on 3.8.17.
 */
import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import InvestmentsHistoryMobileCard from './investmentHistoryMobileCard/InvestmentsHistoryMobileCard';
import {sortCoinTransactions} from '../../../actions/investments';
import './InvestmentsHistoryMobile.css';

const InvestmentsHistoryMobile = props => {

    const currency = props.user.settings.currency || 'USD';
    const isSmallScreen = props.isSmallScreen;

    const handleRequestSort = property => {
        const orderBy = property;
        props.sortCoinTransactions({orderBy});
    };

    return (
        <div className="tableContainer">
            <div className="mobileCardHeader" data-flex data-layout="row">
                <div className="mobileCardCell" data-flex={isSmallScreen ? 35: 25}>

                </div>
                <div className="mobileCardCell dateBoughtCell" data-flex="20">
                    Date
                </div>
                <div className="mobileCardCell" data-flex={isSmallScreen ? 30: 25}>
                    Amount
                </div>
                <div className="mobileCardCell" data-flex={isSmallScreen ? 30: 20}>
                    Price
                </div>
                <div className="mobileCardCell" data-flex="5">

                </div>
            </div>
            {props.investments.map(investment => {
                return (
                    <div data-flex="100" data-layout="column" key={investment._id} className="mobileCardContainer">
                        <InvestmentsHistoryMobileCard investment={investment} key={investment._id} currency={currency} isSmallScreen={props.isSmallScreen}/>
                    </div>
                );
            })}
        </div>
    );

};

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    user: state.rootReducer.auth.user,
    sorting: state.rootReducer.investments.coinTransactionSorting
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    sortCoinTransactions: payload => dispatch(sortCoinTransactions(payload))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(InvestmentsHistoryMobile));
