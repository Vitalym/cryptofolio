/**
 * Created by dev on 8.8.17.
 */
import React from 'react';
import * as moment from 'moment';
import Table, {
    TableCell,
    TableRow
} from 'material-ui/Table';
import TransactionMenu from '../../investmentsHistory/transactionMenu/TransactionMenu';
import './InvestmentsHistoryMobileCard.css';

const InvestmentsHistoryMobileCard = props => {

    const investment = props.investment;
    const isSmallScreen = props.isSmallScreen;
    const currency = props.currency || 'USD';
    const currencySymbol = (currency === 'USD') ? '$' : 'B';
    let imgUrl;

    //build image url
    try {
        imgUrl = require(`../../../../assets/coins/${investment.coin.id}.png`);
    } catch (e) {
        imgUrl = require(`../../../../assets/coins/question-icon.png`);
    }
    return (
        <div>
            <div className="mobileCard" data-flex data-layout="row">
                <div className="mobileCardCell" data-flex={isSmallScreen ? 35: 25}>
                    <div data-flex data-layout="row" data-layout-align="start center">
                        <div>
                            <img src={imgUrl} alt={investment.coin.name} className="coinThumb"/>
                        </div>
                        <div data-flex>
                            <div className="tablePrimaryData" >
                                {investment.coin.name}
                            </div>
                            <div className="tableSecondaryData">
                                {investment.action.toUpperCase()}
                            </div>
                            <div className="tableSecondaryData dateBoughtMobile">
                                {moment(investment.dateBought).format("D-MM-YYYY")}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mobileCardCell dateBoughtCell" data-flex="20">
                    <div className="transactionPrimaryData fullHeight" data-flex data-layout="row" data-layout-align="start center">
                        {moment(investment.dateBought).format("D-MM-YYYY")}
                    </div>
                </div>
                <div className="mobileCardCell" data-flex={isSmallScreen ? 30: 25} data-layout="column" data-layout-align="center center">
                    <div className="transactionPrimaryData">
                        {+investment.amount.toFixed(6)}
                    </div>
                    <div className="transactionSecondaryData pricePerItemMobile">
                        {currencySymbol}
                        {currency === 'USD' ? +investment.usdPricePerItem.toFixed(2) : +investment.btcPricePerItem.toFixed(6)}
                        &nbsp;per coin
                    </div>
                </div>
                <div className="mobileCardCell"data-flex={isSmallScreen ? 30: 20} data-layout="column" data-layout-align="center center">
                    <div className="transactionPrimaryData">
                        {currencySymbol}
                        {currency === 'USD' ? +(investment.usdPricePerItem * investment.amount).toFixed(2) : +(investment.btcPricePerItem * investment.amount).toFixed(6)}
                    </div>
                    <div className="transactionSecondaryData">
                        {currencySymbol}
                        {currency === 'USD' ? +investment.usdPricePerItem.toFixed(2) : +investment.btcPricePerItem.toFixed(6)}
                        &nbsp;per coin
                    </div>
                </div>
                <div className="mobileCardCell" data-flex="5"  data-layout="column" data-layout-align="center center">

                        <TransactionMenu investment={investment}/>

                </div>
            </div>
        </div>
    );
};

export default InvestmentsHistoryMobileCard;