/**
 * Created by dev on 3.8.17.
 */
import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Table, {
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import TransactionRow from './transactionRow/TransactionRow';
import {sortCoinTransactions} from '../../../actions/investments';
import './InvestmentsHistory.css';

const InvestmentsHistory = props => {

    const handleRequestSort = property => {
        const orderBy = property;
        props.sortCoinTransactions({orderBy});
    };

    const currency = props.user.settings.currency || 'USD';

    const tableHeadData = [
        {id: 'coin.name', numeric: false, disablePadding: false, compact: false, label: 'Currency', class: 'investmentsTableHead'},
        {id: 'dateBought', numeric: false, disablePadding: false, compact: false, label: 'Date', class: 'investmentsTableHead'},
        {id: 'amount', numeric: false, disablePadding: false, compact: false, label: 'Amount', class: 'investmentsTableHead'},
        {id: currency === 'USD' ? 'isdPricePerItem' : 'btcPricePerItem', numeric: false, disablePadding: false, compact: false, label: 'Price', class: 'investmentsTableHead pricePerItem'},
        {id: 'options', numeric: false, disablePadding: true, compact: true, label: '', class: 'investmentsTableHead'}
    ];

    return (
        <div>
            <Table>
                <TableHead>
                    <TableRow>
                        {tableHeadData.map(column => {
                            return (
                                <TableCell
                                    className={column.class}
                                    key={column.id}
                                    numeric={column.numeric}
                                    compact={column.compact}
                                    disablePadding={column.disablePadding}>
                                    <TableSortLabel
                                        active={props.sorting.orderBy === column.id}
                                        direction={props.sorting.order}
                                        onClick={() => handleRequestSort(column.id)}>
                                        {column.label}
                                    </TableSortLabel>
                                </TableCell>
                            );
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.investments.map(investment => {
                        return (
                            <TransactionRow investment={investment} key={investment._id} currency={currency}/>
                        );
                    })}
                </TableBody>
            </Table>
        </div>
    );

};

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    user: state.rootReducer.auth.user,
    sorting: state.rootReducer.investments.coinTransactionSorting
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    sortCoinTransactions: payload => dispatch(sortCoinTransactions(payload))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(InvestmentsHistory));
