/**
 * Created by dev on 8.8.17.
 */
import React from 'react';
import * as moment from 'moment';
import Table, {
    TableCell,
    TableRow
} from 'material-ui/Table';
import TransactionMenu from '../transactionMenu/TransactionMenu';
import './TransactionRow.css';

const TransactionRow = props => {

    const investment = props.investment;
    const currency = props.currency || 'USD';
    const currencySymbol = (currency === 'USD') ? '$' : 'B';
    let imgUrl;

    //build image url
    try {
        imgUrl = require(`../../../../assets/coins/${investment.coin.id}.png`);
    } catch (e) {
        imgUrl = require(`../../../../assets/coins/question-icon.png`);
    }

    return (
        <TableRow hover key={investment._id}>
            <TableCell>
                <div data-flex data-layout="row" data-layout-align="start center">
                    <div>
                        <img src={imgUrl} alt={investment.coin.name} className="coinThumb"/>
                    </div>
                    <div>
                        <div className="tablePrimaryData" >
                            {investment.coin.name}
                        </div>
                        <div className="tableSecondaryData">
                            {investment.action.toUpperCase()}
                        </div>
                    </div>
                </div>
            </TableCell>
            <TableCell>
                <div className="transactionPrimaryData">
                {moment(investment.dateBought).format("D-MM-YYYY")}
                </div>
            </TableCell>
            <TableCell>
                <div className="transactionPrimaryData">
                    {+investment.amount.toFixed(8)} {investment.coin.symbol}
                </div>
                <div className="transactionSecondaryData pricePerItemMobile">
                    {currencySymbol}
                    {currency === 'USD' ? +investment.usdPricePerItem.toFixed(2) : +investment.btcPricePerItem.toFixed(6)}
                    &nbsp;per coin
                </div>
            </TableCell>
            <TableCell className="pricePerItem">
                <div className="transactionPrimaryData">
                    {currencySymbol}
                    {currency === 'USD' ? +(investment.usdPricePerItem * investment.amount).toFixed(2) : +(investment.btcPricePerItem * investment.amount).toFixed(6)}
                </div>
                <div className="transactionSecondaryData">
                    {currencySymbol}
                    {currency === 'USD' ? +investment.usdPricePerItem.toFixed(2) : +investment.btcPricePerItem.toFixed(6)}
                    &nbsp;per coin
                </div>
            </TableCell>
            <TableCell compact="true" disablePadding="true">
                <TransactionMenu investment={investment}/>
            </TableCell>
        </TableRow>
    );
}

export default TransactionRow;