/**
 * Created by dev on 8.8.17.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import Menu, {MenuItem} from 'material-ui/Menu';
import Icon  from 'material-ui/Icon';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import {removeInvestment} from '../../../../actions/investments';
import './TransactionMenu.css';

class TransactionMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            anchorEl: undefined,
            open: false
        };
        this.openMenu = this.openMenu.bind(this);
        this.handleRequestClose = this.handleRequestClose.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    openMenu(event) {
        this.setState({open: true, anchorEl: event.currentTarget});
    };

    handleRequestClose() {
        this.setState({open: false});
    };
    
    handleDelete() {
        //this.setState({open: false});
        this.props.remove(this.props.investment);
    }

    render() {
        const investment = this.props.investment;
        const editRoute = `/portfolio/edit/${investment._id}`;
        return (
            <div>
                <IconButton onClick={this.openMenu}>
                    <Icon className="material-icons">more_vert</Icon>
                </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={this.state.anchorEl}
                    open={this.state.open}
                    onRequestClose={this.handleRequestClose}>
                    <MenuItem>
                        <Link to={editRoute}>Edit</Link>
                    </MenuItem>
                    <Divider/>
                    <MenuItem onClick={this.handleDelete}>Remove</MenuItem>
                </Menu>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
    return {
        // You can now say this.props.books
    }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
    return {
        // You can now say this.props.createBook
        remove: investment => dispatch(removeInvestment(investment)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionMenu);