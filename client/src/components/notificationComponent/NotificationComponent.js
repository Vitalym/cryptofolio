/**
 * Created by dev on 28.7.17.
 */
import React, {Component} from 'react';
import NotificationsSystem from 'reapop';
import theme from 'reapop-theme-wybo';

class NotificationComponent extends Component {
    render() {
        return (
            <div>
                <NotificationsSystem theme={theme}/>
            </div>
        );
    }
}

export default NotificationComponent;
