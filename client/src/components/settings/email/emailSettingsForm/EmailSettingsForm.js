/**
 * Created by Виталий on 25.07.2017.
 */
import React from 'react';
import {reduxForm, Field} from 'redux-form';
import { connect } from 'react-redux';
import Input from 'react-toolbox/lib/input/Input';
import Button from 'react-toolbox/lib/button/Button';
import validate from './validate';

const renderTextField = ({input: {onBlur, ...inputForm}, meta: {touched, error}, ...custom},) => (
    <Input
        error={touched && error}
        {...inputForm}
        {...custom}
    />
);

let EmailSettingsForm = props => {

    const {handleSubmit, pristine, reset, submitting, isFetching} = props;

    return (
        <form onSubmit={handleSubmit}>
            <div data-flex="100" className="formGroup">
                <Field name="email"
                       type="email"
                       icon='email'
                       component={renderTextField}
                       label="Email"/>
            </div>
            <div className="formHelperText">Email is used to restore your password</div>
            <div className="settingsAction" data-flex data-layout="row" data-layout-align="end center">
                <Button type="submit" disabled={pristine || submitting || isFetching} label='Save' className="settingsActionButton" raised primary/>
            </div>
        </form>
    );

};

// Decorate with reduxForm(). It will read the initialValues prop provided by connect()
EmailSettingsForm = reduxForm({
    form: 'EmailSettingsForm',  // a unique identifier for this form
    enableReinitialize: true,
    validate
})(EmailSettingsForm);

EmailSettingsForm = connect(state => ({
    initialValues: {email: state.rootReducer.auth.user.email}
}))(EmailSettingsForm);


export default EmailSettingsForm;
