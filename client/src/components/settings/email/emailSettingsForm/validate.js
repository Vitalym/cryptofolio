import * as EmailValidator from 'email-validator';
export default function(values) {
    const errors = {};
    if (!values.email) {
        errors.email = 'This field is required';
    }

    if(!EmailValidator.validate(values.email)){
        errors.email = `It doesn't seem to be a valid email`
    }

    return errors;
}