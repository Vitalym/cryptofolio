/**
 * Created by dev on 29.8.17.
 */
import React from 'react';
import EmailSettingsForm from './emailSettingsForm/EmailSettingsForm';
import Typography from 'material-ui/Typography';
import './Email.css';

const EmailSettings = props => (
    <div>
        <div className="headerContainer">
            <Typography type="title" className="investmentsHeading">Email</Typography>
            <div className="spacer"/>
        </div>
        <div className="setting">

            <EmailSettingsForm onSubmit={props.onEmailChange}/>
        </div>
    </div>
);

export default EmailSettings;