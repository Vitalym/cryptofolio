/**
 * Created by dev on 29.8.17.
 */
import React from 'react';
import Dropdown from 'react-toolbox/lib/dropdown/Dropdown';
import Typography from 'material-ui/Typography';
import './Currency.css';

const currencyOptions = [
    { value: 'USD', label: 'USD'},
    { value: 'BTC', label: 'BTC' }
];

const CurrencySettings = props => (
    <div>
        <div className="headerContainer">
            <Typography type="title" className="investmentsHeading">Display currency</Typography>
            <div className="spacer"/>
        </div>
        <div className="setting">
            <Dropdown
                className="settingsDropdown"
                onChange={props.onCurrencyChange}
                disabled={props.isFetching}
                source={currencyOptions}
                value={props.currency}
            />
        </div>
    </div>
);

export default CurrencySettings;