/**
 * Created by dev on 29.8.17.
 */
import React from 'react';
import PasswordSettingsForm from './passwordSettingsForm/PasswordSettingsForm';
import Typography from 'material-ui/Typography';
import './Password.css';

const PasswordSettings = props => (
    <div>
        <div className="headerContainer">
            <Typography type="title" className="investmentsHeading">Email</Typography>
            <div className="spacer"/>
        </div>
        <div className="setting">

            <PasswordSettingsForm onSubmit={props.onPasswordChange}/>
        </div>
    </div>
);

export default PasswordSettings;