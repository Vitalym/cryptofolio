/**
 * Created by Виталий on 25.07.2017.
 */
import React from 'react';
import {reduxForm, Field} from 'redux-form';
import Input from 'react-toolbox/lib/input/Input';
import Button from 'react-toolbox/lib/button/Button';
import validate from './validate';

const renderTextField = ({input: {onBlur, ...inputForm}, meta: {touched, error}, ...custom},) => (
    <Input
        error={touched && error}
        {...inputForm}
        {...custom}
    />
);

let PasswordSettingsForm = props => {

    const {handleSubmit, pristine, reset, submitting, isFetching} = props;

    return (
        <form onSubmit={handleSubmit}>
            <div data-flex="100" className="formGroup">
                <Field name="password"
                       type="password"
                       icon="lock"
                       component={renderTextField}
                       label="New password"/>
            </div>
            <div data-flex="100" className="formGroup">
                <Field name="confirmPassword"
                       type="password"
                       icon="lock"
                       component={renderTextField}
                       label="Confirm password"/>
            </div>
            <div className="settingsAction" data-flex data-layout="row" data-layout-align="end center">
                <Button type="submit" disabled={pristine || submitting || isFetching} label='Save' className="settingsActionButton" raised primary/>
            </div>
        </form>
    );

};

export default reduxForm({
    form: 'PasswordSettingsForm',
    validate
})(PasswordSettingsForm);
