/**
 * Created by dev on 7.8.17.
 */
import React from 'react';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import './BackButton.css';

const BackButton = props => (
    <div data-flex data-layout="row" data-layout-align="end center">
        <IconButton className="backButton" aria-label="Back" href={props.href}>
            <Icon className="material-icons">close</Icon>
        </IconButton>
    </div>

);

export default BackButton;