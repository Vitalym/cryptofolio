/**
 * Created by Виталий on 30.08.2017.
 */
import React from 'react';
import * as Spinner from 'react-spinkit';
import './TableLoadingIcon.css'

const TableLoadingIcon = props => {
    return (
        <div className="tableLoading" data-flex data-layout="column" data-layout-align="center center">
            <div className="tableLoadingSpinner"  data-flex data-layout="column" data-layout-align="center center">
                <Spinner name="ball-clip-rotate-multiple"/>
            </div>
        </div>
    );
};

export default TableLoadingIcon;