/**
 * Created by dev on 10.8.17.
 */
import React from 'react';
import Table, {
    TableCell,
    TableRow
} from 'material-ui/Table';
import TableTrendIcon from '../../tableTrendIcon/TableTrendIcon';
import './CoinDataMobileCard.css';

const CoinDataRow = props => {

/*    const handleClick = (event, name) => {
        history.push(`/portfolio/history/${name}`);
    };*/
    const coin = props.coin;
    const currency = props.currency || 'USD';
    const currencySymbol = (currency === 'USD') ? '$' : 'B';
    const isMobileSmallScreen = props.isMobileSmallScreen;
    let imgUrl;

    //build image url
    try {
        imgUrl = require(`../../../assets/coins/${coin.id}.png`);
    } catch (e) {
        imgUrl = require(`../../../assets/coins/question-icon.png`);
    }

    return (
        <div className="marketDataContainer">
            <div className="mobileCard" data-flex data-layout="row">
                <div className="mobileCardCell" data-flex={isMobileSmallScreen ? '33' : '25'}>
                    <div className="tablePrimaryData fullHeight" data-flex data-layout="row" data-layout-align="start center">
                        {props.index + 1} &nbsp; <img src={imgUrl} alt={coin.name} className="coinThumb"/> {coin.name}</div>
                </div>
                <div className="mobileCardCell" data-flex={isMobileSmallScreen ? '33' : '25'} data-layout="row" data-layout-align="center center">
                    <div className="tablePrimaryData fullHeight"  data-flex data-layout="row" data-layout-align="center center">
                        {currency === 'USD' ? currencySymbol + +coin.price_usd : +coin.price_btc + currencySymbol}
                        </div>
                </div>
                <div className="mobileCardCell marketCapCell" data-flex="25" data-layout="row" data-layout-align="center center">
                    <div className="tablePrimaryData fullHeight" data-layout="row" data-layout-align="center center">
                        ${+coin.market_cap_usd}
                    </div>
                </div>
                <div data-flex={isMobileSmallScreen ? '33' : '25'} data-layout="row" data-layout-align="center center" className={Number(coin.percent_change_24h) > 0 ? 'positive mobileCardCell fullHeight' : 'negative mobileCardCell fullHeight'}>
                    <div className="tablePrimaryData fullHeight" data-layout="row" data-layout-align="center center">
                        {+coin.percent_change_24h}%
                        <TableTrendIcon trend={+coin.percent_change_24h}/>
                    </div>
                </div>
            </div>
        </div>
    );
};

//{Number(asset.totalProfitPercentage) > 0 ? 'arrow_drop_up' : 'arrow_drop_down'}

export default CoinDataRow;
