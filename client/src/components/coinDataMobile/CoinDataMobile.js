/**
 * Created by dev on 3.8.17.
 */
import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import CoinDataMobileCard from './CoinDataMobileCard/CoinDataMobileCard'
import * as Waypoint from 'react-waypoint';
import {sortCoinsData} from '../../actions/currencies';
import './CoinDataMobile.css';

const CoinDataTable = props => {

    const isMobileSmallScreen = props.isMobileSmallScreen;

    const handleRequestSort = (orderBy) => {
        props.sortCoinsData({orderBy});
    };

    const load = () => {
        props.onLoadMore();
    };

    let coins = props.searchQuery ? props.searchResults :  props.coins;

    return (
        <div className="tableContainer">
            <div className="mobileCardHeader" data-flex data-layout="row">
                <div className="mobileCardCell" data-flex={isMobileSmallScreen ? '33' : '25'} data-layout="column"
                     data-layout-align="center center">
                    Currency
                </div>
                <div className="mobileCardCell" data-flex={isMobileSmallScreen ? '33' : '25'} data-layout="column"
                     data-layout-align="center center">
                    Price
                </div>
                <div className="mobileCardCell marketCapCell" data-flex="25" data-layout="column"
                     data-layout-align="center center">
                    Market cap
                </div>
                <div className="mobileCardCell" data-flex={isMobileSmallScreen ? '33' : '25'} data-layout="column"
                     data-layout-align="center center">
                    24h change
                </div>
            </div>
            {coins.map((coin, index) => {
                return (
                    <div data-flex="100" data-layout="column" key={index} className="mobileCardContainer">
                        <CoinDataMobileCard coin={coin} index={index} currency={props.currency}
                                            isMobileSmallScreen={isMobileSmallScreen}/>
                    </div>
                );
            })}
            <Waypoint onEnter={props.searchQuery ? null : load}/>
        </div>
    );

};

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    coins: state.rootReducer.currencies.coinData,
    sorting: state.rootReducer.currencies.coinSorting,
    searchResults: state.rootReducer.currencies.searchResults,
    searchQuery: state.rootReducer.currencies.searchQuery
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    sortCoinsData: payload => dispatch(sortCoinsData(payload))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CoinDataTable));