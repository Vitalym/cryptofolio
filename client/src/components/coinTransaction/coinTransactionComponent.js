/**
 * Created by Виталий on 15.08.2017.
 */
import React, {Component} from 'react'
import {connect} from 'react-redux';
import LoadingScreen from '../../components/loadingScreen/LoadingScreen';
import {Helmet} from 'react-helmet';
import Typography from 'material-ui/Typography';
import {getByCoin} from '../../actions/investments';

import '../../containers/investments/Investments.css';

export function coinTransactionComponent(WrappedComponent) {

    class TransactionComponent extends Component {

        componentDidMount() {
            this.props.getByCoin(this.props.match.params.id);
        }

        render() {
            if (this.props.isFetching) {
                return (
                    <LoadingScreen/>
                )
            }
            const EmptyTransaction = (
                <div className="appContainer viewMessageContainer" data-flex data-layout="column" data-layout-align="center center">
                    <Helmet>
                        <title>Coin Overwatch | Not found</title>
                    </Helmet>
                    <Typography type="headline" className="messageHeading notFoundHeading">404</Typography>
                    <Typography type="subheading" className="messageSubHeading">We didn't find anything related to this coin in your history</Typography>
                </div>
            );
            return (
                <div>
                    {this.props.transactions && this.props.transactions.length
                        ? <WrappedComponent {...this.props}/>
                        : EmptyTransaction
                    }
                </div>
            )

        }
    }

    const mapStateToProps = (state) => ({
        transactions: state.rootReducer.investments.transactionsByCoin,
        isFetching: state.rootReducer.investments.isFetching,
        stats: state.rootReducer.investments.transactionsByCoinStats,
        history: state.rootReducer.investments.transactionsByCoinHistory,
        isFetchingCoinHistory: state.rootReducer.investments.isFetchingCoinHistory,
    });


    // Maps actions to props
    const mapDispatchToProps = (dispatch) => ({
        getByCoin: (id) => dispatch(getByCoin(id))
    });

    return connect(mapStateToProps, mapDispatchToProps)(TransactionComponent);

}