/**
 * Created by dev on on 16.08.2017.
 */
import React from 'react';
import Card, {CardContent} from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import TableTrendIcon from '../../tableTrendIcon/TableTrendIcon';
import './TransactionValue.css';

const TransactionValue = props => {
    const currency = props.user.settings.currency || 'USD';
    const currencySymbol = (currency === 'USD') ? '$' : 'B';
    return (

            <div className="root">
                <div className="transactionValue" data-flex
                      data-layout='column'

                     data-layout-align-lt-md='space-between center'
                     data-layout-gt-xs="row">
                    <Card className='portfolioCard' data-flex="33" data-layout='column'>
                        <div className="cardHeaderContainer">
                            <Typography type="title">
                                Total value
                            </Typography>
                        </div>
                        <CardContent>
                            <div className="primaryData primaryColor">
                                {+props.stats.totalAmount.toFixed(8)}
                            </div>
                            <div className="valueContainer">
                                <div className="secondaryData">
                                    {currency === 'USD' ? '$' + +props.stats.totalUsdValue.toFixed(2) : +props.stats.totalBtcValue.toFixed(6) + ' BTC'}
                                </div>
                            </div>
                        </CardContent>
                    </Card>

                    <Card className='portfolioCard' data-flex="33" data-layout='column'>
                        <div className="cardHeaderContainer">
                            <Typography type="title">
                                Acquisition cost
                            </Typography>
                        </div>
                        <CardContent>
                            <div className="primaryData primaryColor">
                                {currency === 'USD' ? '$' + +props.stats.totalUsdSpent.toFixed(2) : +props.stats.totalBtcSpent.toFixed(6) + ' BTC'}
                            </div>
                            <div className="valueContainer">
                                <div className="secondaryData">
                                    {currency === 'USD' ? '$' + +props.stats.averageUsdAcquisitionCost.toFixed(2) : +props.stats.averageBtcAcquisitionCost.toFixed(6) + ' BTC'}
                                    &nbsp; per coin
                                </div>
                            </div>
                        </CardContent>
                    </Card>

                    <Card className='portfolioCard' data-flex="33" data-layout='column'>
                        <div className="cardHeaderContainer">
                            <Typography type="title">
                                Total profit
                            </Typography>
                        </div>
                        <CardContent>
                            <div className="primaryData">
                                <div
                                    className={Number(currency === 'USD' ? +props.stats.totalUsdProfitPercentage : +props.stats.totalBtcProfitPercentage) > 0 ? 'positive' : 'negative'}>
                                    {currency === 'USD' ? +props.stats.totalUsdProfitPercentage.toFixed(2) : +props.stats.totalBtcProfitPercentage.toFixed(2)} %
                                    <TableTrendIcon trend={currency === 'USD' ? props.stats.totalUsdProfitPercentage : props.stats.totalBtcProfitPercentage}/>
                                </div>
                            </div>
                            <div className="valueContainer">
                                <div className="secondaryData">
                                    {currency === 'USD' ? '$' + +props.stats.totalProfitUSD.toFixed(2) : +props.stats.totalProfitBTC.toFixed(6) + ' BTC'}
                                </div>
                            </div>
                        </CardContent>
                    </Card>
                </div>
            </div>
    );
};

export default TransactionValue;