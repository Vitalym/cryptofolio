/**
 * Created by Виталий on 16.08.2017.
 */
import React from 'react';
import './TransactionChart.css';
import * as ReactHighstock from'react-highcharts/ReactHighstock.src';
import TableLoadingIcon from '../../tableLoadingIcon/TableLoadingIcon';

const TransactionChart = props => {
        const config = {
            rangeSelector: {
                selected: 1
            },
            title: {
                text: `${props.name} History`
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: 'Price'
            },
            series: [{
                name: 'Value',
                data: props.data.price
            }]
        };

        return (
            <div data-flex data-layout="row" data-layout-align="center center" className="chartContainer">
                <ReactHighstock config={config}/>
                { props.isFetching ? <TableLoadingIcon/> : null }
            </div>
        )
};

// Use connect to put them together
export default TransactionChart;