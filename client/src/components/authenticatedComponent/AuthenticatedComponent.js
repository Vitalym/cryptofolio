/**
 * Created by Виталий on 27.07.2017.
 */
import React, { Component } from 'react'
import {connect} from 'react-redux';

export function requireAuthentication(WrappedComponent) {

    class AuthenticatedComponent extends Component {

        componentWillMount() {
            this.checkAuth();
        }

        componentWillReceiveProps(nextProps) {
            this.checkAuth();
        }

        checkAuth() {
            if (!this.props.isAuthenticated) {
                //let redirectAfterLogin = this.props.location.pathname;
                //this.props.history.push(`/login?next=${redirectAfterLogin}`);
                this.props.history.push(`/login`);
            }
        }

        render() {
            return (
                <div>
                    {this.props.isAuthenticated
                        ? <WrappedComponent {...this.props}/>
                        : null
                    }
                </div>
            )

        }
    }

    const mapStateToProps = (state) => ({
        token: state.rootReducer.auth.token,
        user: state.rootReducer.auth.user,
        isAuthenticated: state.rootReducer.auth.isAuthenticated
    });

    return connect(mapStateToProps)(AuthenticatedComponent);

}