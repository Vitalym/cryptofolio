/**
 * Created by Виталий on 12.08.2017.
 */
import React from 'react';
import Input from 'react-toolbox/lib/input/Input';
import './AllocationInput.css';

const AllocationsInput = props => {
    return (
        <div>
            <Input type='text' label={props.label} name={props.name} onChange={props.onChange} value={props.value} />
        </div>
    )
};

export default AllocationsInput;