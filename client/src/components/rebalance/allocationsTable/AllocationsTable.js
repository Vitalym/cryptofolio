/**
 * Created by Виталий on 11.08.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Table, {
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import HoldingTableRow from './allocationsTableRow/AllocationsTableRow';
import {getSum} from '../../../utils/utils';
import {sortAllocationsSetupTable, updateAllocationValue, setAllocationValues} from '../../../actions/allocation';
import './AllocationsTable.css';

class AllocationsTable extends Component {
    constructor(props) {
        super(props);
        this.handleRequestSort = this.handleRequestSort.bind(this);
    }

    componentDidMount() {
        this.props.setAllocationValues(false);
    }

    handleRequestSort(orderBy) {
        this.props.sort({orderBy});
    };

    render() {
        const tableHeadData = [
            {id: 'coin.name', numeric: false, disablePadding: false, label: 'Currency'},
            {id: 'targetAllocation', numeric: false, disablePadding: false, label: 'Target Allocation'},
            {id: 'share', numeric: false, disablePadding: false, label: 'Current allocation'}
        ];
        return (
            <div>
                    <Table>
                        <TableHead>
                            <TableRow>
                                {tableHeadData.map(column => {
                                    return (
                                        <TableCell
                                            className='investmentsTableHead'
                                            key={column.id}
                                            numeric={column.numeric}
                                            disablePadding={column.disablePadding}>
                                            <TableSortLabel>
                                                {column.label}
                                            </TableSortLabel>
                                        </TableCell>
                                    );
                                })}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.holdings.map(investment => {
                                return (
                                    <HoldingTableRow asset={investment}
                                                     key={investment._id}
                                                     onValueChange={this.props.updateAllocationValue}/>
                                );
                            })}
                            <TableRow>
                                <TableCell>
                                    <p className="tablePrimaryData">Total</p>
                                </TableCell>
                                <TableCell>
                                    {getSum(this.props.allocationSchemaFields, 'value').toFixed(4)}
                                </TableCell>
                                <TableCell>
                                    <p className="tablePrimaryData">
                                        {getSum(this.props.holdings, 'targetAllocation').toFixed(4)}
                                    </p>
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
        holdings: state.rootReducer.allocations.allocationsSchema,
        sorting: state.rootReducer.allocations.allocationsSorting,
        allocationSchemaFields: state.rootReducer.allocations.allocationSchemaFields
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
        sort: payload => dispatch(sortAllocationsSetupTable(payload)),
        updateAllocationValue: payload => dispatch(updateAllocationValue(payload)),
        setAllocationValues: () => dispatch(setAllocationValues())
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AllocationsTable));