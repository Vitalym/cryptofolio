/**
 * Created by Виталий on 11.08.2017.
 */
import React, {Component} from 'react';
import Table, {
    TableCell,
    TableRow
} from 'material-ui/Table';
import AllocationsInput from '../allocationInput/AllocationInput';
import './AllocationsTableRow.css';

class AllocationsTableRow extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        const asset = this.props.asset;
        this.state = {
            value: isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? parseFloat(+asset.share.toFixed(2)) : parseFloat(+asset.targetAllocation.toFixed(2))
        }
    }

    handleChange (e) {
        //pass value from the input up
        this.props.onValueChange({value: e, coin: this.props.asset.coin.id});
        this.setState({
            value: e
        })
    }

    //value={isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? parseFloat(+asset.share.toFixed(2)) : parseFloat(+asset.targetAllocation.toFixed(2))}
    render() {
        const asset = this.props.asset;
        return (
            <TableRow hover key={asset._id}>
                <TableCell>
                    <p className="tablePrimaryData">{asset.coin.name}</p>
                </TableCell>
                <TableCell>
                    <AllocationsInput
                        value={this.state.value}
                        label={asset.coin.name}
                        name={asset.coin.id}
                        onChange={this.handleChange}/>
                </TableCell>
                <TableCell>
                    <p className="tablePrimaryData">
                        {+asset.share.toFixed(2)}%
                    </p>
                </TableCell>
            </TableRow>
        );
    }
}

export default AllocationsTableRow;
