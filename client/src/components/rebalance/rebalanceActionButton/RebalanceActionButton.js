/**
 * Created by dev on 7.8.17.
 */
import React from 'react';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import './RebalanceActionButton.css';

const RebalanceActionButton = props => (
        <Button raised={props.raised}
                color={props.color}
                onClick={props.onButtonClick}
                className='actionButton'>
            <Icon className="material-icons">add</Icon>
            {props.text}
        </Button>
);

export default RebalanceActionButton;