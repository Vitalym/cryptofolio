/**
 * Created by Виталий on 11.08.2017.
 */
import React from 'react';
import TableTrendIcon from '../../../tableTrendIcon/TableTrendIcon';
import './HoldingsTableMobileCard.css';

const HoldingsTableMobileCard = props => {
    const asset = props.asset;
    const selected = props.selected;
    const displayOptions = props.displayOptions;
    const columnWidth = props.columnWidth;
    const currency = props.currency || 'USD';
    const currencySymbol = (currency === 'USD') ? '$' : 'B';
    const toBuy = (currency === 'USD') ? parseFloat(asset.usdToBuy).toFixed(2) : parseFloat(asset.btcToBuy).toFixed(6);
    const usdValueAfterRebalance = (currency === 'USD') ? parseFloat(asset.usdValueAfterRebalance).toFixed(2) : parseFloat(asset.btcValueAfterRebalance).toFixed(6);

    const handleClick = (event, name) => {
        //history.push(`/portfolio/history/${name}`);
    };

    let imgUrl;

    //build image url
    try {
        imgUrl = require(`../../../../assets/coins/${asset.coin.id}.png`);
    } catch (e) {
        imgUrl = require(`../../../../assets/coins/question-icon.png`);
    }

    const details = [
        {
            title: 'Total amount, $',
            data: currencySymbol + +asset.totalUsdSpent.toFixed(2),
            className: 'coinDetailsData'
        },
        {
            title: 'Total amount, BTC',
            data: +asset.totalBtcSpent.toFixed(4) + currencySymbol,
            className: 'coinDetailsData'
        },
    ];

    const coinDetails = (
        <div className="coinDetailsContainer">
            {details.map(info => {
                return (
                    <div className="coinDetails" data-flex data-layout="row" data-layout-align="space-between">
                        <div className="coinDetailsHeading">
                            {info.title}
                        </div>
                        <div className={info.className}>
                            {info.data}
                            {info.trendIcon ? <TableTrendIcon trend={info.trend}/> : null}
                        </div>
                    </div>
                )
            })}
        </div>
    );

    return (
        <div>
            <div className="mobileCard" data-flex data-layout="row"
                 onClick={event => handleClick(event, asset.coin.id)}>
                <div className="mobileCardCell" data-flex={columnWidth ? columnWidth : 15}>
                    <div data-flex data-layout="row" data-layout-align="start center">
                        <div>
                            <img src={imgUrl} alt={asset.coin.name} className="coinThumb"/>
                        </div>
                        <div data-flex>
                            <div className="tablePrimaryData" >
                                {asset.coin.name}
                            </div>
                            <div className="tableSecondaryData">
                                {currency === 'USD' ? currencySymbol + parseFloat(asset.currentPriceUsd).toFixed(2) : parseFloat(asset.currentPriceBtc).toFixed(6) + currencySymbol}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mobileCardCell allocationAmount" data-flex="20" data-layout="column" data-layout-align="center center">
                    <div className="tablePrimaryData">
                        {+asset.totalAmount.toFixed(4)} {asset.coin.symbol}
                    </div>
                    <div className="tableSecondaryData">
                        {currency === 'USD' ? currencySymbol + +asset.totalUsdValue.toFixed(2) : +asset.totalBtcValue.toFixed(6) + currencySymbol}
                        &nbsp;
                    </div>
                </div>
                <div className="mobileCardCell" data-flex={columnWidth ? columnWidth : 20} data-layout="column" data-layout-align="center center">
                    <div className="tablePrimaryData">
                        { isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? '-' : parseFloat(asset.targetAllocation.toFixed(2)) + '%'}
                    </div>
                    <div className="tableSecondaryData primaryColor">
                        <strong>{+asset.share.toFixed(2)}%</strong> current
                    </div>
                </div>
                <div className="mobileCardCell" data-flex={columnWidth ? columnWidth : 20} data-layout="column" data-layout-align="center center">
                    <div className="tablePrimaryData">
                        {isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? "-" : parseFloat(asset.toBuy.toFixed(2)) + " " + asset.coin.symbol}
                        &nbsp; <TableTrendIcon trend={asset.toBuy}/>
                    </div>
                    <div className="tableSecondaryData">
                        {isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? "" :  currencySymbol + toBuy}
                    </div>
                </div>
                <div className="mobileCardCell allocationNewBalance" data-flex="25" data-layout="column" data-layout-align="center center">
                    <p className="tablePrimaryData">
                        {isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? "-" : parseFloat(asset.valueAfterRebalance.toFixed(4)) + " " + asset.coin.symbol}
                    </p>
                    <p className="tableSecondaryData">
                        {isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? "" : currencySymbol + usdValueAfterRebalance}
                    </p>
                </div>
            </div>
            {selected === asset.coin.id ? coinDetails : null}
        </div>
    );
};

export default HoldingsTableMobileCard;