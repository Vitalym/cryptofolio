/**
 * Created by Виталий on 11.08.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import HoldingsTableMobileCard from './holdingsTableMobileCard/HoldingsTableMobileCard';
import {sortHoldings} from '../../../actions/allocation';
import './HoldingsTableMobile.css';

class HoldingsTableMobile extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);

        this.state = {
            selected: null
        };

        this.currency = props.user.settings.currency || 'USD';
        this.displayOptions = this.props.displayOptions;
    }

    handleClick (event, selected) {
        if(this.props.displayOptions){
            this.state.selected === selected ? this.setState( { selected: null }) : this.setState( { selected });
        }
    };


    handleRequestSort(property) {
        this.props.sortHoldings({orderBy: property});
    };

    render() {
        return (
            <div className="tableContainer">
                <div className="mobileCardHeader" data-flex data-layout="row">
                    <div className="mobileCardCell" data-flex={this.props.columnWidth ? this.props.columnWidth:  15} data-layout="column" data-layout-align="center start">
                        Currency
                    </div>
                    <div className="mobileCardCell allocationAmount" data-flex="20" data-layout="column" data-layout-align="center center">
                        Amount
                    </div>
                    <div className="mobileCardCell" data-flex={this.props.columnWidth ? this.props.columnWidth:  20} data-layout="column" data-layout-align="center center">
                        Allocation
                    </div>
                    <div className="mobileCardCell" data-flex={this.props.columnWidth ? this.props.columnWidth:  20} data-layout="column" data-layout-align="center center">
                        To buy / sell
                    </div>
                    <div className="mobileCardCell allocationNewBalance" data-flex="25" data-layout="column" data-layout-align="center center">
                        New balance
                    </div>
                </div>
                {this.props.holdings
                    .filter(investment => {
                        return this.props.user.settings.hideZeroBalances ? investment.totalAmount > 0 : true
                    })
                    .map(investment => {
                        return (
                            <div data-flex="100" data-layout="column" key={investment._id} className="mobileCardContainer"
                                 onClick={event => this.handleClick(event, investment.coin.id)}>
                                <HoldingsTableMobileCard asset={investment}
                                                         currency={this.currency}
                                                         columnWidth={this.props.columnWidth}
                                                         displayOptions={this.displayOptions}
                                                         selected={this.state.selected}/>
                            </div>
                        );
                    })}
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    holdings: state.rootReducer.allocations.holdings,
    sorting: state.rootReducer.allocations.holdingsSorting,
    user: state.rootReducer.auth.user
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    sortHoldings: payload => dispatch(sortHoldings(payload))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HoldingsTableMobile));