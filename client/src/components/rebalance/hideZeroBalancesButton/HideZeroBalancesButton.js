import React from 'react';
import { FormControlLabel } from 'material-ui/Form';
import Switch from 'material-ui/Switch';
import './HideZeroBalancesButton.css';

const HideZeroBalancesButton = props => (
   <div>
       <FormControlLabel
           control={
               <Switch
                   checked={props.checked}
                   onChange={(event, checked) => props.onChange(event, checked)}
               />
           }
           label="Hide zero balances"
       />
   </div>
);

export default HideZeroBalancesButton;