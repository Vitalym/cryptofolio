/**
 * Created by Виталий on 11.08.2017.
 */
import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Table, {
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import HoldingTableRow from './holdingsTableRow/HoldingsTableRow';
import {sortHoldings} from '../../../actions/allocation';
import history  from './../../../history';
import './HoldingsTable.css';

const HoldingsTable = props => {

    const currency = props.user.settings.currency || 'USD';
    const isSmallScreen = props.isSmallScreen;

    const handleRequestSort = orderBy => {
        props.sortHoldings({orderBy});
    };

    const tableHeadData = [
        {id: 'coin.name', numeric: false, disablePadding: false, label: 'Currency', className: 'investmentsTableHead'},
        {id: 'share', numeric: false, disablePadding: false, label: 'Current allocation', className: 'investmentsTableHead allocationShare'},
        {id: currency === 'USD' ? 'totalUsdValue' : 'totalBtcValue', numeric: false, disablePadding: false, label: 'Amount', className: 'investmentsTableHead'},
        {id: 'targetAllocation', numeric: false, disablePadding: false, label: isSmallScreen ? 'Allocation' : 'Target allocation', className: 'investmentsTableHead'},
        {id: 'amountToSell', numeric: false, disablePadding: false, label: 'Amount to buy / sell', className: 'investmentsTableHead'},
        {id: 'afterRebalanceValue', numeric: false, disablePadding: false, label: 'New balance', className: 'investmentsTableHead'},
    ];
    return (
        <div>
            <Table selectable>
                <TableHead>
                    <TableRow>
                        {tableHeadData.map(column => {
                            return (
                                <TableCell
                                    className={column.className}
                                    key={column.id}
                                    numeric={column.numeric}
                                    disablePadding={column.disablePadding}>
                                    <TableSortLabel
                                        active={props.sorting.orderBy === column.id}
                                        direction={props.sorting.order}
                                        onClick={() => handleRequestSort(column.id)}>
                                        {column.label}
                                    </TableSortLabel>
                                </TableCell>
                            );
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.holdings
                        .filter(investment =>{
                            return props.user.settings.hideZeroBalances ? investment.totalAmount > 0 : true
                        })
                        .map(investment => {
                        return (
                            <HoldingTableRow asset={investment} currency={currency} key={investment._id}/>
                        );
                    })}
                </TableBody>
            </Table>
        </div>
    );
};

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    holdings: state.rootReducer.allocations.holdings,
    sorting: state.rootReducer.allocations.holdingsSorting,
    user: state.rootReducer.auth.user
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    sortHoldings: payload => dispatch(sortHoldings(payload))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HoldingsTable));