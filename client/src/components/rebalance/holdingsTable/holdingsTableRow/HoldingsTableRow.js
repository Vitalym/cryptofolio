/**
 * Created by Виталий on 11.08.2017.
 */
import React from 'react';
import Table, {
    TableCell,
    TableRow
} from 'material-ui/Table';
import TableTrendIcon from '../../../tableTrendIcon/TableTrendIcon';
import './HoldingsTableRow.css';

const HoldingRow = props => {
    const asset = props.asset;
    const currency = props.currency || 'USD';
    const currencySymbol = (currency === 'USD') ? '$' : 'B';
    const toBuy = (currency === 'USD') ? parseFloat(asset.usdToBuy).toFixed(2) : parseFloat(asset.btcToBuy).toFixed(6);
    const usdValueAfterRebalance = (currency === 'USD') ? parseFloat(asset.usdValueAfterRebalance).toFixed(2) : parseFloat(asset.btcValueAfterRebalance).toFixed(6);

    let imgUrl;

    //build image url
    try {
        imgUrl = require(`../../../../assets/coins/${asset.coin.id}.png`);
    } catch (e) {
        imgUrl = require(`../../../../assets/coins/question-icon.png`);
    }

    return (
        <TableRow hover key={asset._id}>
            <TableCell>
                <div data-flex data-layout="row" data-layout-align="start center">
                    <div>
                        <img src={imgUrl} alt={asset.coin.name} className="coinThumb"/>
                    </div>
                    <div>
                        <div className="tablePrimaryData" >
                            {asset.coin.name}
                        </div>
                        <div className="tableSecondaryData">
                            {currency === 'USD' ? currencySymbol + parseFloat(asset.currentPriceUsd).toFixed(2) : parseFloat(asset.currentPriceBtc).toFixed(6) + currencySymbol}
                        </div>
                    </div>
                </div>
            </TableCell>
            <TableCell className="allocationShare">
                <div className="tablePrimaryData">
                    {+asset.share.toFixed(2)}%
                </div>
            </TableCell>
            <TableCell>
                <p className="tablePrimaryData">
                    {+asset.totalAmount.toFixed(4)} {asset.coin.symbol}
                    </p>
                <p className="tableSecondaryData">
                    {currency === 'USD' ? currencySymbol + +asset.totalUsdValue.toFixed(2) : +asset.totalBtcValue.toFixed(6) + currencySymbol}
                    &nbsp;
                </p>
            </TableCell>
            <TableCell>
                <div className="tablePrimaryData allocationShare">
                    { isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? '-' : parseFloat(asset.targetAllocation.toFixed(2)) + '%'}
                </div>
                <div className="allocationShareMobile">
                    <div className="tablePrimaryData">
                        { isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? '-' : parseFloat(asset.targetAllocation.toFixed(2)) + '%'}
                    </div>
                    <div className="tableSecondaryData primaryColor">
                        <strong>{+asset.share.toFixed(2)}%</strong> current
                    </div>
                </div>
            </TableCell>
            <TableCell className={Number(asset.toBuy) > 0 ? 'positive' : 'negative'}>
                <div className="cellContainer" data-layout="row" data-flex>
                    <div className="subcellContainer">
                        <p className="tablePrimaryData">
                            {isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? "-" : parseFloat(asset.toBuy.toFixed(2)) + " " + asset.coin.symbol}
                        </p>
                        <p className="tableSecondaryData">
                            {isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? "" :  currencySymbol + toBuy}
                        </p>
                    </div>
                    <div className="subcellContainer" data-layout="row" data-layout-align="center center">
                        <TableTrendIcon trend={asset.toBuy}/>
                    </div>
                </div>
            </TableCell>
            <TableCell>
                <p className="tablePrimaryData">
                    {isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? "-" : parseFloat(asset.valueAfterRebalance.toFixed(4)) + " " + asset.coin.symbol}
                </p>
                <p className="tableSecondaryData">
                    {isNaN(asset.targetAllocation) || asset.targetAllocation <= 0 ? "" : currencySymbol + usdValueAfterRebalance}
                </p>
            </TableCell>
        </TableRow>
    );
};

export default HoldingRow;