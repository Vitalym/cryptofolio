/**
 * Created by Виталий on 12.08.2017.
 */
import React from 'react';
import Dialog, {
    withResponsiveFullScreen,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';
import Button from 'material-ui/Button';
import AllocationsTable from '../allocationsTable/AllocationsTable'
import './AllocationsDialog.css';

const ResponsiveDialog = withResponsiveFullScreen({breakpoint: 'sm'})(Dialog);

const AllocationsDialog = props => {
    return (<div>
        <ResponsiveDialog

            open={props.active}
            onRequestClose={props.onDialogToggle}>
            <DialogTitle>
                {`How would you like to balance your portfolio?`}
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    <AllocationsTable/>
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onDialogToggle} color="accent">
                    Cancel
                </Button>
                <Button raised onClick={props.onSave} color="primary">
                    Save
                </Button>
            </DialogActions>

        </ResponsiveDialog>
    </div>)
};

export default AllocationsDialog;

/*
<Dialog
    actions={actions}
    active={props.active}
    onEscKeyDown={props.onDialogToggle}
    onOverlayClick={props.onDialogToggle}
    title='How would you like to balance your portfoliog'>
    <div className="overflowWrapper">
        <AllocationsTable/>
    </div>
</Dialog>*/
