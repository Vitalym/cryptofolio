/**
 * Created by Виталий on 15.08.2017.
 */
import React from 'react';
import Icon from 'material-ui/Icon';
import './TableTrendIcon.css'

const TableTrendIcon = props => {
    const {trend} = props;
    return (
        Number(trend) > 0 ?
            <span className="iconContainer">
                <Icon>arrow_upward</Icon>
            </span>
            :
            <span className="iconContainer">
                <Icon>arrow_downward</Icon>
            </span>
    );
};

export default TableTrendIcon;