/**
 * Created by dev on 6.9.17.
 */
import React from 'react';
import Dropdown from 'react-toolbox/lib/dropdown';
import './SortingDropdown.css';

const SortingDropdown = props => (
    <div data-flex data-layout="row" data-layout-align="end center">
        <Dropdown
            auto
            className="sortingDropdown"
            onChange={props.onChange}
            source={props.options}
            value={props.value}
        />
    </div>
);

export default SortingDropdown;