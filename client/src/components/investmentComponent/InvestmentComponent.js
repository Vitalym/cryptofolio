/**
 * Created by dev on 1.8.17.
 */
import React, { Component } from 'react'
import {Helmet} from "react-helmet";
import {connect} from 'react-redux';
import {Container, Grid, Card} from 'semantic-ui-react';
import {getInvestments} from '../../actions/investments';

export function investmentComponent(WrappedComponent) {

    class InvestmentComponent extends Component {

        componentWillMount() {
            if(!this.props.investments.length){
                this.props.getInvestments();
            }
        }

        //<WrappedComponent {...this.props}/>
        render() {
            const EmptyPortfolio = (
                <Container textAlign='center' className="auth-container appContainer">
                    No Investments
                </Container>
            );
            return (
                <div>
                    {this.props.investments && this.props.investments.length
                        ? <WrappedComponent {...this.props}/>
                        : EmptyPortfolio
                    }
                </div>
            )
        }
    }

    const mapStateToProps = (state) => ({
        investments: state.rootReducer.investments.investments
    });

    // Maps actions to props
    const mapDispatchToProps = (dispatch) => {
        return {
            getInvestments: user => dispatch(getInvestments())
        }
    };

    return connect(mapStateToProps, mapDispatchToProps)(InvestmentComponent);

}