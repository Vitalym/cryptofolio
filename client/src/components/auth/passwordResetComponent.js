/**
 * Created by Виталий on 25.08.2017.
 */
import React, {Component} from 'react'
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import {verifyResetToken} from '../../actions/authActions'
import {Link} from 'react-router-dom';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

export function passwordResetComponent(WrappedComponent) {

    class PasswordResetComponent extends Component {

        componentDidMount() {
            this.props.verifyResetToken(this.props.match.params.token);
        }

        render() {
            const Verification = (
                <div className="auth-container">
                    <Helmet>
                        <title>Coin Overwatch | Reset Password</title>
                    </Helmet>
                    <div className="auth-form-container" data-layout="column" data-layout-align="center center">
                        <div className="authLogoContainer">
                            Coin Overwatch
                        </div>
                        <Paper className="authFormPaper">
                            {!this.props.passwordTokenVerification ?
                                <div>
                                    <Typography type="title" className="authHeading">Verifying token...</Typography>
                                </div> 
                                : null}
                            {this.props.passwordTokenVerified && !this.props.passwordTokenVerification ? null :
                                <div>
                                    <Typography type="title" className="authHeading">This reset token is invalid</Typography>
                                    <Typography type="subheading" className="authSubHeading">Please use the link we've emailed you recently</Typography>
                                    <div data-flex="100" className="authSwitcher" data-layout="row" data-layout-align="center center">
                                        Didn't get a link? &nbsp;<Link className="authSwitcherLink" to="/forgot-password">Request it again</Link>
                                    </div>
                                </div>}

                        </Paper>
                    </div>
                </div>
            );
            return (
                <div>
                    {this.props.passwordTokenVerified
                        ? <WrappedComponent {...this.props}/>
                        : Verification
                    }
                </div>
            )

        }
    }

    const mapStateToProps = (state) => ({
        passwordTokenVerified: state.rootReducer.auth.passwordTokenVerified,
        passwordTokenVerification: state.rootReducer.auth.passwordTokenVerification
    });

    const mapDispatchToProps = (dispatch) => ({
        verifyResetToken: token => dispatch(verifyResetToken(token)),
    });

    return connect(mapStateToProps, mapDispatchToProps)(PasswordResetComponent);

}