/**
 * Created by Виталий on 25.08.2017.
 */
import * as EmailValidator from 'email-validator';
export default function(values) {
    const errors = {};
    if (!values.email) {
        errors.email = 'Email is required';
    }

    if(!EmailValidator.validate(values.email)){
        errors.email = `It doesn't seem to be a valid email`
    }

    return errors;
}