/**
 * Created by Виталий on 25.07.2017.
 */
import React from 'react';
import {reduxForm, Field} from 'redux-form';
import Input from 'react-toolbox/lib/input/Input';
import Button from 'react-toolbox/lib/button/Button';
import validate from './validate';
import './RequestResetForm.css';

const renderTextField = ({input: {onBlur, ...inputForm}, meta: {touched, error}, ...custom},) => (
    <Input
        error={touched && error}
        {...inputForm}
        {...custom}
    />
);

let RequestResetForm = props => {

    const {handleSubmit, pristine, reset, submitting, isFetching} = props;
    
    return (
        <form onSubmit={handleSubmit}>
            <div data-flex="100" className="formGroup">
                <Field name="email"
                       type="email"
                       icon='email'
                       component={renderTextField}
                       label="Email"/>
            </div>
            <div data-flex="100" className="formAction" data-layout="row" data-layout-align="end center">
                <Button type="submit" disabled={pristine || isFetching} label='Request password reset' className="formActionButton" raised primary/>
            </div>
        </form>
    );

};

export default reduxForm({
    form: 'RequestResetForm',
    validate
})(RequestResetForm);
