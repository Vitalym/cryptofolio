/**
 * Created by Виталий on 25.07.2017.
 */
import React from 'react';
import {reduxForm, Field} from 'redux-form';
import Input from 'react-toolbox/lib/input/Input';
import Button from 'react-toolbox/lib/button/Button';
import validate from './validate';

import './SignupForm.scss';

const renderTextField = ({input: {onBlur, ...inputForm}, meta: {touched, error}, ...custom},) => (
    <Input
        error={touched && error}
        {...inputForm}
        {...custom}
    />
);

const SignupForm = props => {

    const {handleSubmit, pristine, reset, submitting, isFetching} = props;

    return (
        <form onSubmit={handleSubmit}>
            <div data-flex="100" className="formGroup">
                <Field name="email"
                       type="email"
                       icon="email"
                       component={renderTextField}
                       label="Email"/>
            </div>
            <div data-flex="100" className="formGroup">
                <Field name="password"
                       type="password"
                       icon="lock"
                       component={renderTextField}
                       label="Password"/>
            </div>
            <div data-flex="100" className="formGroup">
                <Field name="confirmPassword"
                       type="password"
                       icon="lock"
                       component={renderTextField}
                       label="Confirm password"/>
            </div>
            <div data-flex="100" className="formAction" data-layout="row" data-layout-align="end center">
                <Button type="submit" disabled={pristine || submitting || isFetching} label='Sign up' className="formActionButton" raised primary/>
            </div>
        </form>
    );
};

export default reduxForm({
    form: 'SignupForm',
    validate
})(SignupForm);