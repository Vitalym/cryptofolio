/**
 * Created by dev on 21.08.2017.
 */
import * as EmailValidator from 'email-validator';
export default function(values) {
    const errors = {};
    const requiredFields = [
        'email',
        'password',
        'confirmPassword'
    ];
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'This field is required';
        }
    });

    if(!EmailValidator.validate(values.email)){
        errors.email = `It doesn't seem to be a valid email`
    }

    if (values.password && values.password.length < 6) {
        errors.password = 'Password must have at least 6 characters';
    }

    if (values.password && values.confirmPassword && values.password !== values.confirmPassword) {
        errors.confirmPassword = 'Passwords do not match';
    }

    return errors;
}