/**
 * Created by Виталий on 25.08.2017.
 */
export default function(values) {
    const errors = {};
    const requiredFields = [
        'password',
        'confirmPassword'
    ];
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'This field is required';
        }
    });

    if (values.password && values.password.length < 6) {
        errors.password = 'Password must have at least 6 characters';
    }

    if (values.password && values.confirmPassword && values.password !== values.confirmPassword) {
        errors.confirmPassword = 'Passwords do not match';
    }

    return errors;
}