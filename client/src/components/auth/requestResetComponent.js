/**
 * Created by Виталий on 25.08.2017.
 */
import React, { Component } from 'react'
import {connect} from 'react-redux';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

export function requestResetComponent(WrappedComponent) {

    class RequestResetComponent extends Component {

   /*     componentDidMount() {
            this.props.navEnable(false);
        }*/

        render() {
            const ResetRequested = (
                <div className="auth-container">
                    <div className="auth-form-container" data-layout="column" data-layout-align="center center">
                        <div className="authLogoContainer">
                            Coin Overwatch
                        </div>
                        <Paper className="authFormPaper">
                            <Typography type="title" className="authHeading">You've requested password reset</Typography>
                            <Typography type="subheading" className="authSubHeading">For the next steps, Please check your email</Typography>
                        </Paper>
                    </div>
                </div>
            );
            return (
                <div>
                    {this.props.passwordResetRequested
                        ? ResetRequested
                        : <WrappedComponent {...this.props}/>
                    }
                </div>
            )

        }
    }

    const mapStateToProps = (state) => ({
        passwordResetRequested: state.rootReducer.auth.passwordResetRequested
    });

    return connect(mapStateToProps)(RequestResetComponent);

}