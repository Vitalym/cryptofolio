/**
 * Created by Виталий on 22.08.2017.
 */
import React from 'react';
import FacebookProvider, { Login } from 'react-facebook';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom'
import {loginViaFacebook} from '../../../actions/authActions';
import Button from 'react-toolbox/lib/button/Button';
import './FacebookLogin.css';

const FacebookLogin = props => {
    const handleResponse = (data) => {
        let profile = data.profile;
        let request = {
            email: profile.email ? profile.email : null,
            id: profile.id,
            clientId: '802613709863946',
            name: profile.name,
            code: data.tokenDetail.accessToken,
            redirectUri: window.location.origin + '/'
        };
        props.login(request);
    };

    const handleError = (error) => {
        console.log(error);
    };

        return (
            <FacebookProvider appId="802613709863946">
                <Login
                    scope="email"
                    onResponse={handleResponse}
                    onError={handleError}>
                    <div data-flex="100" className="facebookLogin" data-layout="row" data-layout-align="center center">
                        <Button label='Login via Facebook' className="facebookLoginButton" raised primary/>
                    </div>
                </Login>
            </FacebookProvider>
        );
};

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    token: state.rootReducer.auth.token,
    error: state.rootReducer.auth.error,
    isFetching: state.rootReducer.auth.isFetching
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    login: user => dispatch(loginViaFacebook(user))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FacebookLogin));

/*export default FacebookLogin;*/

//profile
/*email
    :
    "vitaliy.maltsev@gmail.com"
first_name
    :
    "Vitaly"
gender
    :
    "male"
id
    :
    "1514558961920470"
last_name
    :
    "Maltsev"
link
    :
    "https://www.facebook.com/app_scoped_user_id/1514558961920470/"
locale
    :
    "en_US"
name
    :
    "Vitaly Maltsev"
timezone
    :
    3
verified
    :
    true*/

/*
tokenDetail
accessToken
    :
    "EAALZAZBRERfAoBAAeZBHLZA2pZBRFdAIXoVcBSxtJ5x5ZBFFxFdORAy3fjRGMaABy8FfzBnqldJdP3vtE7mzSpyzsLqjjV6trM2feTBCexAIqZBMNtd88pOWhdmUovEAjEVuKKry1tGvkheW7T6Mfush8H2LL5aBm0E50ZC6LQw7EZBEADcB8xuodAsEPhf9xrrqPnp6lSg9HVAZDZD"
expiresIn
    :
    4609
signedRequest
    :
    "yYtz-IoOTY13PPKt7Mb7t8RlqmTywOilKllVG9uU498.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUROZDZRaml6cG44TGZUQmJQeFdpTmtJdXVkekhfbnNKVmhwXzlEdFpKdkVXU0JxaTFQYVRaR0tNNmRqWVpGVTJLdU9Wek5CdDd6NXpuT0Jabzc4UTlfNW1mUTF4TzdMOEhEQjBrT2x1Zm9Uc2FETThOZFlnSjF6a3pySWlweE9BLXpKeFExQllpMnZERVQ3bFY3aGdDazRDSS1fSFFtV19XblBiZ1A1OVFXSmhUZExQWEdMN3hjdE9LWDY2b0hKTzVjSHpnYk5qb1BwVFhtMGlxZ0FZbVhTREJuQ0czTWl6a1JDREt0RnNTYXJGdTFQUFozTXoxdmVkQTJtcTFRd25IcUJsTm1zM3lTbThGel9BbXVVM0ZCWm91MnZlX2g5bHRSMC1jZy01V19odzBCdUlWbXBYRWJ5cWx0Q2JhUmRiU3p3WU1VRF8zbW5NN3RQNURObDFJRiIsImlzc3VlZF9hdCI6MTUwMzM1ODk5MSwidXNlcl9pZCI6IjE1MTQ1NTg5NjE5MjA0NzAifQ"
userID
    :
    "1514558961920470"*/
