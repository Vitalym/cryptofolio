import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import Input from 'react-toolbox/lib/input/Input';

class CoinSearchForm extends Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            value: ''
        };
    }

    handleChange (value){
        this.setState({value});
        this.props.onChange(value);
    }

    render() {
        return (
            <Input type='text' label='Search coins...' icon='search' value={this.state.value} onChange={this.handleChange} />
        );
    }
}

export default CoinSearchForm;