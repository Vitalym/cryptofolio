import React from 'react';
import {connect} from 'react-redux';
import {searchCoins} from '../../../actions/currencies';
import CoinSearchForm from './coinSearchForm/CoinSearchForm';
import TextField from 'material-ui/TextField';
import Input from 'react-toolbox/lib/input/Input';
import './CoinSearch.css';

const CoinSearch = props => {

    const search = (query) => {
        props.searchCoins(query);
    };

    return (
       <div className="coinSearchContainer">
           <CoinSearchForm onChange={search}/>
       </div>
    );
};

const mapStateToProps = (state, ownProps) => ({
    isSearching: state.rootReducer.currencies.coinSorting
});

const mapDispatchToProps = (dispatch) => ({
    searchCoins: query => dispatch(searchCoins(query))
});

export default connect(mapStateToProps, mapDispatchToProps)(CoinSearch);