/**
 * Created by dev on 10.8.17.
 */
import React from 'react';
import Table, {
    TableCell,
    TableRow
} from 'material-ui/Table';
import TableTrendIcon from '../../tableTrendIcon/TableTrendIcon';
import './CoinDataRow.css';

const CoinDataRow = props => {

/*    const handleClick = (event, name) => {
        history.push(`/portfolio/history/${name}`);
    };*/
    const coin = props.coin;
    const currency = props.currency || 'USD';
    const currencySymbol = (currency === 'USD') ? '$' : 'B';
    let imgUrl;

    //build image url
    try {
        imgUrl = require(`../../../assets/coins/${coin.id}.png`);
    } catch (e) {
        imgUrl = require(`../../../assets/coins/question-icon.png`);
    }

    return (
        <TableRow hover key={coin.id}>
            <TableCell>
                <div className="tablePrimaryData" data-flex data-layout="row" data-layout-align="start center">
                    {props.index + 1} &nbsp; <img src={imgUrl} alt={coin.name} className="coinThumb"/> {coin.name}</div>
            </TableCell>
            <TableCell>
                <p className="tablePrimaryData">${+coin.price_usd}</p>
            </TableCell>
            <TableCell>
                <p className="tablePrimaryData">
                    ${+coin.market_cap_usd}
                </p>
            </TableCell>
            <TableCell className={Number(coin.percent_change_24h) > 0 ? 'positive' : 'negative'}>
                <p className="tablePrimaryData">
                    {+coin.percent_change_24h}%
                    <TableTrendIcon trend={+coin.percent_change_24h}/>
                </p>
            </TableCell>
            <TableCell>

            </TableCell>

        </TableRow>
    );
};

//{Number(asset.totalProfitPercentage) > 0 ? 'arrow_drop_up' : 'arrow_drop_down'}

export default CoinDataRow;
