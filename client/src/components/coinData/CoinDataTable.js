/**
 * Created by dev on 3.8.17.
 */
import React from 'react';
import {connect} from 'react-redux';
import Table, {
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import * as Waypoint from 'react-waypoint';
import CoinDataRow from './CoinDataRow/CoinDataRow';
import {sortCoinsData} from '../../actions/currencies';
import './CoinDataTable.css';

const CoinDataTable = props => {

    const handleRequestSort = (orderBy) => {
        props.sortCoinsData({orderBy});
    };

    const load = () => {
        props.onLoadMore();
    };

    const tableHeadData = [
        {id: 'name', numeric: false, disablePadding: false, label: 'Currency', className: 'investmentsTableHead'},
        {id: 'price_usd', numeric: false, disablePadding: false, label: 'Price', className: 'investmentsTableHead'},
        {
            id: 'market_cap_usd',
            numeric: false,
            disablePadding: false,
            label: 'Market cap',
            className: 'investmentsTableHead'
        },
        {
            id: 'percent_change_24h',
            numeric: false,
            disablePadding: false,
            label: '24h change',
            className: 'investmentsTableHead'
        },
        {id: 'options', numeric: false, disablePadding: false, label: '', className: 'investmentsTableHead'}
    ];

    let coins = props.searchQuery ? props.searchResults :  props.coins;

    return (
        <div data-flex data-flex-grow>
            <Paper className="tablePaper">
                <Table>
                    <TableHead>
                        <TableRow>
                            {tableHeadData.map(column => {
                                return (
                                    <TableCell
                                        className={column.className}
                                        key={column.id}
                                        numeric={column.numeric}
                                        disablePadding={column.disablePadding}>
                                        {column.label}
                                    </TableCell>
                                );
                            })}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {coins.map((coin, index) => {
                            return (
                                <CoinDataRow coin={coin} key={index} index={index} currency={props.currency}/>
                            );
                        })}
                    </TableBody>
                    <Waypoint onEnter={props.searchQuery ? null : load}/>
                </Table>
            </Paper>
        </div>
    );

};

const mapStateToProps = (state, ownProps) => ({
    coins: state.rootReducer.currencies.coinData,
    searchResults: state.rootReducer.currencies.searchResults,
    searchQuery: state.rootReducer.currencies.searchQuery,
    sorting: state.rootReducer.currencies.coinSorting
});

const mapDispatchToProps = (dispatch) => ({
    sortCoinsData: payload => dispatch(sortCoinsData(payload))
});

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(CoinDataTable);