/**
 * Created by Виталий on 30.07.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    Link
} from 'react-router-dom';
import Scrollchor from 'react-scrollchor';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import NavUserInfo from './userInfo/UserInfo';
import {logout} from '../../actions/authActions';
import {toggleSidenav} from '../../actions/navActions';
import './NavBar.css';

class NavBar extends Component {

    constructor(props) {
        super(props);
        this.handleScroll = this.handleScroll.bind(this);
        this.state = {
            navbar: 'static'
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll() {
        let windowsScrollTop = window.pageYOffset;
        if (windowsScrollTop >= this.props.landingOffset) {
            this.setState({navbar: 'whiteAppBar'});
        } else {
            this.setState({navbar: 'primaryAppBar'});
        }
    }

    render() {
        if (!this.props.enabled) {
            return null;
        }

        const Appmenu = (
            <ul className="navMenu" data-flex data-layout="row" data-layout-align="end center">
                <li className="navMenuLink">
                    <Scrollchor to="">Home</Scrollchor>
                </li>
                <li className="navMenuLink">
                    <Scrollchor to="#features">Features</Scrollchor>
                </li>
                <li className="navMenuLink">
                    <Scrollchor to="#contact">Contact</Scrollchor>
                </li>
                <li className="navMenuLink">
                    <Link to="/login" className="loginLink">Login</Link>
                </li>
            </ul>
        );

        const LandingPageTemplate = (
            <AppBar position="fixed" ref={(ref) => this.navbar = ref} className={this.state.navbar}>
                <Toolbar>
                    <IconButton className="landingMenuToggle" color="contrast" aria-label="Menu"
                                onClick={this.props.toggleSidenav}>
                        <Icon className="material-icons">menu</Icon>
                    </IconButton>
                    <div className="appLogo">
                        <Icon className="material-icons">remove_red_eye </Icon>
                        Coin Overwatch
                    </div>
                    {Appmenu}
                    <div className="navMobileSpacer" data-flex/>
                    <Button color="contrast" className="navLoginButton">
                        <Link to="/login" className="navSignupLink">Login</Link>
                    </Button>
                    <Button color="contrast" className="navSignupButton">
                        <Link to="/signup" className="navSignupLink">Sign Up</Link>
                    </Button>

                </Toolbar>
            </AppBar>
        );

        const Template = (
            <AppBar position="fixed" className="appNavbar">
                <Toolbar>
                    <IconButton color="contrast" aria-label="Menu" onClick={this.props.toggleSidenav}>
                        <Icon className="material-icons">menu</Icon>
                    </IconButton>
                    <div className="appLogo">
                        <Icon className="material-icons">remove_red_eye </Icon>
                        Coin Overwatch
                    </div>
                    <div className="spacer"/>
                    {this.props.user ?
                        <NavUserInfo user={this.props.user} onLogout={this.props.logout}/>
                        :
                        <Button color="contrast" className="navSignupButton">
                            <Link to="/signup" className="navSignupLink">Signup</Link>
                        </Button>}

                </Toolbar>
            </AppBar>
        );

        return (
            this.props.isLanding ? LandingPageTemplate : Template
        )
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    user: state.rootReducer.auth.user,
    enabled: state.rootReducer.nav.enabled,
    isLanding: state.rootReducer.nav.isLanding
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    toggleSidenav: () => dispatch(toggleSidenav()),
    logout: () => dispatch(logout()),
});


// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(NavBar);