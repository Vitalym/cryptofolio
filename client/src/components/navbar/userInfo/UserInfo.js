/**
 * Created by dev on 29.8.17.
 */
import React, {Component} from 'react';
import Button from 'material-ui/Button';
import Gravatar from 'react-gravatar'
import Menu, {MenuItem} from 'material-ui/Menu';
import Icon from 'material-ui/Icon';
import {Link} from 'react-router-dom';
import './UserInfo.css';


class NavUserInfo extends Component {
    constructor(props) {
        super(props);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.handleRequestClose = this.handleRequestClose.bind(this);

        this.state = {
            anchorEl: undefined,
            open: false,
        };
    }

    toggleMenu (event) {
        this.setState({ open: true, anchorEl: event.currentTarget });
    }

    handleRequestClose () {
        this.setState({ open: false });
    };

    render() {
        return (
            <div>
                <div className="navUserInfo" data-flex data-layout="row" data-layout-align="end center" onClick={this.toggleMenu}>

                        <Gravatar email={this.props.user.email} size={30}/>
                        <span className="userName">{this.props.user.email ? this.props.user.email : this.props.user.displayName}</span>
                        <Icon className="material-icons">arrow_drop_down </Icon>



                </div>
                <Menu
                    anchorEl={this.state.anchorEl}
                    open={this.state.open}
                    onRequestClose={this.handleRequestClose}>
                    <MenuItem onClick={this.handleRequestClose}>
                        <Link to="/settings">Settings</Link>
                    </MenuItem>
                    <MenuItem onClick={this.props.onLogout}>Logout</MenuItem>
                </Menu>
            </div>

        )
    }
}

export default NavUserInfo;