/**
 * Created by dev on 24.8.17.
 */
import React from 'react';
import {connect} from 'react-redux';
import { hideMessage } from '../../actions/notifications';
import Snackbar from 'material-ui/Snackbar';

const AppSnackBar = props => {
    const { title, message, isOpen } = props;

    const onRequestClose = () => {
        props.hideMessage();
    };

    return (
        <div>
            <Snackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                open={isOpen}
                onRequestClose={onRequestClose}
                SnackbarContentProps={{
                    'aria-describedby': 'Notification',
                }}
                autoHideDuration={5000}
                message={props.message}
            />
        </div>

    );
};

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    isOpen: state.rootReducer.notifications.isOpen,
    title: state.rootReducer.notifications.title,
    message: state.rootReducer.notifications.message
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    hideMessage: () => dispatch(hideMessage())
});

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(AppSnackBar);