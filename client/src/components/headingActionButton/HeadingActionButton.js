/**
 * Created by dev on 7.8.17.
 */
import React from 'react';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import './HeadingActionButton.css';

const HeadingActionButton = props => (
    <Button raised={props.raised} color={props.color}
            href="/portfolio/add"
            className='actionButton'>
        <Icon className="material-icons">
            {props.icon || 'add'}
        </Icon>
        {props.text}
    </Button>
);

export default HeadingActionButton;