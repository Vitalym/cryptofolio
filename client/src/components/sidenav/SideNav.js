/**
 * Created by Виталий on 30.07.2017.
 */
import React from 'react';
import {connect} from 'react-redux';
import {Link, NavLink} from 'react-router-dom';
import Scrollchor from 'react-scrollchor';
import {toggleSidenav} from '../../actions/navActions';
import {logout} from '../../actions/authActions';
import Drawer from 'material-ui/Drawer';
import Icon from 'material-ui/Icon';
import List, {ListItem, ListItemIcon, ListItemText} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import './SideNav.css';

/*<Link className="menu-item" to="/alerts">
 <ListItem button>
 <ListItemIcon>
 <Icon className="material-icons">alarm</Icon>
 </ListItemIcon>
 <ListItemText primary="Alerts"/>
 </ListItem>
 </Link>*/

const SideNav = props => {

    const logout = () => {
        props.toggleSidenav();
        props.logout();
    };

    const landingMenu = (
        <div>
            <Scrollchor to="" className="menu-item"onClick={props.toggleSidenav}>
                <ListItem button>
                        <ListItemIcon>
                            <Icon className="material-icons">home</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Home"/>
                </ListItem>
            </Scrollchor>
            <Scrollchor to="#features" className="menu-item" onClick={props.toggleSidenav}>
                <ListItem button>
                        <ListItemIcon>
                            <Icon className="material-icons">show_chart</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Features"/>
                </ListItem>
            </Scrollchor>
            <Scrollchor className="menu-item" to="#contact" onClick={props.toggleSidenav}>
                <ListItem button>
                        <ListItemIcon>
                            <Icon className="material-icons">email</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Contact"/>
                </ListItem>
            </Scrollchor>
        </div>
    );

    if (props.isLanding) {
        return (
            <Drawer
                open={props.isOpen}
                onRequestClose={props.toggleSidenav}>
                <div className="drawerHeader" data-flex data-layout-align="center center">
                    <div className="navAppLogo">
                        <Icon className="material-icons">remove_red_eye </Icon>
                        Coin Overwatch
                    </div>
                </div>
                {landingMenu}
            </Drawer>
        )
    }

    const primaryMenu = (
        <div>
            <NavLink activeClassName="active" className="menu-item" to="/market" onClick={props.toggleSidenav}>
                <ListItem button>
                    <ListItemIcon>
                        <Icon className="material-icons">dashboard</Icon>
                    </ListItemIcon>
                    <ListItemText primary="Market"/>
                </ListItem>
            </NavLink>
            <NavLink activeClassName="active" className="menu-item" to="/portfolio" onClick={props.toggleSidenav}>
                <ListItem button>
                    <ListItemIcon>
                        <Icon className="material-icons">show_chart</Icon>
                    </ListItemIcon>
                    <ListItemText primary="Portfolio"/>
                </ListItem>
            </NavLink>
            <NavLink activeClassName="active" className="menu-item" to="/settings" onClick={props.toggleSidenav}>
                <ListItem button>
                    <ListItemIcon>
                        <Icon className="material-icons">settings</Icon>
                    </ListItemIcon>
                    <ListItemText primary="Settings"/>
                </ListItem>
            </NavLink>
        </div>
    );

    const secondaryMenu = (
        <div>
            <ListItem button onClick={logout}>
                <ListItemIcon>
                    <Icon className="material-icons">exit_to_app</Icon>
                </ListItemIcon>
                <ListItemText primary="Logout"/>
            </ListItem>
        </div>
    );

    const menu = (
        <div>
            <List className='menuList' disablePadding>
                {primaryMenu}
            </List>
            <Divider />
            <List className='menuList' disablePadding>
                {secondaryMenu}
            </List>
        </div>
    );

    const Template = (

        <Drawer
            open={props.isOpen}
            onRequestClose={props.toggleSidenav}>
            <div className="drawerHeader" data-flex data-layout-align="center center">
                <div className="navAppLogo">
                    <Icon className="material-icons">remove_red_eye </Icon>
                    Coin Overwatch
                </div>
            </div>
            {menu}
        </Drawer>
    );

    return (
        props.enabled ? Template : null
    );
};

const mapStateToProps = (state, ownProps) => ({
    enabled: state.rootReducer.nav.enabled,
    isOpen: state.rootReducer.nav.isOpen,
    isLanding: state.rootReducer.nav.isLanding
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    logout: () => dispatch(logout()),
    toggleSidenav: () => dispatch(toggleSidenav())
});

export default connect(mapStateToProps, mapDispatchToProps)(SideNav);
