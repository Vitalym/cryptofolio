import React from 'react';
import {connect} from 'react-redux';
import {setCurrency} from '../../actions/settings';
import { FormControlLabel } from 'material-ui/Form';
import Switch from 'material-ui/Switch';
import './CurrencySettingsSwitch.css';

const CurrencySettingsSwtitch = props => {

    const handleCurrencyChange = (event, value) => {
        let currency = value ? "USD" : "BTC";
        props.setCurrency(currency);
    };

    return (
        <div>
            <FormControlLabel
                control={
                    <Switch
                        checked={props.user && props.user.settings ? props.user.settings.currency === 'USD' : false }
                        disabled={props.isFetching}
                        onChange={(event, value) => handleCurrencyChange(event, value)}
                    />
                }
                label={props.user && props.user.settings ? props.user.settings.currency : 'USD'}
            />
        </div>
        )

};

const mapStateToProps = (state, ownProps) => ({
    isFetching: state.rootReducer.settings.isFetchingCurrencySettings
});

const mapDispatchToProps = (dispatch) => ({
    setCurrency: currency => dispatch(setCurrency(currency))
});

export default connect(mapStateToProps, mapDispatchToProps)(CurrencySettingsSwtitch);