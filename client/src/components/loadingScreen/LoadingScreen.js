/**
 * Created by Виталий on 30.08.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import Icon from 'material-ui/Icon';
import * as Spinner from 'react-spinkit';
import {navEnable} from '../../actions/navActions';
import './LoadingScreen.css'

class LoadingScreen extends Component {
//const LoadingScreen = props => {

    componentDidMount() {
        this.props.navEnable(false);
    }

    componentWillUnmount() {
        this.props.navEnable(true);
    }

    render () {
        return (
            <div className="loadingScreen" data-flex data-layout="column" data-layout-align="center center">
                <div className="loadingHeading">
                    <Icon className="material-icons">remove_red_eye </Icon>
                    Coin Overwatch
                </div>
                <Spinner name="pacman" color="white"/>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({

});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    navEnable: enable => dispatch(navEnable(enable))
});

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(LoadingScreen);

//export default LoadingScreen;