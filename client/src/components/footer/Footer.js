/**
 * Created by Виталий on 30.07.2017.
 */
import React from 'react';
import {Link} from 'react-router-dom';
import Icon from 'material-ui/Icon';
import './Footer.css';

let btcImg, ethImg;

try {
    btcImg = require(`../../assets/icons/Bitcoin.svg`);
} catch (e) {
    btcImg = null;
}
try {
    ethImg = require(`../../assets/icons/ETH.svg`);
} catch (e) {
    ethImg = null;
}

const Footer = props => {
    return (
        <div className="footer" data-flex data-layout="column">
            <div className="footerContent" data-flex data-layout="column" data-layout-gt-xs="row">
                <div className="footerContainer" data-flex="100" data-flex-gt-md="33" data-flex-gt-xs="40" data-layout="row" data-layout-align="start center">
                    <div className="footerCopyRight">
                        &#9400; 2017 <span className="companyName">Coin Overwatch</span>
                    </div>
                </div>
                <div className="footerContainer" data-flex="100" data-flex-gt-md="33" data-flex-gt-xs="60" data-layout="row" data-layout-gt-md="column" data-layout-align="center start">
                    <div data-flex="50" data-flex-gt-md="100">
                        <Link className="footerLink" to="/privacy">
                            Privacy Policy
                        </Link>
                    </div>
                    <div data-flex="50" data-flex-gt-md="100">
                        <Link className="footerLink" to="/about#contact">
                            Contact us
                        </Link>
                    </div>
                </div>
                <div className="footerContainer footerDonation"
                     data-flex="100"
                     data-flex-gt-xs="33"
                     data-layout="column"
                     data-layout-align="center start"
                     data-layout-align-gt-xs="start center">
                    <div className="donationContainer" data-flex data-layout="row" data-layout-align="start center">
                        <img src={btcImg} alt="Bitcoin" className="footerIcon" height="32" width="32"/>
                        1AsaM2t9aEqsHUZKFJpw434QQoEPjWr87U
                    </div>
                    <div className="donationContainer" data-flex data-layout="row" data-layout-align="start center">
                        <img src={ethImg} alt="Ethereum" className="footerIcon" height="32" width="32"/>
                        0x5dfea6174ec1ce577c1941e3c1f68e2ed304d536
                    </div>
                </div>
            </div>
            <div className="footerContent footerDonationMobile" data-flex data-layout="column" data-layout-gt-sm="row">
                <div className="donationContainer donationContainerMobile" data-flex="50" data-layout="row" data-layout-align="center center">
                    <img src={btcImg} alt="Bitcoin" className="footerIcon" height="24" width="24"/>
                    1AsaM2t9aEqsHUZKFJpw434QQoEPjWr87U
                </div>
                <div className="donationContainer donationContainerMobile" data-flex="50" data-layout="row" data-layout-align="center center">
                    <img src={ethImg} alt="Ethereum" className="footerIcon" height="24" width="24"/>
                    0x5dfea6174ec1ce577c1941e3c1f68e2ed304d536
                </div>
            </div>
        </div>
    );
};

export default Footer;
