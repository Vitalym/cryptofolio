/**
 * Created by dev on 18.9.17.
 */
import ReactGA from 'react-ga';

const trackEvent = store => next => action => {
    console.log('tracking...');
    ReactGA.event({
        category: 'Redux action',
        action: action.type
    });

    return next(action);
}