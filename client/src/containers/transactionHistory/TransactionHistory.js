/**
 * Created by Виталий on 15.08.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import InvestmentsHistory from '../../components/investments/investmentsHistory/InvestmentsHistory';
import PortfolioActionButton from '../../components/headingActionButton/HeadingActionButton';
import {sortHistory} from '../../actions/investments';
import './TransactionHistory.css';

class TransactionHistory extends Component {
    constructor(props) {
        super(props);
        this.handleSort = this.handleSort.bind(this);
    }

    componentDidMount() {

    }

    handleSort(order) {
        this.props.sortHistory(order);
    };

    render() {
        return (
            <div className="appContainer">
                <div className="investmentsContainer investmentsHoldingsContainer">
                    <div data-flex="100" data-layout-align="center stretch" className="layoutColumn">
                        <div className="headerContainer" data-flex data-layout="row"
                             data-layout-align="space-between">
                            <Typography type="headline" className="investmentsHeading">Transaction
                                history</Typography>
                            <div className='spacer'/>
                            <PortfolioActionButton text='Add Transaction' color='accent' raised={false}/>
                        </div>
                        <Paper>
                            <InvestmentsHistory
                                investments={this.props.investments}
                                onHandleSort={this.handleSort}
                                sorting={this.props.sorting}/>
                        </Paper>
                    </div>
                </div>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
    return {
        // You can now say this.props.books
        investments: state.rootReducer.investments.investments,
        sorting: state.rootReducer.investments.historySorting
    }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
    return {
        // You can now say this.props.createBook
        sortHistory: payload => dispatch(sortHistory(payload))
    }
};

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TransactionHistory));
