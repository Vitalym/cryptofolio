/**
 * Created by Виталий on 11.08.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {getHoldingsAllocation} from '../../actions/allocation';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import RebalanceActionButton from '../../components/rebalance/rebalanceActionButton/RebalanceActionButton';

//import HoldingsTable from '../../components/rebalance/holdingsTable/HoldingsTable';

import './AllocationSchema.css';

class AllocationSchema extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
       // this.props.getHoldingsAllocation();
    }

    render() {
        return (
            <div className="appContainer">
                <div className="headerContainer">
                    <div className="spacer"></div>
                    <RebalanceActionButton text='Add allocation schema' color='accent' raised={false}/>
                </div>
                <Paper>
                    
                </Paper>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
    return {
        // You can now say this.props.books
        user: state.rootReducer.auth.user,
        holdings: state.rootReducer.allocations.holdings
    }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
    return {
        // You can now say this.props.createBook
        getHoldingsAllocation: user => dispatch(getHoldingsAllocation())
    }
};

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AllocationSchema));