/**
 * Created by Виталий on 11.08.2017.
 */
import React from 'react';
import {Helmet} from 'react-helmet';
import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';
import BackButton from '../../components/backButton/BackButton';

import './About.css';

let mailImg, telegramImg, btcImg, ethImg, xmrImg, ltcImg;

//build image url
try {
    mailImg = require(`../../assets/icons/Gmail.svg`);
} catch (e) {
    mailImg = null;
}
try {
    telegramImg = require(`../../assets/icons/telegram.svg`);
} catch (e) {
    telegramImg = null;
}
try {
    btcImg = require(`../../assets/icons/Bitcoin.svg`);
} catch (e) {
    btcImg = null;
}
try {
    ethImg = require(`../../assets/icons/ETH.svg`);
} catch (e) {
    ethImg = null;
}
try {
    ltcImg = require(`../../assets/icons/LTC.svg`);
} catch (e) {
    ltcImg = null;
}
try {
    xmrImg = require(`../../assets/icons/XMR.svg`);
} catch (e) {
    xmrImg = null;
}

const About = props => {

    return (
        <div className="appContainer">
            <Helmet>
                <title>Coin Overwatch | About</title>
            </Helmet>
            <div className="headerContainer">
                <Typography type="display1" className="investmentsHeading">About Coin Overwatch</Typography>
                <div className="spacer"/>
                <BackButton href={'/portfolio'}/>
            </div>

            <div className="aboutContainer">
                <div className="aboutTextContainer">
                    <span className="aboutCompanyName">Coin Overwatch</span> is a portfolio tracker for cryptocurrencies
                    that allows you to analyze your portfolio performance,
                    track your favorite currencies and help you rebalance portfolio from time to time. It supports more
                    then 1000 currencies,
                    including most of the coins that are listed on at list on major exchange. <span className="aboutCompanyName">Coin Overwatch</span> uses data
                    from <a href="https://coinmarketcap.com/" className="aboutLink">coinmarketcap</a> and <a
                    href="http://coincap.io/" className="aboutLink">coincap.io</a>.
                    Data is being updated every 10 minutes.
                </div>

                <Typography type="headline" className="aboutHeading">Features</Typography>
                <div className="underline">
                    <Icon color="primary">
                        assessment
                    </Icon>
                </div>

                <div className="aboutTextContainer">
                    <div className="aboutFeatureRow" data-flex data-layout="column" data-layout-gt-md="row">
                        <div className="aboutFeatureColumn" data-flex="50" data-layout="column">
                            <div className="aboutFeatureContent">
                                <div className="aboutFeatureSubtitle">
                                    Portfolio Management
                                </div>
                                <div className="aboutFeatureText">
                                    Keep track of your assets, their price change and value. You can bind your
                                    transactions to your Bitcoin balance, so that buying or selling altcoins can
                                    add/withdraw your Bitcoin funds
                                </div>
                            </div>
                        </div>
                        <div className="aboutFeatureColumn" data-flex="50" data-layout="column">
                            <div className="aboutFeatureContent">
                                <div className="aboutFeatureSubtitle">
                                    Portfolio Performance
                                </div>
                                <div className="aboutFeatureText">
                                    Get overall portfolio performance over time calculated in US Dollars or BTC, or
                                    track how each investment performed over time compared to dollars or bitcons.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="aboutFeatureRow" data-flex data-layout="column" data-layout-gt-md="row">
                        <div className="aboutFeatureColumn" data-flex="50" data-layout="column">
                            <div className="aboutFeatureContent">
                                <div className="aboutFeatureSubtitle">
                                    Cryptomarket overview
                                </div>
                                <div className="aboutFeatureText">
                                    Keep your hand on cryptomarket pulse and check coin prices and market cap change
                                    right in the app. Create a shortlist of currencies you watch to make them appear on
                                    top.
                                </div>
                            </div>
                        </div>
                        <div className="aboutFeatureColumn" data-flex="50" data-layout="column">
                            <div className="aboutFeatureContent">
                                <div className="aboutFeatureSubtitle">
                                    Portfolio rebalancing
                                </div>
                                <div className="aboutFeatureText">
                                    Set the desired level of asset allocation for each coin and track how it's share
                                    changes over time. Coin Overwatch will calculate how much should you sell or buy to
                                    restore the desired balance.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="aboutTextContainer">
                    <span className="aboutCompanyName">Coin Overwatch</span> is currently in Beta, so please don't get
                    mad if you spot any bug. If you found anything that is not working properly, or if you think
                    that there is any feature that <span className="aboutCompanyName">Coin Overwatch</span> really needs,
                    do not hesitate to contact me.
                    There are more features coming in the nearest future. The next to come are Android/Iphone apps,
                    price alerts and localization,
                    You can view the roadmap <a href="https://trello.com/b/yDRUtHXE/coinoverwatch-roadmap" className="aboutLink">here</a>.
                    Drop me a message if you think it's missing something important.
                </div>

                <Typography type="headline" className="aboutHeading"> Meet the author</Typography>
                <div className="underline">
                    <Icon color="primary">
                        mood
                    </Icon>
                </div>

                <div className="aboutTextContainer">
                    I'm Vitaly, JS Developer at day and crypto enthusiast at night, so this was my chance to combine
                    this two passions into one product.
                    When I started building it, I was missing a portfolio that can help you calculate assets
                    rebalancing, have most of the available coins and can not
                    only add and sell them, but do it with BTC balances you already have.
                </div>
                <div className="aboutTextContainer">
                    I've created a tool that I will be happy to use myself, and I'm planning to improve it all the time.
                    If you would like to contribute in any way, share some ideas,
                    give a feedback or just chat with me, reach me out!
                </div>

                <Typography type="headline" className="aboutHeading" id="contact"> Contacts</Typography>
                <div className="underline">
                    <Icon color="primary">
                        chat
                    </Icon>
                </div>

                <div className="aboutTextContainer">
                    <div className="aboutFeatureRow" data-flex data-layout="column" data-layout-gt-md="row">
                        <div className="aboutFeatureColumn" data-flex="50" data-layout="column">
                            <div className="aboutFeatureContent aboutContactContent">
                                <div className="aboutContact">
                                    <div data-flex data-layout="row" data-layout-align="start center">
                                        <img src={mailImg} alt="Telegram" className="contactIcon" height="32"
                                             width="32"/>
                                        <a href="mailto:vitaliy.maltsev@gmail.com" className="contactLink">
                                            vitaliy.maltsev@gmail.com
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="aboutFeatureColumn" data-flex="50" data-layout="column">
                            <div className="aboutFeatureContent aboutContactContent">
                                <div className="aboutContact" data-flex data-layout="row"
                                     data-layout-align="start center">
                                    <img src={telegramImg} alt="Telegram" className="contactIcon" height="32"
                                         width="32"/>
                                    <a href="https://t.me/decipher1" className="contactLink">
                                        @decipher1
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="aboutTextContainer">
                    Coin Overwatch is completely free, but if you enjoy using it and would like to buy me a beer or a cup
                    of coffee, you can use any of the following donation addresses.
                    Lambos are also accepted :)
                </div>

                <Typography type="headline" className="aboutHeading"> Donate</Typography>
                <div className="underline">
                    <Icon color="primary">
                        attach_money
                    </Icon>
                </div>

                <div className="aboutTextContainer">
                    <div className="aboutFeatureRow" data-flex data-layout="column" data-layout-gt-md="row">
                        <div className="aboutFeatureColumn" data-flex="50" data-layout="column">
                            <div className="aboutFeatureContent aboutContactContent">
                                <div className="aboutContact" data-flex data-layout="row"
                                     data-layout-align="start center">
                                    <img src={btcImg} alt="Bitcoin" className="contactIcon" height="32" width="32"/>
                                    1AsaM2t9aEqsHUZKFJpw434QQoEPjWr87U
                                </div>
                            </div>
                        </div>
                        <div className="aboutFeatureColumn" data-flex="50" data-layout="column">
                            <div className="aboutFeatureContent aboutContactContent">
                                <div className="aboutContact" data-flex data-layout="row"
                                     data-layout-align="start center">
                                    <img src={ethImg} alt="Ethereum" className="contactIcon" height="32" width="32"/>
                                    0x5dfea6174ec1ce577c1941e3c1f68e2ed304d536
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="aboutFeatureRow" data-flex data-layout="column" data-layout-gt-md="row">
                        <div className="aboutFeatureColumn" data-flex="50" data-layout="column">
                            <div className="aboutFeatureContent aboutContactContent">
                                <div className="aboutContact" data-flex data-layout="row"
                                     data-layout-align="start center">
                                    <img src={ltcImg} alt="Litecoin" className="contactIcon" height="32" width="32"/>
                                    LZTr8CqKjVXmwZdu5W8xayFAR3YpCPPc8h
                                </div>
                            </div>
                        </div>
                        <div className="aboutFeatureColumn" data-flex="50" data-layout="column">
                            <div className="aboutFeatureContent aboutContactContent">
                                <div className="aboutContact" data-flex data-layout="row"
                                     data-layout-align="start center">
                                    <img src={xmrImg} alt="Monero" className="contactIcon" height="32" width="32"/>
                                    xmr
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    );
};

// Use connect to put them together
export default About;