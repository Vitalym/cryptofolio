/**
 * Created by Виталий on 23.07.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Helmet} from "react-helmet";
import Typography from 'material-ui/Typography';
import LoadingScreen from '../../components/loadingScreen/LoadingScreen';
import CoinDataTable from '../../components/coinData/CoinDataTable';
import CoinDataMobile from '../../components/coinDataMobile/CoinDataMobile';
import CoinSearch from '../../components/coinData/coinSearch/CoinSearch';
import {getCoinData} from '../../actions/currencies';
import './Market.css';

class CoinDataDashboard extends Component {
    constructor(props) {
        super(props);
        this.loadCoinData = this.loadCoinData.bind(this);
        this.handleTabChange = this.handleTabChange.bind(this);
        this.skip = 0;
        this.limit = 50;
        this.state = {
            value: 0, //todo move to redux
            index: 1,
            width: window.innerWidth,
        };
    }

    componentWillMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }

    componentDidMount() {
        this.props.getCoinData(this.skip, this.limit);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({width: window.innerWidth});
    };

    loadCoinData() {
        if (this.props.coins.length > this.skip) {
            this.skip++;
            this.props.getCoinData(this.skip, this.limit);
        }
    }

    handleTabChange = (event, value) => {
        this.setState({value});
    };

    render() {
       /* if (this.props.isFetching) {
            return (
                <LoadingScreen/>
            )
        }*/

        const {index, width} = this.state;
        const isMobile = width <= 820;
        const isMobileSmallScreen = width <= 510;
        return (
            <div className="appContainer">
                <Helmet>
                    <title>Coin Overwatch | Market</title>
                </Helmet>
                <div className="headerContainer" data-flex data-layout="row" data-layout-align="space-between">
                    <Typography type="headline" className="investmentsHeading">Market overview</Typography>
                    <div className='spacer'/>
                    <CoinSearch/>
                </div>
                {isMobile ?
                    <CoinDataMobile onLoadMore={this.loadCoinData}
                                    isMobile={isMobile}
                                    isMobileSmallScreen={isMobileSmallScreen}
                                    currency={this.props.user ? this.props.user.settings.currency : 'USD'}/>
                    : <CoinDataTable onLoadMore={this.loadCoinData} isMobile={isMobile}
                                     currency={this.props.user ? this.props.user.settings.currency : 'USD'}/>}
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    coins: state.rootReducer.currencies.coinData,
    user: state.rootReducer.auth.user,
    isFetching: state.rootReducer.currencies.isFetchingCoinData
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    getCoinData: (skip, limit) => dispatch(getCoinData(skip, limit))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CoinDataDashboard));