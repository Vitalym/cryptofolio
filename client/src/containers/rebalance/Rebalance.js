/**
 * Created by Виталий on 11.08.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Helmet} from "react-helmet";
import {getHoldingsAllocation, toggleDialog, saveAllocationSchema} from '../../actions/allocation';
import LoadingScreen from '../../components/loadingScreen/LoadingScreen';
import Typography from 'material-ui/Typography';
import AllocationsDialog from '../../components/rebalance/allocationsDialog/AllocationsDialog';
import HideZeroBalancesButton from '../../components/rebalance/hideZeroBalancesButton/HideZeroBalancesButton'
import RebalanceActionButton from '../../components/rebalance/rebalanceActionButton/RebalanceActionButton';
import HoldingsTable from '../../components/rebalance/holdingsTable/HoldingsTable';
import HoldingsTableMobile from '../../components/rebalance/holdingsTableMobile/HoldingsTableMobile';
import {setZeroBalanceSettings} from '../../actions/settings';

import './Rebalance.css';

class Rebalance extends Component {
    constructor(props) {
        super(props);
        this.toggleDialog = this.toggleDialog.bind(this);
        this.saveAllocationsTable = this.saveAllocationsTable.bind(this);
        this.handleSwitch = this.handleSwitch.bind(this);
        this.state = {
            width: window.innerWidth,
            columnWidth: null
        };
    }

    componentDidMount() {
        this.props.getHoldingsAllocation();
        this.getColumnWidth ();
    }

    componentWillMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({width: window.innerWidth});
        this.getColumnWidth ();

    };

    toggleDialog() {
        return this.props.toggleDialog();
    }

    saveAllocationsTable() {
        this.props.saveAllocationSchema(this.props.allocationSchemaFields);
    }

    handleSwitch(event, value) {
        this.props.setZeroBalanceSettings(value);
    }

    getColumnWidth () {
        if (this.state.width <= 590) {
            this.setState({columnWidth: 33});
            return 33;
        } else if (this.state.width <= 720){
            this.setState({columnWidth: 25});
            return 25;
        } else {
            this.setState({columnWidth: null});
        }
    };

    render() {
        if (this.props.isFetching) {
            return (
                <LoadingScreen/>
            )
        }

        const {width} = this.state;
        const isMobile = width <= 1060;
        const isSmallScreen = width <= 1260;
        const displayOptions = width <= 720; // when we will show options on click

        return (
            <div className="appContainer">
                <Helmet>
                    <title>Coin Overwatch | Rebalance</title>
                </Helmet>
                <div className="headerContainer">
                    <Typography type="headline" className="investmentsHeading">
                        Allocation
                    </Typography>
                    <div className="spacer"/>
                    <RebalanceActionButton text='Set allocation schema' color='accent' raised={false}
                                           onButtonClick={this.toggleDialog}/>
                </div>
                <div>
                    <HideZeroBalancesButton checked={this.props.user.settings.hideZeroBalances}
                                            onChange={this.handleSwitch}/>
                </div>

                <div data-flex='100' data-layout-align="center stretch"  className={isMobile ? "layoutColumn noPadding noShadow": "layoutColumn noPadding"}>
                    <div data-flex data-layout="column" data-layout-align="center stretch" data-flex-grow
                         className="paperFullHeight noPadding">
                        {isMobile ? <HoldingsTableMobile user={this.props.user}
                                                         displayOptions={displayOptions}
                                                         columnWidth={this.state.columnWidth}/> :
                            <HoldingsTable user={this.props.user}
                                           isSmallScreen={isSmallScreen}/>}
                    </div>
                </div>
                <AllocationsDialog active={this.props.isOpen}
                                   onSave={this.saveAllocationsTable}
                                   onDialogToggle={this.toggleDialog}/>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    user: state.rootReducer.auth.user,
    holdings: state.rootReducer.allocations.holdings,
    allocationSchemaFields: state.rootReducer.allocations.allocationSchemaFields,
    isOpen: state.rootReducer.allocations.dialogIsOpen,
    isFetching: state.rootReducer.allocations.isFetching,
    isFetchingBalanceSettings: state.rootReducer.settings.isFetchingBalanceSettings
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    getHoldingsAllocation: () => dispatch(getHoldingsAllocation()),
    toggleDialog: () => dispatch(toggleDialog()),
    saveAllocationSchema: fields => dispatch(saveAllocationSchema(fields)),
    setZeroBalanceSettings: (value) => dispatch(setZeroBalanceSettings(value))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Rebalance));