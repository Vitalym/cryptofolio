/**
 * Created by Виталий on 23.07.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom'
import {Helmet} from 'react-helmet';
import {resetUserPassword} from '../../../actions/authActions';
import {navEnable} from '../../../actions/navActions';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import PasswordResetForm from '../../../components/auth/passwordResetForm/PasswordResetForm';
import {Link} from 'react-router-dom';

import '../Auth.css';
import './PasswordReset.css';

class RequestPasswordReset extends Component {
    constructor(props) {
        super(props);
        this.resetUserPassword = this.resetUserPassword.bind(this);

        this.state = {
            isFetching: false
        };
    }

    componentDidMount() {
        this.props.navEnable(false);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isFetching !== this.state.isFetching) {
            this.setState({
                isFetching: nextProps.isFetching
            });
        }
    }

    resetUserPassword(password) {
        this.setState({
            isFetching: true
        });
        password.resetToken = this.props.match.params.token;
        this.props.resetUserPassword(password);
    }

    render() {
        return (
            <div className="auth-container">
                <div className="auth-form-container" data-layout="column" data-layout-align="center center">
                    <div className="authLogoContainer">
                        Coin Overwatch
                    </div>
                    <Paper className="authFormPaper">
                        <Typography type="title" className="authHeading">Enter a new password</Typography>
                        <PasswordResetForm onSubmit={this.resetUserPassword} isFetching={this.state.isFetching}/>
                    </Paper>
                </div>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    token: state.rootReducer.auth.token,
    error: state.rootReducer.auth.error,
    isFetching: state.rootReducer.auth.isFetching
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    resetUserPassword: password => dispatch(resetUserPassword(password)),
    navEnable: enable => dispatch(navEnable(enable))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RequestPasswordReset));