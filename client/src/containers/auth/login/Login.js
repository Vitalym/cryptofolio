/**
 * Created by Виталий on 23.07.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom'
import {Helmet} from 'react-helmet';
import {login} from '../../../actions/authActions';
import {navEnable} from '../../../actions/navActions';
import Paper from 'material-ui/Paper';
import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';
import LoginForm from '../../../components/auth/loginForm/LoginForm';
import FacebookLogin from '../../../components/auth/facebook/FacebookLogin';
import {Link} from 'react-router-dom';

import '../Auth.css';
import './Login.css';

class Login extends Component {
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);

        this.state = {
            isFetching: false
        };
    }

    componentDidMount() {
        this.props.navEnable(false);
    }

    componentWillUnmount() {
        this.props.navEnable(true);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isFetching !== this.state.isFetching) {
            this.setState({
                isFetching: nextProps.isFetching
            });
        }
    }

    login(user) {
        this.setState({
            isFetching: true
        });
        this.props.login(user);
    }

    render() {
        return (
            <div className="auth-container">
                <Helmet>
                    <title>Coin Overwatch | Login</title>
                </Helmet>
                <div className="auth-form-container" data-layout="column" data-layout-align="center center">
                    <div className="authLogoContainer">
                        <Icon className="material-icons">remove_red_eye </Icon>
                        Coin Overwatch
                    </div>
                    <Paper className="authFormPaper">
                        <Typography type="title" className="authHeading">Login to your account</Typography>
                        <LoginForm onSubmit={this.login} isFetching={this.state.isFetching}/>
                        <div className="authSocialDivider">OR</div>
                        <FacebookLogin/>
                        <div data-flex="100" className="authSwitcher" data-layout="row" data-layout-align="center center">
                            Don't have an account yet? &nbsp;<Link className="authSwitcherLink" to="/signup">Sign up!</Link>
                        </div>
                    </Paper>
                </div>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    token: state.rootReducer.auth.token,
    error: state.rootReducer.auth.error,
    isFetching: state.rootReducer.auth.isFetching
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    login: user => dispatch(login(user)),
    navEnable: enable => dispatch(navEnable(enable))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));