/**
 * Created by Виталий on 23.07.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom'
import {Helmet} from 'react-helmet';
import {requestReset} from '../../../actions/authActions';
import {navEnable} from '../../../actions/navActions';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import RequestResetForm from '../../../components/auth/requestResetForm/RequestResetForm';
import {Link} from 'react-router-dom';

import '../Auth.css';
import './RequestPasswordReset.css';

class RequestPasswordReset extends Component {
    constructor(props) {
        super(props);
        this.requestReset = this.requestReset.bind(this);

        this.state = {
            isFetching: false
        };
    }

    componentDidMount() {
        this.props.navEnable(false);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isFetching !== this.state.isFetching) {
            this.setState({
                isFetching: nextProps.isFetching
            });
        }
    }

    requestReset(email) {
        this.setState({
            isFetching: true
        });
        this.props.requestReset(email);
    }

    render() {
        return (
            <div className="auth-container">
                <Helmet>
                    <title>Coin Overwatch | Reset Password</title>
                    <meta name="description" content="Coin Overwatch | Reset Password" />
                </Helmet>
                <div className="auth-form-container" data-layout="column" data-layout-align="center center">
                    <div className="authLogoContainer">
                        Coin Overwatch
                    </div>
                    <Paper className="authFormPaper">
                        <Typography type="title" className="authHeading">Forgot your password?</Typography>
                        <Typography type="subheading" className="authSubHeading">Enter the email that you used for signing up, and we will send you the instructions</Typography>
                        <RequestResetForm onSubmit={this.requestReset} isFetching={this.state.isFetching}/>
                        <div data-flex="100" className="authSwitcher" data-layout="row" data-layout-align="center center">
                            No need to reset a password? &nbsp;<Link className="authSwitcherLink" to="/login">Log in</Link> &nbsp;to your account instead
                        </div>
                    </Paper>
                </div>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    token: state.rootReducer.auth.token,
    error: state.rootReducer.auth.error,
    isFetching: state.rootReducer.auth.isFetching
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    requestReset: email => dispatch(requestReset(email)),
    navEnable: enable => dispatch(navEnable(enable))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RequestPasswordReset));