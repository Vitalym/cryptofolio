/**
 * Created by Виталий on 23.07.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Container, Header} from 'semantic-ui-react';
import {verify} from '../../../actions/authActions';

import '../Auth.css';
import './Verify.css';

class VerifyEmail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            status: `Verifying...`,
            token: this.props.match.params.token
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.verificationStatus !== this.props.verificationStatus) {
            if(nextProps.verificationStatus === 'failed') {
                this.setState({
                    status: `This verification token is incorrect. Please verify your email by clicking the link we've sent to your email!`
                });
            } else if (nextProps.verificationStatus === 'success'){
                this.setState({
                    status: `Redirecting...`
                });
                this.props.history.push('/login');
            }
           
        }
    }

    componentDidMount() {
        if (!this.state.token) {
            this.setState({
                status: `Please verify your email by clicking the link we've sent to your email!`
            });
        } else {
            this.props.verify(this.state.token);
        }
    }

    render() {
        return (
            <Container textAlign='center' className="auth-container">
                <Container className="auth-form-container">
                    <Container textAlign='center'>
                        <Header as='h2'>{this.state.status}</Header>
                    </Container>
                </Container>
            </Container>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
    return {
        verificationStatus: state.rootReducer.auth.verificationStatus
    }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
    return {
        verify: user => dispatch(verify(user))
    }
};


// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(VerifyEmail);