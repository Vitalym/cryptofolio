/**
 * Created by Виталий on 23.07.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import { signup } from '../../../actions/authActions';
import { navEnable } from '../../../actions/navActions';
import Paper from 'material-ui/Paper';
import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';
import SignupForm from '../../../components/auth/signupForm/SignupForm';
import FacebookLogin from '../../../components/auth/facebook/FacebookLogin';
import {Link} from 'react-router-dom';

import '../Auth.css';
import './Signup.css';

class Signup extends Component {
    constructor(props) {
        super(props);
        this.signup = this.signup.bind(this);
        this.state = {
            isFetching: this.props.isFetching
        };
    }

    componentDidMount() {
        this.props.navEnable(false);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.isFetching !== this.state.isFetching){
            this.setState({
                isFetching: nextProps.isFetching
            });
        }
    }

    signup (user) {
        this.setState({
            isFetching: true
        });
        this.props.signup(user);
    }

    render() {
        return (
        <div className="auth-container">
            <div className="auth-form-container" data-layout="column" data-layout-align="center center">
                <Helmet>
                    <title>Coin Overwatch | Signup</title>
                </Helmet>
                <div className="authLogoContainer">
                    <Icon className="material-icons">remove_red_eye </Icon>
                    Coin Overwatch
                </div>
                <Paper className="authFormPaper">
                    <Typography type="title" className="authHeading">Create new account</Typography>
                    <SignupForm onSubmit={this.signup} isFetching={this.state.isFetching}/>
                    <div className="authSocialDivider">OR</div>
                    <FacebookLogin/>
                    <div data-flex="100" className="authSwitcher" data-layout="row" data-layout-align="center center">
                       Already have an account? &nbsp;<Link className="authSwitcherLink" to="/login">Login!</Link>
                    </div>
                </Paper>
            </div>
        </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
    return {
        user: state.rootReducer.auth.user,
        error: state.rootReducer.auth.error,
        isFetching: state.rootReducer.auth.isFetching
    }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
    return {
        signup: user => dispatch(signup(user)),
        navEnable: enable => dispatch(navEnable(enable))
    }
};

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(Signup);