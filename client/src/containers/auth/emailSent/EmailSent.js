/**
 * Created by Виталий on 23.07.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import Paper from 'material-ui/Paper';
import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';
import { navEnable } from '../../../actions/navActions';

import '../Auth.css';
import './EmailSent.css';

class EmailSent extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.navEnable(false);
    }

    render() {
        let email = null;
        if (this.props.user) {
            email = this.props.user.email
        } else {
            email = 'your email'
        }
        return (
        <div className="auth-container">
            <Helmet>
                <title>Coin Overwatch | Email Sent</title>
            </Helmet>
            <div className="auth-form-container" data-layout="column" data-layout-align="center center">
                <div className="authLogoContainer">
                    <Icon className="material-icons">remove_red_eye </Icon>
                    Coin Overwatch
                </div>
                <Paper className="authFormPaper">
                    <Typography type="title" className="authHeading">lmost there!</Typography>
                    <p>Please check {email} to verify your account!</p>
                </Paper>
            </div>
        </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
    return {
        user: state.rootReducer.auth.user
    }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
    return {
        navEnable: enable => dispatch(navEnable(enable))
    }
};

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(EmailSent);