/**
 * Created by Виталий on 11.08.2017.
 */
import React from 'react';
import {Helmet} from 'react-helmet';
import Typography from 'material-ui/Typography';
import BackButton from '../../components/backButton/BackButton';

import './Privacy.css';

const Privacy = props => {

    return (
        <div className="appContainer">
            <Helmet>
                <title>Coin Overwatch | Privacy Policy</title>
            </Helmet>
            <div className="headerContainer">
                <Typography type="display1" className="investmentsHeading">Privacy Policy</Typography>
                <div className="spacer"/>
                <BackButton href={'/portfolio'}/>
            </div>

            <div className="aboutContainer">
                <div className="aboutTextContainer">
                    We protect any information that you give to <span className="aboutCompanyName">Coin Overwatch</span>. We are commited to ensuring that your privacy is protected.
                    We may update this page as the project develops, so you should check this page from time to time to ensure that you are happy with any changes.
                </div>
            </div>

            <div className="aboutContainer">
                <div className="aboutTextContainer">
                    Out of your personal data, we require only your email, that will be used only so that you can restore your password. We will not share it with anyone and we
                    will not send you any emails, except of password reset. We may add additional mailing alerts in the future.
                </div>
            </div>

            <div className="aboutContainer">
                <div className="aboutTextContainer">
                    We store your email and your password encrypted, but it is strongly recommended that you use different passwords for different website and applications. It is
                    specifically advised to create a <span className="aboutCompanyName">Coin Overwatch</span> password that is not used on any wallets, exchanges, etc.
                </div>
            </div>

            <div className="aboutContainer">
                <div className="aboutTextContainer aboutTextContainerImportant">
                    We will provide you a notice if our private policy changes, but please check this page from time to time to see
                    if you are happy with any changes.
                </div>
            </div>
        </div>
    );
};

// Use connect to put them together
export default Privacy;