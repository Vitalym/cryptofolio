import React, {Component} from 'react';
import {
    Route, Switch
} from 'react-router-dom';
import {Helmet} from "react-helmet";
import ReactGA from 'react-ga';
import history  from './../../history';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import purple from 'material-ui/colors/deepPurple';
import red from 'material-ui/colors/red';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';
import '../../react-toolbox/theme.css';
import toolboxTheme from  '../../react-toolbox/theme';
import './App.css';

import {requireAuthentication} from '../../components/authenticatedComponent/AuthenticatedComponent';
import NotificationComponent from '../../components/notificationComponent/NotificationComponent';
import NavBar from '../../components/navbar/NavBar';
import SideNav from '../../components/sidenav/SideNav';
import Footer from '../../components/footer/Footer';
import AppSnackBar from '../../components/snackbar/SnackBar';
import Main from './../main/Main';
import CoinDataDashboard from './../market/Market';
import Settings from './../settings/Settings';
import About from './../about/About';
import Privacy from './../privacy/Privacy';
import NoMatch from './../noMatch/NoMatch';

//auth
import Signup from './../auth/signup/Signup';
import Login from './../auth/login/Login';
import EmailSent from './../auth/emailSent/EmailSent';
import VerifyEmail from './../auth/verify/Verify';
import RequestPasswordReset from './../auth/requestPasswordReset/RequestPasswordReset';
import {requestResetComponent} from '../../components/auth/requestResetComponent';
import PasswordReset from './../auth/passwordReset/PasswordReset';
import {passwordResetComponent} from '../../components/auth/passwordResetComponent';

//investments
import {investmentsComponent} from '../../components/investments/InvestmentsComponent';
import Investments from '../investments/Investments';
import AddInvestment from '../investments/addInvestment/AddInvestment';
import EditInvestment from '../investments/editInvestment/EditInvestment';
import TransactionsHistory from '../transactionHistory/TransactionHistory';
import {fetchTransaction} from '../../components/investments/fetchTransactionComponent/FetchTransactionComponent';
import {coinTransactionComponent} from '../../components/coinTransaction/coinTransactionComponent';
import CoinTransactionHistory from '../coinTransactionHistory/CoinTransactionHistory';

//rebalance
import Rebalance from '../rebalance/Rebalance';

const theme = createMuiTheme({
    palette: {
        primary: purple,
        accent: {
            ...red
        },
        secondary: {
            ...red
        },
        error: red,
        typography: {
            fontFamily: 'Roboto, "Helvetica Neue",Arial,sans-serif'
        },
        "text": {
            "primary": "rgba(33, 33, 33, 1)",
            "secondary": "#757575",
            "disabled": "rgba(0, 0, 0, 0.38)",
            "hint": "rgba(0, 0, 0, 0.38)",
            "icon": "rgba(0, 0, 0, 0.38)",
            "divider": "rgba(0, 0, 0, 0.12)",
            "lightDivider": "rgba(0, 0, 0, 0.075)"
        }
    }
});

ReactGA.initialize('UA-106585562-1');

history.listen((location, action) => {
    ReactGA.set({ page: location.pathname });
    ReactGA.pageview(location.pathname);
});

class App extends Component {

    constructor() {
        super();

        this.setLandingOffset = this.setLandingOffset.bind(this);

        this.state = {
            loading: true,
            landingOffset: 0
        };
    }

    componentDidMount() {
        setTimeout(() => this.setState({ loading: false }));
    }

    setLandingOffset (offset) {
        this.setState({landingOffset: offset})
    }

    render() {
        //const { loading } = this.state;
        return (
            <MuiThemeProvider theme={theme}>
                <ThemeProvider theme={toolboxTheme}>
                <main>
                    <Helmet>
                        <title>Coin Overwatch</title>
                    </Helmet>
                    <NotificationComponent/>
                    <NavBar landingOffset={this.state.landingOffset}/>
                    <SideNav/>
                    <Switch data-flex data-layout-fill>
                        <Route exact path="/" render={(props) => <Main onSetOffset={this.setLandingOffset} {...props}/>}/>
                        <Route path="/signup" component={Signup}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/forgot-password" component={requestResetComponent(RequestPasswordReset)}/>
                        <Route exact path="/reset/:token" component={passwordResetComponent(PasswordReset)}/>
                        <Route path="/reset" component={passwordResetComponent(PasswordReset)}/>
                        <Route path="/email-sent" component={EmailSent}/>
                        <Route exact path="/verify/:token" component={VerifyEmail}/>
                        <Route path="/verify" component={VerifyEmail}/>
                        <Route path="/market" component={requireAuthentication(CoinDataDashboard)}/>
                        <Route path="/settings" component={requireAuthentication(Settings)}/>
                        <Route exact path="/portfolio"
                               component={requireAuthentication(investmentsComponent(Investments))}/>
                        <Route exact path="/portfolio/history"
                               component={requireAuthentication(investmentsComponent(TransactionsHistory))}/>
                        <Route exact path="/portfolio/history/:id"
                               component={requireAuthentication(coinTransactionComponent(CoinTransactionHistory))}/>
                        <Route exact path="/portfolio/add" component={requireAuthentication(AddInvestment)}/>
                        <Route exact path="/portfolio/edit/:id" component={requireAuthentication(fetchTransaction(EditInvestment))}/>
                        <Route exact path="/portfolio/rebalance" component={requireAuthentication(Rebalance)}/>
                        <Route path="/about" component={About}/>
                        <Route path="/privacy" component={Privacy}/>
                        <Route component={NoMatch}/>
                    </Switch>
                    <AppSnackBar/>
                    <Footer/>
                </main>
                </ThemeProvider>
            </MuiThemeProvider>
        );
    }
}

export default App;
