/**
 * Created by Виталий on 25.07.2017.
 */
import React from 'react';
import {Helmet} from 'react-helmet';
import Typography from 'material-ui/Typography';
import './NoMatch.css';

const NoMatch = props => (
    <div className="appContainer viewMessageContainer" data-flex data-layout="column" data-layout-align="center center">
        <Helmet>
            <title>Coin Overwatch | Not found</title>
        </Helmet>
        <Typography type="headline" className="messageHeading notFoundHeading">404</Typography>
        <Typography type="subheading" className="messageSubHeading">It's not a page you are looking for</Typography>
    </div>
);

export default NoMatch;



