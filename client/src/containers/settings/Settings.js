/**
 * Created by Виталий on 11.08.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Helmet} from "react-helmet";
import {setCurrency, setEmail, setPassword} from '../../actions/settings';
import CurrencySettings from '../../components/settings/currency/Currency';
import EmailSettings from '../../components/settings/email/Email';
import PasswordSettings from '../../components/settings/password/Password';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import BackButton from '../../components/backButton/BackButton';

import './Settings.css';

class Settings extends Component {
    constructor(props) {
        super(props);
        this.handleCurrencyChange = this.handleCurrencyChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    handleCurrencyChange(value) {
        this.props.setCurrency(value);
    }

    handleEmailChange(email) {
        this.props.setEmail(email);
    }

    handlePasswordChange (password) {
        this.props.setPassword(password);
    }

    render() {
        return (
            <div className="appContainer">
                <Helmet>
                    <title>Coin Overwatch | Settings</title>
                </Helmet>
                <div className="headerContainer">
                    <Typography type="headline" className="investmentsHeading">User settings</Typography>
                    <div className="spacer"/>
                    <BackButton href={'/portfolio'}/>
                </div>

                <div className="settingsContainer">
                    <Paper className="settingsPaper">
                        <CurrencySettings onCurrencyChange={this.handleCurrencyChange}
                                          isFetching={this.props.isFetchingCurrencySettings}
                                          currency={this.props.user.settings.currency}/>
                    </Paper>
                </div>

                <div className="settingsContainer">
                    <Paper className="settingsPaper">
                        <EmailSettings onEmailChange={this.handleEmailChange}
                                       isFetching={this.props.isFetchingEmailSettings}/>
                    </Paper>
                </div>

                <div className="settingsContainer">
                    <Paper className="settingsPaper">
                        <PasswordSettings onPasswordChange={this.handlePasswordChange}
                                       isFetching={this.props.isFetchingEmailSettings}/>
                    </Paper>
                </div>

            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    user: state.rootReducer.auth.user,
    isFetchingCurrencySettings: state.rootReducer.settings.isFetchingCurrencySettings,
    isFetchingEmailSettings: state.rootReducer.settings.isFetchingEmailSettings,
    isFetchingPasswordSettings: state.rootReducer.settings.isFetchingPasswordSettings
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    setCurrency: currency => dispatch(setCurrency(currency)),
    setEmail: email => dispatch(setEmail(email)),
    setPassword: password => dispatch(setPassword(password))
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Settings));