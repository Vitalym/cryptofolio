/**
 * Created by Виталий on 22.07.2017.
 */
import React, { Component } from 'react';
import {connect} from 'react-redux';
import {
    Link
} from 'react-router-dom';
import {navIsLanding} from '../../actions/navActions';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';
import './Main.css';

let mainScreenshot, feature1, feature2, feature3, feature4, mailImg, telegramImg;

try {
    mainScreenshot = require(`../../assets/screens/main.png`);
} catch (e) {
    mainScreenshot = null;
}

try {
    feature1 = require(`../../assets/screens/feature1.png`);
} catch (e) {
    feature1 = null;
}

try {
    feature2 = require(`../../assets/screens/main.png`);
} catch (e) {
    feature2 = null;
}

try {
    feature3 = require(`../../assets/screens/feature3.png`);
} catch (e) {
    feature3 = null;
}

try {
    feature4 = require(`../../assets/screens/feature4.png`);
} catch (e) {
    feature4 = null;
}

try {
    mailImg = require(`../../assets/icons/Gmail.svg`);
} catch (e) {
    mailImg = null;
}
try {
    telegramImg = require(`../../assets/icons/telegram.svg`);
} catch (e) {
    telegramImg = null;
}

class Main extends Component {

    constructor(props) {
        super(props);
        this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        this.checkAuth();
        this.props.navIsLanding(true);
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillReceiveProps(nextProps) {
        this.checkAuth();
    }

    componentWillUnmount() {
        this.props.navIsLanding(false);
        window.removeEventListener('scroll', this.handleScroll);
    }

    checkAuth() {
        if (this.props.isAuthenticated) {
            this.props.history.push(`/portfolio`);
        }
    }

    getOffset (element){
        let bounding = element.getBoundingClientRect();
        return {
            top: bounding.top + document.body.scrollTop,
            left: bounding.left + document.body.scrollLeft
        };
    }

    handleScroll (){
        const startElement = this.featuresSection;
        let offset = this.getOffset(startElement);
        let windowsScrollTop  = window.pageYOffset;
        this.props.onSetOffset(offset, windowsScrollTop);
    }

    render() {
        return (
                <div className="landingContainer">
                    <div className="landingPrimarySection" data-flex data-layout="column" data-layout-align="start center">
                        <div className="landingPrimarySectionHeading">
                            Coin Overwatch
                        </div>
                        <div className="landingPrimarySectionSubheading">
                            Your crypto portfolio tracker
                        </div>
                        <div className="landingSectionActionButton">
                            <Button className="registerButton" color="accent" raised>
                                <Link to="/signup" className="actionButtonLink"> Create account</Link>
                            </Button>
                        </div>
                        <div className="landingPrimarySectionImage">
                            <img src={mainScreenshot} alt="Coin Overwatch"/>
                        </div>
                    </div>

                    <div className="clear"/>

                    <div className="landingSection featuresSection" ref={(ref) => this.featuresSection = ref} id="features">

                        <div className="aboutTextContainer">

                            <div className="feature">
                                <div className="featureName clear">
                                    Portfolio Management
                                    <div className="underline">
                                        <Icon color="primary">
                                            assessment
                                        </Icon>
                                    </div>
                                </div>
                                <div className="featureContent clear" data-flex data-layout="column" data-layout-gt-md="row">
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <img src={feature1} alt="Portfolio Management" className="featureImage"/>
                                    </div>
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <div className="featureDescription">
                                            Keep track of your assets, their price change and value. You can bind your
                                            transactions to your Bitcoin balance, so that buying or selling altcoins can
                                            add/withdraw your Bitcoin funds
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="clear"/>

                            <div className="feature">
                                <div className="featureName clear">
                                    Portfolio Performance
                                    <div className="underline">
                                        <Icon color="primary">
                                            assessment
                                        </Icon>
                                    </div>
                                </div>
                                <div className="featureContent featureContentMobile clear" data-flex data-layout="column" data-layout-gt-md="row">
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <img src={feature2} alt="Portfolio Performance" className="featureImage"/>
                                    </div>
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <div className="featureDescription">
                                            Get overall portfolio performance over time calculated in US Dollars or BTC, or
                                            track how each investment performed over time compared to dollars or bitcons.
                                        </div>
                                    </div>
                                </div>
                                <div className="featureContent featureContentFullscreen clear" data-flex data-layout="column" data-layout-gt-md="row">
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <div className="featureDescription">
                                            Get overall portfolio performance over time calculated in US Dollars or BTC, or
                                            track how each investment performed over time compared to dollars or bitcons.
                                        </div>
                                    </div>
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <img src={feature2} alt="Portfolio Performance" className="featureImage"/>
                                    </div>
                                </div>
                            </div>

                            <div className="clear"/>

                            <div className="feature">
                                <div className="featureName clear">
                                    Cryptomarket Overview
                                    <div className="underline">
                                        <Icon color="primary">
                                            assessment
                                        </Icon>
                                    </div>
                                </div>
                                <div className="featureContent clear" data-flex data-layout="column" data-layout-gt-md="row">
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <img src={feature3} alt="Cryptomarket Overview" className="featureImage"/>
                                    </div>
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <div className="featureDescription">
                                            Keep your hand on cryptomarket pulse and check coin prices and market cap change
                                            right in the app. Create a shortlist of currencies you watch to make them appear on
                                            top.
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="clear"/>

                            <div className="feature">
                                <div className="featureName clear">
                                    Portfolio Rebalancing
                                    <div className="underline">
                                        <Icon color="primary">
                                            assessment
                                        </Icon>
                                    </div>
                                </div>
                                <div className="featureContent featureContentMobile clear" data-flex data-layout="column" data-layout-gt-md="row">
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <img src={feature4} alt="Cryptomarket Overview" className="featureImage"/>
                                    </div>
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <div className="featureDescription">
                                            Set the desired level of asset allocation for each coin and track how it's share
                                            changes over time. Coin Overwatch will calculate how much should you sell or buy to
                                            restore the desired balance.
                                        </div>
                                    </div>
                                </div>
                                <div className="clear"/>
                                <div className="featureContent featureContentFullscreen clear" data-flex data-layout="column" data-layout-gt-md="row">
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <div className="featureDescription">
                                            Set the desired level of asset allocation for each coin and track how it's share
                                            changes over time. Coin Overwatch will calculate how much should you sell or buy to
                                            restore the desired balance.
                                        </div>
                                    </div>
                                    <div className="featureColumn" data-flex="50" data-layout="column" data-layout-align="center center">
                                        <img src={feature4} alt="Cryptomarket Overview" className="featureImage"/>
                                    </div>
                                </div>
                            </div>

                            <div className="clear"/>

                            <div className="landingSection contactsSection" id="contact">
                                <div className="aboutTextContainer" id="contact">
                                    <div className="landingHeading clear">
                                        Contacts
                                        <div className="underline">
                                            <Icon color="primary">
                                                chat
                                            </Icon>
                                        </div>
                                    </div>
                                    <div className="aboutFeatureRow" data-flex data-layout="column" data-layout-gt-xs="row">
                                        <div className="aboutFeatureColumn clear" data-flex="50" data-layout="column">
                                            <div className="aboutFeatureContent aboutContactContent">
                                                <div className="aboutContact">
                                                    <div data-flex data-layout="row" data-layout-align="start center" data-layout-align-gt-xs="center center">
                                                        <img src={mailImg} alt="Telegram" className="contactIcon" height="32"
                                                             width="32"/>
                                                        <a href="mailto:vitaliy.maltsev@gmail.com" className="contactLink">
                                                            vitaliy.maltsev@gmail.com
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="aboutFeatureColumn clear" data-flex="50" data-layout="column">
                                            <div className="aboutFeatureContent aboutContactContent">
                                                <div className="aboutContact" data-flex data-layout="row"
                                                     data-layout-align="start center"  data-layout-align-gt-xs="center center">
                                                    <img src={telegramImg} alt="Telegram" className="contactIcon" height="32"
                                                         width="32"/>
                                                    <a href="https://t.me/decipher1" className="contactLink">
                                                        @decipher1
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
        );
    }
}


// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    isAuthenticated: state.rootReducer.auth.isAuthenticated
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    navIsLanding: enable => dispatch(navIsLanding(enable))
});

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(Main);