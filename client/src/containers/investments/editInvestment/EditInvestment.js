/**
 * Created by Виталий on 02.08.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import Typography from 'material-ui/Typography';
import BackButton from '../../../components/backButton/BackButton';
import {updateInvestment} from '../../../actions/investments';
import {getAvailableCurrencies} from '../../../actions/currencies';

import EditInvestmentForm from '../../../components/investments/investmentForm/EditInvestmentForm';

import './EditInvestment.css';

class EditInvestment extends Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
        this.state = {
            isFetching: this.props.isFetching
        };
    }

    componentDidMount() {
        //TODO temporary
        this.props.getAvailableCurrencies();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isFetching !== this.state.isFetching) {
            this.setState({
                isFetching: nextProps.isFetching
            });
        }
    }

    submit(values) {
        let normalizedCoinResult = this.props.currencies.filter(coin => {
            if (coin.label === values.coin) {
                return coin;
            }
        });
        if (normalizedCoinResult.length) {
            values.normalizedCoin = normalizedCoinResult[0].value;
            this.setState({
                isFetching: true
            });
            this.props.updateInvestment(values, this.props.match.params.id);
        }
    }

    render() {
        const initialValues = {
            //form defaults
            action: this.props.selectedTransaction ? this.props.selectedTransaction.action : 'buy',
            priceType: this.props.selectedTransaction ? this.props.selectedTransaction.priceType : 'per coin',
            currency: this.props.selectedTransaction ? this.props.selectedTransaction.currency : 'USD',
            dateBought: this.props.selectedTransaction ? this.props.selectedTransaction.dateBought : new Date(),
            coin: this.props.selectedTransaction ? this.props.selectedTransaction.coin : null, // todo we gotta transform this
            price: this.props.selectedTransaction ? this.props.selectedTransaction.price : null,
            amount: this.props.selectedTransaction ? this.props.selectedTransaction.amount : null,
            note: this.props.selectedTransaction ? this.props.selectedTransaction.note : null
        };
        return (

            <div className="appContainer">
                <Helmet>
                    <title>Coin Overwatch | Edit Transaction</title>
                </Helmet>
                <div className="headerContainer" data-flex data-layout="row" data-layout-align="space-between">
                    <Typography type="headline" className="investmentsHeading">Edit Transaction</Typography>
                    <div className='spacer'/>
                    <BackButton href={this.props.previousPathName ? this.props.previousPathName : `/portfolio/history/${this.props.match.params.id}`}/>
                </div>
                <div data-flex='100' data-layout-align="center stretch" className="layoutColumn noBackground"
                     data-flex-grow>
                    <EditInvestmentForm onSubmit={this.submit} initialValues={initialValues}
                                        coins={this.props.currencies} isFetching={this.state.isFetching}/>
                </div>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    selectedTransaction: state.rootReducer.investments.selectedTransaction,
    isFetching: state.rootReducer.investments.isFetching,
    currencies: state.rootReducer.currencies.currencies,
    isFetchingCurrencies: state.rootReducer.currencies.isFetching,
    previousPathName: state.rootReducer.location.previousPathName
});

// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    updateInvestment: (transaction, id) => dispatch(updateInvestment(transaction, id)),
    getAvailableCurrencies: () => dispatch(getAvailableCurrencies()),
});

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(EditInvestment);