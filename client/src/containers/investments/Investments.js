import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Helmet} from "react-helmet";
import Typography from 'material-ui/Typography';
import Holdings from '../../components/investments/holdings/Holdings';
import HoldingsMobile from '../../components/investments/holdingsMobile/HoldingsMobile';
import PortfolioDistribution from '../../components/investments/portfolioDistribution/PortfolioDistribution';
import PortfolioValue from '../../components/investments/portfolioValue/PortfolioValue';
import PortfolioActionButton from '../../components/headingActionButton/HeadingActionButton';
import RebalanceButton from '../../components/investments/rebalanceButton/RebalanceButton';
import CurrencySettingsSwtitch from '../../components/currencySettingsSwitch/CurrencySettingsSwitch';
import './Investments.css';

class Investments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: window.innerWidth,
        };
    }

    componentWillMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };

    render() {
        const { width } = this.state;
        const isMobile = width <= 600;
        return (
            <div className="appContainer">
                <Helmet>
                    <title>Coin Overwatch | Portfolio</title>
                </Helmet>
                <div className="investmentsContainer investmentsValueContainer">
                    <div className="headerContainer" data-flex data-layout="row" data-layout-align="space-between">
                        <Typography type="headline" className="investmentsHeading">My portfolio</Typography>
                        <div className='spacer'/>
                        <PortfolioActionButton text='Add Transaction' color='accent' raised={false}/>
                    </div>
                    <div className="clear"/>
                    <CurrencySettingsSwtitch user={this.props.user}/>
                    <div className="portfolioValueContainer"
                         data-flex
                         data-layout="column"
                         data-layout-gt-md="row">
                        <div data-flex="33"  data-flex-grow className="layoutColumn noBackground" data-layout-align="start stretch">
                            <div className="paperFullHeight" data-flex data-layout="column" data-layout-align="center stretch" data-flex-grow>
                                <PortfolioValue
                                    totalValue={this.props.totalValue}
                                    currency={this.props.user && this.props.user.settings ? this.props.user.settings.currency : 'USD'}/>
                            </div>
                        </div>
                        <div className="clear"/>
                        <div data-flex="66" data-layout-align="center stretch" data-flex-grow className="layoutColumn">
                            <div data-flex data-layout="column" data-layout-align="center stretch" data-flex-grow
                                 className="paperFullHeight chartPaper">
                                <PortfolioDistribution holdings={this.props.holdings} isMobile={isMobile}/>
                                <RebalanceButton portfolio={this.props.user && this.props.user.settings ? this.props.user.currentPortfolio : null}/>
                            </div>
                        </div>
                    </div>
                    <div className="clear"/>
                    <div data-flex='100' data-layout-align="center stretch"  className={isMobile ? "layoutColumn noPadding noShadow": "layoutColumn noPadding"}>
                        <div data-flex data-layout="column" data-layout-align="center stretch" data-flex-grow
                             className="paperFullHeight noPadding">
                            {isMobile ?
                                <HoldingsMobile currency={this.props.user && this.props.user.settings ? this.props.user.settings.currency : 'USD'}/> :
                                <Holdings currency={this.props.user && this.props.user.settings ? this.props.user.settings.currency : 'USD'}/>}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => ({
    totalValue: state.rootReducer.investments.totalPortfolioValue,
    holdings: state.rootReducer.investments.holdings,
    user: state.rootReducer.auth.user
});

// Use connect to put them together
export default withRouter(connect(mapStateToProps)(Investments));
