/**
 * Created by Виталий on 02.08.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import Typography from 'material-ui/Typography';
import BackButton from '../../../components/backButton/BackButton';
import {addInvestment} from '../../../actions/investments';
import {getAvailableCurrencies} from '../../../actions/currencies';

import AddInvestmentForm from '../../../components/investments/investmentForm/AddInvestmentForm';

import './AddInvestment.css';

class AddInvestment extends Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
        this.state = {
            isFetching: this.props.isFetching
        };
    }

    componentDidMount() {
        //TODO temporary
        this.props.getAvailableCurrencies();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isFetching !== this.state.isFetching) {
            this.setState({
                isFetching: nextProps.isFetching
            });
        }
    }

    submit(values) {
        //normalize currency value
        let normalizedCoinResult = this.props.currencies.filter(coin => {
            if (coin.label === values.coin) {
                return coin;
            }
        });
        if (normalizedCoinResult.length) {
            values.normalizedCoin = normalizedCoinResult[0].value;
            this.setState({
                isFetching: true
            });
            this.props.save(values);
        }
    }

    render() {
        const initialValues = {
            //form defaults
            action: 'buy',
            priceType: 'per coin',
            currency: 'USD',
            dateBought: new Date()
        };
        return (
            <div className="appContainer">
                <Helmet>
                    <title>Coin Overwatch | New Transaction</title>
                </Helmet>
                <div className="headerContainer" data-flex data-layout="row" data-layout-align="space-between">
                    <Typography type="headline" className="investmentsHeading">New Transaction</Typography>
                    <div className='spacer'/>
                    <BackButton href={'/portfolio'}/>
                </div>
                <div data-flex='100' data-layout-align="center stretch" className="layoutColumn noBackground"
                     data-flex-grow>
                    <AddInvestmentForm onSubmit={this.submit} initialValues={initialValues}
                                       coins={this.props.currencies} isFetching={this.state.isFetching}/>
                </div>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
    return {
        isFetching: state.rootReducer.investments.isFetching,
        currencies: state.rootReducer.currencies.currencies,
        isFetchingCurrencies: state.rootReducer.currencies.isFetching
    }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
    return {
        save: (transaction) => dispatch(addInvestment(transaction)),
        getAvailableCurrencies: () => dispatch(getAvailableCurrencies())
    }
};

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(AddInvestment);