/**
 * Created by Виталий on 15.08.2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Helmet} from 'react-helmet';
import Typography from 'material-ui/Typography';
import InvestmentsHistory from '../../components/investments/investmentsHistory/InvestmentsHistory';
import InvestmentsHistoryMobile from '../../components/investments/investmentsHistoryMobile/InvestmentsHistoryMobile';
import PortfolioActionButton from '../../components/headingActionButton/HeadingActionButton';
import TransactionValue from '../../components/coinTransaction/transactionValue/TransactionValue';
import TransactionChart from '../../components/coinTransaction/transactionChart/TransactionChart';
import {getHistoryDataByCoin} from '../../actions/investments';
import './CoinTransactionHistory.css';

class SingleTransactionHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: window.innerWidth,
        };
    }

    componentDidMount() {
        let stats = this.props.stats;
        let coin = stats && stats.coin.symbol ? stats.coin.symbol.toUpperCase() : null;
        this.props.getHistoryDataByCoin(coin);
    }

    componentWillMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };

    render() {

        const { width } = this.state;
        const isSmallScreen = width <= 640;
        const isMobile = width <= 710;

        let transactionChart = this.props.history ?
            <TransactionChart data={this.props.history} name={this.props.transactions[0].coin.name}
                              isFetching={this.props.isFetchingCoinHistory}/> : null;

        return (
            <div className="appContainer">
                <Helmet>
                    <title>Coin Overwatch | {this.props.transactions[0].coin.name}</title>
                </Helmet>
                <div className="investmentsContainer investmentsHoldingsContainer">
                    <div className="headerContainer" data-flex data-layout="row"
                         data-layout-align="space-between">
                        <Typography type="headline"
                                    className="investmentsHeading">{this.props.transactions[0].coin.name}</Typography>
                        <div className='spacer'/>
                        <PortfolioActionButton text='Add Transaction' color='accent' raised={false}/>
                    </div>
                    <div data-flex="100" data-layout-align="center stretch" className="noBackground layoutColumn">
                        <div className="portfolioValueContainer" data-flex data-layout="column"
                             data-layout-align="start stretch">
                            <TransactionValue stats={this.props.stats} user={this.props.user}/>
                        </div>
                    </div>
                    <div data-flex="100" data-layout-align="center stretch" className="layoutColumn">
                        <div className="portfolioValueContainer" data-flex data-layout="column"
                             data-layout-align="start stretch">
                            {transactionChart}
                        </div>
                    </div>
                    <div data-flex="100" data-layout-align="center stretch" className={isMobile ? 'layoutColumn noPadding noShadow' : 'layoutColumn noPadding'}>
                        {isMobile ? <InvestmentsHistoryMobile investments={this.props.transactions} isSmallScreen={isSmallScreen} isMobile={isMobile}/> :
                            <InvestmentsHistory investments={this.props.transactions} isSmallScreen={isSmallScreen} isMobile={isMobile}/>}
                    </div>
                </div>
            </div>
        );
    }


}

const mapStateToProps = (state) => ({
    transactions: state.rootReducer.investments.transactionsByCoin,
    isFetching: state.rootReducer.investments.isFetching,
    stats: state.rootReducer.investments.transactionsByCoinStats,
    history: state.rootReducer.investments.transactionsByCoinHistory,
    isFetchingCoinHistory: state.rootReducer.investments.isFetchingCoinHistory,
});


// Maps actions to props
const mapDispatchToProps = (dispatch) => ({
    getHistoryDataByCoin: (id) => dispatch(getHistoryDataByCoin(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleTransactionHistory);
