/**
 * Created by Виталий on 22.07.2017.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as coinActions from '../../actions/coinActions';

class PortfolioExistanceVerification extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <h2>Welcome to React</h2>
                </div>
                <p className="App-intro">
                    yo wazzup
                </p>
            </div>
        );
    }
}

// Maps state from store to props
const mapStateToProps = (state, ownProps) => {
    return {
        // You can now say this.props.books
        coins: state.coins
    }
};

// Maps actions to props
const mapDispatchToProps = (dispatch) => {
    return {
        // You can now say this.props.createBook
        addCoin: coin => dispatch(coinActions.addCoin(coin))
    }
};

// Use connect to put them together
export default connect(mapStateToProps, mapDispatchToProps)(PortfolioExistanceVerification);

//export default PortfolioExistanceVerification;