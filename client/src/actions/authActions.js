/**
 * Created by Виталий on 23.07.2017.
 */
import {USER_SIGNUP, 
    EMAIL_VERIFIED, 
    USER_LOGGED_IN, 
    USER_LOGGED_OUT, 
    SAVE_USER_TOKEN, 
    PASSWORD_RESET_REQUEST_SENT,
    PASSWORD_TOKEN_VERIFIED,
    PASSWORD_RESET
} from '../actions';
import ReactGA from 'react-ga';
import {showErrorMessage, showSuccessMessage} from './notifications';
import {createUser, verifyEmail, loginUser, loginFacebook, requestPasswordReset, verifyPasswordReset, resetPassword} from '../api/auth';
import {updateHeaders} from '../api/utils';
import history  from './../history';

const localStorage = window.localStorage;

export const signup = (user) => {
    return dispatch => {
        createUser(user)
            .then( (payload) => {
                dispatch({type: USER_SIGNUP, payload});
                updateHeaders({ Authorization: 'Bearer ' + payload.token });
                ReactGA.event({
                    category: 'Authentication',
                    action: 'Signed up'
                });
                try {
                    let user = parseJwt(payload.token);
                    localStorage.setItem('user', JSON.stringify(user.data));
                    localStorage.setItem('token', payload.token)
                } catch (e) {
                    //if we are unable to save user to localstorage
                    return;
                }
                history.push('/portfolio');
            })
            .catch(payload => {
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: USER_SIGNUP,
                    error: true,
                    payload
                });
                ReactGA.event({
                    category: 'Authentication',
                    action: 'Sign up error'
                });
            })
    }
};

export const verify = (user) => {
    return dispatch => {
        verifyEmail(user)
            .then( (payload) => {
                dispatch({type: EMAIL_VERIFIED, payload});
            })
            .catch(payload => {
                dispatch({
                    type: EMAIL_VERIFIED,
                    error: true,
                    payload
                });
            })
    }
};

export const login = (user) => {
    return dispatch => {
        loginUser(user)
            .then( (payload) => {
                updateHeaders({ Authorization: 'Bearer ' + payload.token });
                dispatch({type: USER_LOGGED_IN, payload});
                history.push('/portfolio');
                ReactGA.event({
                    category: 'Authentication',
                    action: 'Logged in'
                });
                //try to save user and token to localstorage
                try {
                    let user = parseJwt(payload.token);
                    localStorage.setItem('user', JSON.stringify(user.data));
                    localStorage.setItem('token', payload.token)
                } catch (e) {
                    //if we are unable to save user to localstorage
                    return;
                }
               
            })
            .catch(payload => {
                dispatch(showErrorMessage('Error', 'username or password is incorrect'));
                dispatch({
                    type: USER_LOGGED_IN,
                    error: true,
                    payload
                });
                ReactGA.event({
                    category: 'Authentication',
                    action: 'Log in error'
                });
            })
    }
};

export const loginViaFacebook = (user) => {
    return dispatch => {
        loginFacebook(user)
            .then( (payload) => {
                updateHeaders({ Authorization: 'Bearer ' + payload.token });
                dispatch(updateToken(payload.token));
                dispatch({type: USER_LOGGED_IN, payload});
                history.push('/portfolio');
                ReactGA.event({
                    category: 'Authentication',
                    action: 'Logged in via facebook'
                });

            })
            .catch(payload => {
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: USER_LOGGED_IN,
                    error: true,
                    payload
                });
                ReactGA.event({
                    category: 'Authentication',
                    action: 'Error logging in via facebook'
                });
            })
    }
};

export const requestReset = (email) => {
    return dispatch => {
        requestPasswordReset(email)
            .then( (payload) => {
                dispatch({type: PASSWORD_RESET_REQUEST_SENT, payload});
            })
            .catch(payload => {
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: PASSWORD_RESET_REQUEST_SENT,
                    error: true,
                    payload
                });
            })
    }
};

export const verifyResetToken = (token) => {
    return dispatch => {
        verifyPasswordReset(token)
            .then( (payload) => {
                dispatch({type: PASSWORD_TOKEN_VERIFIED, payload});
            })
            .catch(payload => {
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: PASSWORD_TOKEN_VERIFIED,
                    error: true,
                    payload
                });
            })
    }
};

export const resetUserPassword = (password) => {
    return dispatch => {
        resetPassword(password)
            .then( (payload) => {
                dispatch({type: PASSWORD_RESET, payload});
                //show toast
                dispatch(showSuccessMessage('Error', 'You may login now'));
                //redirect to login
                history.push('/login');
            })
            .catch(payload => {
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: PASSWORD_RESET,
                    error: true,
                    payload
                });
            })
    }
};

export const updateToken = (token) => {
    //set ne token to localstorage if there is a new one
    return dispatch => {
        try {
            let user = parseJwt(token);
            localStorage.setItem('user', JSON.stringify(user.data));
            localStorage.setItem('token', token)
        } catch (e) {
            //if we can't access localstorage
            return;
        }
        dispatch({type: SAVE_USER_TOKEN, token});
    }
};

export const logout = () => {
    let payload = null;
    return dispatch => {
        try {
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            dispatch({type: USER_LOGGED_OUT, payload});
            history.push('/login');
            ReactGA.event({
                category: 'Authentication',
                action: 'Logged out'
            });
        } catch (e) {
            //if we can't access localstorage
            dispatch({type: USER_LOGGED_OUT, payload});
            history.push('/login');
        }   
    }
};



function parseJwt (token) {
    let base64Url = token.split('.')[1];
    let base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
}