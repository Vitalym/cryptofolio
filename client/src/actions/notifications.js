/**
 * Created by Виталий on 01.08.2017.
 */
import {addNotification as notify} from 'reapop';
import {
    SHOW_NOTIFICATION,
    HIDE_NOTIFICATION
} from '../actions';

export const showErrorMessage = (title, message) => {
    return dispatch => {
        /*dispatch(notify({
            title: title,
            message: message,
            status: 'error',
            dismissible: true,
            dismissAfter: 3000
        }));*/
        const payload = {
            title: title,
            message: message
        };
        dispatch({ type: SHOW_NOTIFICATION, payload });
    }
};

export const showSuccessMessage = (title, message) => {
    return dispatch => {
        /*dispatch(notify({
            title: title,
            message: message,
            status: 'success',
            dismissible: true,
            dismissAfter: 3000
        }));*/
        const payload = {
            title: title,
            message: message
        };
        dispatch({ type: SHOW_NOTIFICATION, payload });
    }
};

export const hideMessage = () => {
    return dispatch => {
        dispatch({ type: HIDE_NOTIFICATION });
    }
};