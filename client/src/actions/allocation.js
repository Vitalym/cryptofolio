/**
 * Created by Виталий on 23.07.2017.
 */
import {
    HOLDINGS_ALLOCATION_FETCH,
    HOLDINGS_ALLOCATION_FETCH_STARTED,
    SORT_HOLDINGS_ALLOCATION_TABLE,
    SORT_ALLOCATION_SETUP_TABLE,
    TOGGLE_ALLOCATION_DIALOG,
    ALLOCATION_VALUE_UPDATED,
    ALLOCATION_VALUES_SET
} from '../actions'
import {showErrorMessage, showSuccessMessage} from './notifications';
import {updateToken, logout} from './authActions';
import {getUserHoldingsAllocation, setUserHoldingsAllocation} from '../api/allocation';
import {updateHeaders} from '../api/utils';
import {getSum} from '../utils/utils';
import history  from './../history';

export const getHoldingsAllocation = () => {
    return dispatch => {
        dispatch({type: HOLDINGS_ALLOCATION_FETCH_STARTED});
        getUserHoldingsAllocation()
            .then((payload) => {
                dispatch({type: HOLDINGS_ALLOCATION_FETCH, payload});
                dispatch(updateToken(payload.token));
            })
            .catch(payload => {
                if(payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: HOLDINGS_ALLOCATION_FETCH,
                    error: true,
                    payload
                });
            })
    }
};

export const saveAllocationSchema = (payload) => {
    if (getSum(payload, 'value') > 100){
        return dispatch => {
            dispatch(showErrorMessage('Error', `Sum is greater than 100`));
        }
    } else {
        return dispatch => {
            if(payload.error && payload.error.status && payload.error.status === 401) {
                return dispatch(logout());
            }
            dispatch({type: TOGGLE_ALLOCATION_DIALOG});
            setUserHoldingsAllocation(payload)
                .then( (payload) => {
                    dispatch({type: HOLDINGS_ALLOCATION_FETCH, payload});
                    dispatch(updateToken(payload.token));
                    dispatch(getHoldingsAllocation());
                })
                .catch(payload => {
                    dispatch(showErrorMessage('Error', payload.message));
                })
        }
    }
};

export const sortHoldings = (payload) => {
    return dispatch => {
        dispatch({type: SORT_HOLDINGS_ALLOCATION_TABLE, payload});
    }
};

export const sortAllocationsSetupTable = (payload) => {
    return dispatch => {
        dispatch({type: SORT_ALLOCATION_SETUP_TABLE, payload});
    }
};

export const toggleDialog = () => {
    return dispatch => {
        dispatch({type: TOGGLE_ALLOCATION_DIALOG});
    }
};

export const updateAllocationValue = (payload) => {
    return dispatch => {
        //check if it's a valid number
        if (Number(payload.value)) {
            dispatch({type: ALLOCATION_VALUE_UPDATED, payload});
        } else {
            dispatch(showErrorMessage('Error', 'Field must be a number')); //TODO move validation to onblur
            dispatch({type: ALLOCATION_VALUE_UPDATED, error: true});
        }
    }
};

export const setAllocationValues = () => {
    return dispatch => {
        dispatch({type: ALLOCATION_VALUES_SET});
    }
};

/*
function getSum(items, prop) {
    //get sum of object keys in an array
    return items.reduce( (a, b) => {
        return Number(a) + Number(b[prop]);
    }, 0);
}*/
