/**
 * Created by dev on 29.8.17.
 */
import {
    CURRENCY_SETTINGS_UPDATE,
    CURRENCY_SETTINGS_FETCH_STARTED,
    ZERO_BALANCE_SETTINGS_UPDATE,
    ZERO_BALANCE_FETCH_STARTED,
    EMAIL_SETTINGS_UPDATE,
    EMAIL_SETTINGS_FETCH_STARTED,
    PASSWORD_SETTINGS_UPDATE,
    PASSWORD_SETTINGS_FETCH_STARTED,
} from '../actions';
import {showErrorMessage, showSuccessMessage} from './notifications';
import {updateToken, logout} from './authActions';
import {updateCurrencySettings, updateDisplayZeroBalanceSettings, updateEmailSettings, updatePasswordSettings} from '../api/settings';

export const setCurrency = (currency) => {
    return dispatch => {
        dispatch({type: CURRENCY_SETTINGS_FETCH_STARTED});
        updateCurrencySettings(currency)
            .then( (payload) => {
                dispatch({type: CURRENCY_SETTINGS_UPDATE, payload});
                dispatch(updateToken(payload.token));
                dispatch(showSuccessMessage('Success', payload.message));
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: CURRENCY_SETTINGS_UPDATE,
                    error: true,
                    payload
                });
            })
    }
};

export const setZeroBalanceSettings = (value) => {
    return dispatch => {
        dispatch({type: ZERO_BALANCE_FETCH_STARTED});
        updateDisplayZeroBalanceSettings(value)
            .then( (payload) => {
                dispatch({type: ZERO_BALANCE_SETTINGS_UPDATE, payload});
                dispatch(updateToken(payload.token));
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: ZERO_BALANCE_SETTINGS_UPDATE,
                    error: true,
                    payload
                });
            })
    }
};

export const setEmail = (email) => {
    return dispatch => {
        dispatch({type: EMAIL_SETTINGS_FETCH_STARTED});
        updateEmailSettings(email)
            .then( (payload) => {
                dispatch({type: EMAIL_SETTINGS_UPDATE, payload});
                dispatch(updateToken(payload.token));
                dispatch(showSuccessMessage('Success', payload.message));
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: EMAIL_SETTINGS_UPDATE,
                    error: true,
                    payload
                });
            })
    }
};

export const setPassword = (password) => {
    return dispatch => {
        dispatch({type: PASSWORD_SETTINGS_FETCH_STARTED});
        updatePasswordSettings(password)
            .then( (payload) => {
                dispatch({type: PASSWORD_SETTINGS_UPDATE, payload});
                dispatch(updateToken(payload.token));
                dispatch(showSuccessMessage('Success', payload.message));
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: PASSWORD_SETTINGS_UPDATE,
                    error: true,
                    payload
                });
            })
    }
};