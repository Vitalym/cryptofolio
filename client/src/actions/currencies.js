/**
 * Created by Виталий on 23.07.2017.
 */
import {
    CURRENCIES_FETCH,
    COIN_DATA_FETCH,
    COIN_DATA_FETCH_STARTED,
    SORT_COINS_DATA,
    CURRENCIES_SEARCH_STARTED,
    CURRENCIES_SEARCH,
    SET_CURRENCIES_SEARCH_QUERY,
    CLEAR_COINS_SEARCH
} from '../actions'
import {showErrorMessage} from './notifications';
import {updateToken, logout} from './authActions';
import {getCurrencyNames, getCurrencyData, searchCurrency} from '../api/currencies';

const localStorage = window.localStorage;

export const getAvailableCurrencies = () => {
    return dispatch => {
        getCurrencyNames()
            .then( (payload) => {
                dispatch({type: CURRENCIES_FETCH, payload});
                dispatch(updateToken(payload.token));
                if(payload.token){//means that data came from server
                    try {
                        localStorage.setItem('coins', JSON.stringify(payload.currencies));
                    } catch (e) {
                        //if we are unable to save user to localstorage
                        return;
                    }
                }
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: CURRENCIES_FETCH,
                    error: true,
                    payload
                });
            })
    }
};

//get full currency data
export const getCoinData = (skip, limit) => {
    return dispatch => {
        dispatch({type: COIN_DATA_FETCH_STARTED});
        getCurrencyData(skip, limit)
            .then( (payload) => {
                dispatch({type: COIN_DATA_FETCH, payload});
                dispatch(updateToken(payload.token));
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: COIN_DATA_FETCH,
                    error: true,
                    payload
                });
            })
    }
};

//get full currency data
export const searchCoins = (query) => {
    return dispatch => {
        dispatch({type: SET_CURRENCIES_SEARCH_QUERY, query});
        dispatch({type: CURRENCIES_SEARCH_STARTED});
        searchCurrency(query)
            .then( (payload) => {
                dispatch({type: CURRENCIES_SEARCH, payload});
                dispatch(updateToken(payload.token));
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: CURRENCIES_SEARCH,
                    error: true,
                    payload
                });
            })
    }
};

export const clearSearchData = () => {
    return dispatch => {
        dispatch({type: CLEAR_COINS_SEARCH});
    }
};

export const sortCoinsData = (payload) => {
    return dispatch => {
        dispatch({type: SORT_COINS_DATA, payload});
    }
};