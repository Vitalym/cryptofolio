/**
 * Created by Виталий on 30.07.2017.
 */
import { NAV_ENABLE, SIDENAV_TOGGLE, NAV_LANDING_PAGE } from '../actions';

export function navEnable(enabled) {
    return { type: NAV_ENABLE, enabled };
}

export function navIsLanding(enabled) {
    return { type: NAV_LANDING_PAGE, enabled };
}

export function toggleSidenav() {
    return { type: SIDENAV_TOGGLE }
}