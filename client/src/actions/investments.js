/**
 * Created by Виталий on 23.07.2017.
 */
import {
    INVESTMENT_ADDED,
    SINGLE_INVESTMENTS_FETCH,
    INVESTMENTS_BY_COIN_FETCH,
    INVESTMENTS_FETCH,
    INVESTMENTS_FETCH_STARTED,
    SORT_HISTORY_TABLE,
    SORT_HOLDINGS_TABLE,
    SORT_COIN_TRANSACTIONS_TABLE,
    INVESTMENT_REMOVED,
    INVESTMENT_UPDATED,
    TABLE_SORTING_STARTED,
    HISTORY_DATA_FETCH,
    HISTORY_DATA_FETCH_STARTED
} from '../actions'
import {showErrorMessage, showSuccessMessage} from './notifications';
import {updateToken, logout} from './authActions';
import {
    getUserInvestments,
    addNewInvestment,
    getInvestmentsByCoin,
    getCoinHistoryData,
    deleteInvestment,
    getInvestmentById,
    editInvestment
} from '../api/investments';
import ReactGA from 'react-ga';
import {updateHeaders} from '../api/utils';
import history from './../history';

export const getInvestments = () => {
    return dispatch => {
        dispatch({type: INVESTMENTS_FETCH_STARTED});
        getUserInvestments()
            .then((payload) => {
                dispatch({type: INVESTMENTS_FETCH, payload});
                dispatch(updateToken(payload.token));
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: INVESTMENTS_FETCH,
                    error: true,
                    payload
                });
            })
    }
};

export const getByCoin = (id) => {
    return dispatch => {
        dispatch({type: INVESTMENTS_FETCH_STARTED});
        getInvestmentsByCoin(id)
            .then((payload) => {
                dispatch({type: INVESTMENTS_BY_COIN_FETCH, payload});
                dispatch(updateToken(payload.token));
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: INVESTMENTS_BY_COIN_FETCH,
                    error: true,
                    payload
                });
            })
    }
};

export const getHistoryDataByCoin = (id) => {
    return dispatch => {
        dispatch({type: HISTORY_DATA_FETCH_STARTED});
        getCoinHistoryData(id)
            .then((payload) => {
                dispatch({type: HISTORY_DATA_FETCH, payload});
                dispatch(updateToken(payload.token));
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: HISTORY_DATA_FETCH,
                    error: true,
                    payload
                });
            })
    }
};

export const getInvestment = (id) => {
    return dispatch => {
        dispatch({type: INVESTMENTS_FETCH_STARTED});
        getInvestmentById(id)
            .then((payload) => {
                dispatch({type: SINGLE_INVESTMENTS_FETCH, payload});
                dispatch(updateToken(payload.token));
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: SINGLE_INVESTMENTS_FETCH,
                    error: true,
                    payload
                });
            })
    }
};

export const addInvestment = (investment) => {
    return dispatch => {
        dispatch({type: INVESTMENTS_FETCH_STARTED});
        addNewInvestment(investment)
            .then((payload) => {
                dispatch({type: INVESTMENT_ADDED, payload});
                dispatch(updateToken(payload.token));
                dispatch(showSuccessMessage('Success', payload.message));
                dispatch(getInvestments());
                history.push('/portfolio'); //TODO rebuild to back
                ReactGA.event({
                    category: 'Investments',
                    action: 'New investment added'
                });
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: INVESTMENT_ADDED,
                    error: true,
                    payload
                });
                ReactGA.event({
                    category: 'Investments',
                    action: 'New investment error'
                });
            })
    }
};

export const updateInvestment = (investment, id) => {
    return dispatch => {
        dispatch({type: INVESTMENTS_FETCH_STARTED});
        editInvestment(investment, id)
            .then((payload) => {
                dispatch({type: INVESTMENT_UPDATED, payload});
                dispatch(updateToken(payload.token));
                dispatch(showSuccessMessage('Success', payload.message));
                history.push(`/portfolio/history/${payload.investment.coin.id}`);
                ReactGA.event({
                    category: 'Investments',
                    action: 'Investment updated'
                });
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: INVESTMENT_UPDATED,
                    error: true,
                    payload
                });
                ReactGA.event({
                    category: 'Investments',
                    action: 'Investment update error'
                });
            })
    }
};

export const removeInvestment = (investment) => {
    return dispatch => {
        dispatch({type: INVESTMENTS_FETCH_STARTED});
        deleteInvestment(investment._id)
            .then((payload) => {
                dispatch({type: INVESTMENT_REMOVED, investment});
                dispatch(updateToken(payload.token));
                dispatch(showSuccessMessage('Success', payload.message));
                history.push('/portfolio');
                ReactGA.event({
                    category: 'Investments',
                    action: 'Investment removed'
                });
            })
            .catch(payload => {
                if (payload.error && payload.error.status && payload.error.status === 401) {
                    return dispatch(logout());
                }
                dispatch(showErrorMessage('Error', payload.message));
                dispatch({
                    type: INVESTMENT_REMOVED,
                    error: true,
                    payload
                });
                ReactGA.event({
                    category: 'Investments',
                    action: 'Investment removal error'
                });
            })
    }
};

export const sortHistory = (payload) => {
    return dispatch => {
        dispatch({type: TABLE_SORTING_STARTED});
        dispatch({type: SORT_HISTORY_TABLE, payload});
    }
};

export const sortHoldings = (payload) => {
    return dispatch => {
        dispatch({type: TABLE_SORTING_STARTED});
        dispatch({type: SORT_HOLDINGS_TABLE, payload});
    }
};

export const sortCoinTransactions = (payload) => {
    return dispatch => {
        dispatch({type: TABLE_SORTING_STARTED});
        dispatch({type: SORT_COIN_TRANSACTIONS_TABLE, payload});
    }
};