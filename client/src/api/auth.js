/**
 * Created by dev on 25.7.17.
 */

import {headers, parseJSON} from './utils';
import {baseUrl} from './utils';

export function createUser(user) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify(user)
    };

    return fetch(`${baseUrl}/auth/signup`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function verifyEmail(verificationToken) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({verificationToken})
    };

    return fetch(`${baseUrl}/auth/verify`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function loginUser(user) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify(user)
    };

    return fetch(`${baseUrl}/auth/login`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function loginFacebook(user) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify(user)
    };

    return fetch(`${baseUrl}/auth/facebook`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function requestPasswordReset(email) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify(email)
    };

    return fetch(`${baseUrl}/auth/reset`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function verifyPasswordReset(resetToken) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({resetToken})
    };

    return fetch(`${baseUrl}/auth/verifyResetToken`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function resetPassword(password) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify(password)
    };

    return fetch(`${baseUrl}/auth/updatePassword`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}