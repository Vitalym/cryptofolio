/**
 * Created by dev on 11.8.17.
 */
import {headers, parseJSON} from './utils';
import {baseUrl} from './utils';

export function getUserHoldingsAllocation () {
    const options = {
        headers: headers(),
        method: 'GET'
    };

    return fetch(`${baseUrl}/api/holdingsAllocation`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function setUserHoldingsAllocation (data) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({allocation: data})
    };

    return fetch(`${baseUrl}/api/investments/allocation`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}