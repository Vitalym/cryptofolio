/**
 * Created by dev on 25.7.17.
 */

const localStorage = window.localStorage;
export const baseUrl = process.env.NODE_ENV !== 'production' ? 'http://localhost:8080' : 'https://coinoverwatch.com';

let _headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
};

export function headers() {
    return addAuthorizationHeader(_headers);
}

export function parseJSON(response) {
    let json = response.json();
    //TODO save new token here
    if (response.ok) {
        return json;
    }
    
    return json.then(Promise.reject.bind(Promise));
}

export function updateHeaders(newHeaders) {
    Object.keys(_headers).forEach((key) => {
        if (undefined === _headers[key]) {
            delete _headers[key];
        }
    });
    return _headers = Object.assign({}, _headers, newHeaders);
}

function isAuthenticated () {
    return localStorage.getItem('token');
}

function addAuthorizationHeader (headers) {
    const token = isAuthenticated();
    if(token){
        return Object.assign({}, headers, {
            Authorization: 'Bearer ' + token
        });
    } else {
        return headers;
    }
}