/**
 * Created by dev on 29.8.17.
 */

import {headers, parseJSON} from './utils';
import {baseUrl} from './utils';

export function updateCurrencySettings(currency) {
    const options = {
        headers: headers(),
        method: 'PUT',
        body: JSON.stringify({currency})
    };

    return fetch(`${baseUrl}/api/user/settings/currency`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function updateEmailSettings(email) {
    const options = {
        headers: headers(),
        method: 'PUT',
        body: JSON.stringify(email)
    };

    return fetch(`${baseUrl}/api/user/email`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function updatePasswordSettings(password) {
    const options = {
        headers: headers(),
        method: 'PUT',
        body: JSON.stringify(password)
    };

    return fetch(`${baseUrl}/api/user/password`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function updateDisplayZeroBalanceSettings (value) {
    const options = {
        headers: headers(),
        method: 'PUT',
        body: JSON.stringify({value})
    };

    return fetch(`${baseUrl}/api/user/settings/zerobalance`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}