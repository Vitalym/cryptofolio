import {headers, parseJSON} from './utils';
import {baseUrl} from './utils';

export function getUserNotifications() {
    const options = {
        headers: headers(),
        method: 'GET'
    };

    return fetch(`${baseUrl}/api/notifications`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}