import {headers, parseJSON} from './utils';
import {baseUrl} from './utils';

export function getUserInvestments() {
    const options = {
        headers: headers(),
        method: 'GET'
    };

    return fetch(`${baseUrl}/api/investments`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function getInvestmentsByCoin(id) {
    const options = {
        headers: headers(),
        method: 'GET'
    };

    return fetch(`${baseUrl}/api/investments/coin/${id}`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function getCoinHistoryData(id) {
    const options = {
        headers: headers(),
        method: 'GET'
    };

    return fetch(`${baseUrl}/api/investments/coin/history/${id}`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function getInvestmentById(id) {
    const options = {
        headers: headers(),
        method: 'GET'
    };

    return fetch(`${baseUrl}/api/investments/${id}`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function addNewInvestment(investment) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify(investment)
    };

    return fetch(`${baseUrl}/api/investments`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function editInvestment(investment, id) {
    const options = {
        headers: headers(),
        method: 'PUT',
        body: JSON.stringify(investment)
    };

    return fetch(`${baseUrl}/api/investments/${id}`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function deleteInvestment(id) {
    const options = {
        headers: headers(),
        method: 'DELETE'
    };

    return fetch(`${baseUrl}/api/investments/${id}`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}