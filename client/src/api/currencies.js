import * as moment from 'moment';
import {headers, parseJSON} from './utils';
import {baseUrl} from './utils';

const localStorage = window.localStorage;

export function getCurrencyNames() {

    //check if we have fresh data in localstorage already
    let parsedCoinData, coinData = localStorage.getItem('coins');
    try {
        parsedCoinData = JSON.parse(coinData);
    } catch (e) {
        parsedCoinData = null;
    }

    if (coinData && coinData != 'undefined') {
        let now = moment(new Date()); //todays date
        let end = moment(parsedCoinData.timestamp); // timestamp of saved data
        let duration = moment.duration(now.diff(end));
        if (duration.asHours > 24) {
            return {
                currencies: parsedCoinData
            };
        }

    }

    const options = {
        headers: headers(),
        method: 'GET'
    };

    return fetch(`${baseUrl}/api/currency/names`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function getCurrencyData(skip, limit) {
    const options = {
        headers: headers(),
        method: 'GET'
    };

    return fetch(`${baseUrl}/api/currency?skip=${skip}&limit=${limit}`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}

export function searchCurrency (query) {
    const options = {
        headers: headers(),
        method: 'POST',
        body: JSON.stringify({query})
    };

    return fetch(`${baseUrl}/api/currency/search`, options)
        .then(parseJSON)
        .catch((error) => {
            return Promise.reject(error);
        });
}