import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'semantic-ui-css/semantic.min.css';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from 'react-redux';
import { ConnectedRouter, syncHistoryWithStore } from 'react-router-redux';
import configureStore from './store/configureStore';
import history from './history';
import App from './containers/app/App';

const store = configureStore();

const rootElement = (
    <Provider store={store}>
        <ConnectedRouter  history={history}>
            <App />
        </ConnectedRouter >
    </Provider>
);

ReactDOM.render(rootElement, document.getElementById('root'));

registerServiceWorker();
