/**
 * Created by Виталий on 14.08.2017.
 */

//calculate value of object keys
export const getSum = (items, prop) => {
    //get sum of object keys in an array
    return items.reduce( (a, b) => {
        return Number(a) + Number(b[prop]);
    }, 0);
};

export const parseJson = (data, def) => {
    try {
        return JSON.parse(data);
    } catch (e) {
        //if we can't access localstorage
       return def;
    }
};

export const getItemPrice = (priceType, currency, amount, usdPricePerItem, btcPricePerItem) => {
    //calculate price for edit form
    let pricePerItem = currency === 'BTC' ? btcPricePerItem : usdPricePerItem;
    return priceType === 'per coin' ? pricePerItem : pricePerItem / amount;
};