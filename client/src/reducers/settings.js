/**
 * Created by dev on 29.8.17.
 */
/**
 * Created by Виталий on 23.07.2017.
 */
import {
    CURRENCY_SETTINGS_UPDATE,
    CURRENCY_SETTINGS_FETCH_STARTED,
    ZERO_BALANCE_SETTINGS_UPDATE,
    ZERO_BALANCE_FETCH_STARTED,
    EMAIL_SETTINGS_UPDATE,
    EMAIL_SETTINGS_FETCH_STARTED,
    PASSWORD_SETTINGS_UPDATE,
    PASSWORD_SETTINGS_FETCH_STARTED,
} from '../actions';
import {createReducer} from './utils';

const initialState = {
    isFetchingCurrencySettings: false,
    isFetchingBalanceSettings: false,
    isFetchingEmailSettings: false,
    isFetchingPasswordSettings: false,
    error: null
};

const handlers = {
    [CURRENCY_SETTINGS_UPDATE]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isFetchingCurrencySettings: false,
                error: null
            };
        }
        return {
            ...state,
            isFetchingCurrencySettings: false,
            error: action.payload
        };
    },
    [CURRENCY_SETTINGS_FETCH_STARTED]: (state = initialState) => {
        return {
            ...state,
            isFetchingCurrencySettings: true
        };
    },
    [ZERO_BALANCE_SETTINGS_UPDATE]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isFetchingBalanceSettings: false,
                error: null
            };
        }
        return {
            ...state,
            isFetchingBalanceSettings: false,
            error: action.payload
        };
    },
    [ZERO_BALANCE_FETCH_STARTED]: (state = initialState) => {
        return {
            ...state,
            isFetchingBalanceSettings: true
        };
    },
    [EMAIL_SETTINGS_UPDATE]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isFetchingEmailSettings: false,
                error: null
            };
        }
        return {
            ...state,
            isFetchingEmailSettings: false,
            error: action.payload
        };
    },
    [EMAIL_SETTINGS_FETCH_STARTED]: (state = initialState) => {
        return {
            ...state,
            isFetchingEmailSettings: true
        };
    },
    [PASSWORD_SETTINGS_UPDATE]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isFetchingPasswordSettings: false,
                error: null
            };
        }
        return {
            ...state,
            isFetchingPasswordSettings: false,
            error: action.payload
        };
    },
    [PASSWORD_SETTINGS_FETCH_STARTED]: (state = initialState) => {
        return {
            ...state,
            isFetchingPasswordSettings: true
        };
    }

};

export default createReducer(initialState, handlers);