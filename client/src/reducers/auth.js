/**
 * Created by Виталий on 24.07.2017.
 */
import {
    USER_SIGNUP,
    EMAIL_VERIFIED,
    USER_LOGGED_IN,
    USER_LOGGED_OUT,
    SAVE_USER_TOKEN,
    PASSWORD_RESET_REQUEST_SENT,
    PASSWORD_TOKEN_VERIFIED
} from '../actions';
import history  from './../history';
import {createReducer} from './utils';

const localStorage = window.localStorage;

let initialState = {
    isAuthenticated: isAuthenticated(),
    isFetching: false,
    isVerified: isVerified(),
    verificationStatus: null,
    userEmail: '',
    error: null,
    passwordResetRequested: false,
    passwordTokenVerified: false,
    passwordTokenVerification: false, //verification progress, can be replaced by is fetching later
    user: getCurrentUser(),
    currentUser: null, //current user holds value, while user gets it either from here or localstorage
    token: getToken() //todo change to get token
};

const handlers = {
    [USER_SIGNUP]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isFetching: false,
                isAuthenticated: true,
                token: action.payload.token,
                currentUser: setUser(action.payload.token),
                error: null
            };
        }
        return {
            ...state,
            isFetching: false,
            user: null,
            error: action.payload
        };
    },
    [EMAIL_VERIFIED]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isVerified: true,
                userEmail: action.payload.email,
                verificationStatus: 'success'
            };
        } else {
            return {
                ...state,
                verificationStatus: 'failed'
            };
        }
    },
    [PASSWORD_RESET_REQUEST_SENT]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                passwordResetRequested: true
            };
        } else {
            return {
                ...state,
                passwordResetRequested: false
            };
        }
    },
    [PASSWORD_TOKEN_VERIFIED]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                passwordTokenVerified: true,
                passwordTokenVerification: true
            };
        } else {
            return {
                ...state,
                passwordTokenVerified: false,
                passwordTokenVerification: true
            };
        }
    },
    [USER_LOGGED_IN]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isAuthenticated: true,
                token: action.payload.token,
                currentUser: setUser(action.payload.token),
                isFetching: false
            };
        } else {
            return {
                ...state,
                isFetching: false,
                error: action.payload
            };
        }
    },
    [SAVE_USER_TOKEN]: (state = initialState, action) => {
        return {
            ...state,
            isFetching: false,
            token: action.token,
            currentUser: setUser(action.token),
            user: getCurrentUser()
        };
    },
    [USER_LOGGED_OUT]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isAuthenticated: false,
                token: null,
                currentUser: null,
                isFetching: false,
                user: getCurrentUser()
            };
        } else {
            return {
                ...state,
                isFetching: false,
                error: action.payload
            };
        }
    }
};

export default createReducer(initialState, handlers);

function isAuthenticated() {
    return localStorage.getItem('token');
}

function getToken() {
    return localStorage.getItem('token'); //TODO remove extra token
}

function getCurrentUser() {
    if (initialState && initialState.currentUser) {
        return initialState.currentUser;
    } else {
        //if no user
        let user = localStorage.getItem('user');
        try {
            return JSON.parse(user);
        } catch (e) {
            return  history.push('/login');
        }
    }
}

function isVerified() {
    return false;
}

function parseJwt(token) {
    let base64Url = token.split('.')[1];
    let base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
}

function setUser(token) {
    let parsedUser = parseJwt(token);
    return parsedUser.data;
}