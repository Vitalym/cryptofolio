/**
 * Created by Виталий on 23.07.2017.
 */
import { combineReducers } from 'redux';
import investments from './investments';
import auth from './auth';
import allocations from './allocations';
import settings from './settings';
import notifications from './notifications';
import currencies from './currencies';
import loading from './loading';
import nav from './nav';
import location from './location';

export default combineReducers({
    investments,
    auth,
    currencies,
    allocations,
    location,
    settings,
    loading,
    nav,
    notifications
});