import { LOCATION_CHANGE } from 'react-router-redux';
import {createReducer} from './utils';

const initialState = {
    pathName: null,
    previousPathName: null
};

const handlers = {
    [LOCATION_CHANGE]: (state = initialState, action) => {
        if(!state.previousPathName) {
            return {
                ...state,
                pathName: action.payload.pathname,
                previousPathName: action.payload.pathname
            }
        } else {
            return {
                ...state,
                pathName: action.payload.pathname
            }
        }

    }
};

export default createReducer(initialState, handlers)