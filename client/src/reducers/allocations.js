/**
 * Created by Виталий on 23.07.2017.
 */
import * as _ from 'lodash';
import {
    HOLDINGS_ALLOCATION_FETCH,
    HOLDINGS_ALLOCATION_FETCH_STARTED,
    SORT_HOLDINGS_ALLOCATION_TABLE,
    TOGGLE_ALLOCATION_DIALOG,
    SORT_ALLOCATION_SETUP_TABLE,
    ALLOCATION_VALUE_UPDATED,
    ALLOCATION_VALUES_SET
} from '../actions';
import {createReducer} from './utils';

const initialState = {
    holdings: [],
    allocationsSchema: [], //same as holdings, to display in dialog table
    totalPortfolioValue: null,
    holdingsSorting: { //for table sorting on investments page
        order: 'desc',
        orderBy: 'share'
    },
    allocationsSorting: {
        order: 'desc',
        orderBy: 'share'
    },
    dialogIsOpen: false,
    allocationSchemaFields: [], //field values from allocation form
    isFetching: false,
    error: null
};

const handlers = {
    [HOLDINGS_ALLOCATION_FETCH]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isFetching: false,
                error: null,
                holdings: _.orderBy(action.payload.holdings, [state.holdingsSorting.orderBy], [state.holdingsSorting.order]),
                allocationsSchema: _.orderBy(action.payload.holdings, [state.allocationsSorting.orderBy], [state.allocationsSorting.order]),
                totalPortfolioValue: action.payload.totalPortfolioValue
            };
        }
        return {
            ...state,
            isFetching: false,
            error: action.payload
        };
    },
    [SORT_HOLDINGS_ALLOCATION_TABLE]: (state = initialState, action) => {
        const order = state.holdingsSorting.orderBy === action.payload.orderBy && state.holdingsSorting.order === 'desc' ? 'asc' : 'desc';
        return {
            ...state,
            holdingsSorting: {
                orderBy: action.payload.orderBy,
                order: order
            },
            holdings: _.orderBy(state.holdings, [action.payload.orderBy], [order])
        };
    },
    [SORT_ALLOCATION_SETUP_TABLE]: (state = initialState, action) => {
        const order = state.allocationsSorting.orderBy === action.payload.orderBy && state.allocationsSorting.order === 'desc' ? 'asc' : 'desc';
        return {
            ...state,
            holdingsSorting: {
                orderBy: action.payload.orderBy,
                order: order
            },
            allocationsSchema: _.orderBy(action.payload.allocationsSchema, [action.payload.orderBy], [order])
        };
    },
    [TOGGLE_ALLOCATION_DIALOG]: (state = initialState) => {
        return {
            ...state,
            dialogIsOpen: !state.dialogIsOpen
        };
    },
    [ALLOCATION_VALUE_UPDATED]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                //...state.arr, action.newItem
                allocationSchemaFields: saveField(state.allocationSchemaFields, action.payload)
            };
        }
        return {
            ...state
        };
    },
    [ALLOCATION_VALUES_SET]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                //...state.arr, action.newItem
                allocationSchemaFields: _.map(state.holdings, asset => {
                    let allocationValue = {};
                    allocationValue.coin = asset.coin.id;
                    allocationValue.value = asset.targetAllocation > 0 ? asset.targetAllocation : asset.share;
                    return allocationValue;
                })
            };
        }
        return {
            ...state
        };
    },
    [HOLDINGS_ALLOCATION_FETCH_STARTED]: (state = initialState) => {
        return {
            ...state,
            isFetching: true
        };
    },
};

export default createReducer(initialState, handlers);

function saveField (array, field) {
    //update allocationSchemaFields
    let arrayEl = _.find(array, ['coin', field.coin]); //check if it's value is in array already
    if(!arrayEl){
        //push it to array
        return [...array, field]
    } else {
        //otherwise we update the value
        return array.map(asset =>
            // transform the one with a matching id, otherwise return original
            asset.coin === field.coin ? field : asset
        )
    }
}