/**
 * Created by Виталий on 23.07.2017.
 */
import {
    CURRENCIES_FETCH,
    COIN_DATA_FETCH,
    COIN_DATA_FETCH_STARTED,
    CURRENCIES_SEARCH,
    CURRENCIES_SEARCH_STARTED,
    SET_CURRENCIES_SEARCH_QUERY,
    CLEAR_COINS_SEARCH
} from '../actions';
import {createReducer} from './utils';
import {parseJson} from '../utils/utils';

const initialState = {
    currencies: [],
    currenciesTimestamp: null,
    coinData:[],
    coinDataSkip: 0,
    coinDataLimit: 50,
    coinSorting: {
        order: 'desc',
        orderBy: 'market_cap_usd'
    },
    searchResults: [],
    searchQuery: null,
    isFetchingCoinData: false,
    isFetching: false,
    isSearching: false,
    error: null
};

const handlers = {
    [CURRENCIES_FETCH]: (state = initialState, action) => {
        if (!action.error) {
            return Object.assign({}, state, {
                isFetching: false,
                error: null,
                currencies: parseJson(action.payload.coins, []),
                currenciesTimestamp: parseJson(action.payload.timestamp, null)
            });
        }
        return Object.assign({}, state, {
            isFetching: false,
            error: action.payload
        });
    },
    [COIN_DATA_FETCH]: (state = initialState, action) => {
        let response = parseJson(action.payload.coinData, []);
        if (!action.error) {
            return Object.assign({}, state, {
                isFetchingCoinData: false,
                error: null,
                coinData: state.coinData.length ? state.coinData.concat(response) : response,
                coinDataSkip: state.coinDataSkip++
            });
        }
        return Object.assign({}, state, {
            isFetchingCoinData: false,
            error: action.payload
        });
    },
    [COIN_DATA_FETCH_STARTED]: (state = initialState) => {
        return {
            ...state,
            isFetchingCoinData: true
        };
    },
    [CURRENCIES_SEARCH_STARTED]: (state = initialState) => {
        return {
            ...state,
            isSearching: true
        };
    },
    [SET_CURRENCIES_SEARCH_QUERY]: (state = initialState, action) => {
        return {
            ...state,
            searchQuery: action.query && action.query.length ? action.query : null
        };
    },
    [CLEAR_COINS_SEARCH]: (state = initialState) => {
        return {
            ...state,
            searchQuery: null,
            searchResults: []
        };
    },
    [CURRENCIES_SEARCH]: (state = initialState, action) => {
        if (!action.error) {
            return Object.assign({}, state, {
                isSearching: false,
                error: null,
                searchResults: action.payload.result
            });
        }
        return Object.assign({}, state, {
            isFetching: false,
            error: action.payload
        });
    },
};

export default createReducer(initialState, handlers);