/**
 * Created by dev on 24.8.17.
 */
import {
    SHOW_NOTIFICATION,
    HIDE_NOTIFICATION
} from '../actions';
import {createReducer} from './utils';

const initialState = {
    message: null,
    isOpen: false
};

const handlers = {
    [SHOW_NOTIFICATION]: (state = initialState, action) => {
        return {
            ...state,
            message: action.payload.message,
            isOpen: true
        }
    },

    [HIDE_NOTIFICATION]: (state = initialState) => {
        return {
            ...state,
            message: null,
            isOpen: false
        }
    }
};

export default createReducer(initialState, handlers)