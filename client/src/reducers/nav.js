/**
 * Created by Виталий on 30.07.2017.
 */
import {
    NAV_ENABLE,
    NAV_LANDING_PAGE,
    SIDENAV_TOGGLE
} from '../actions';
import {createReducer} from './utils';

const initialState = {
    enabled: true,
    isLanding: false,
    isOpen: false
};

const handlers = {
    [NAV_ENABLE]: (state = initialState, action) => {
        return {
            ...state,
            enabled: action.enabled
        }
    },

    [NAV_LANDING_PAGE]: (state = initialState, action) => {
        return {
            ...state,
            isLanding: action.enabled
        }
    },

    [SIDENAV_TOGGLE]: (state = initialState) => {
        return {
            ...state,
            isOpen: !state.isOpen
        }
    }
};

export default createReducer(initialState, handlers)