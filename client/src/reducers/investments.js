/**
 * Created by Виталий on 23.07.2017.
 */
import * as _ from 'lodash';
import {
    INVESTMENTS_FETCH,
    INVESTMENT_ADDED,
    INVESTMENT_UPDATED,
    SINGLE_INVESTMENTS_FETCH,
    INVESTMENTS_BY_COIN_FETCH,
    INVESTMENT_REMOVED,
    INVESTMENTS_FETCH_STARTED,
    SORT_HISTORY_TABLE,
    SORT_HOLDINGS_TABLE,
    SORT_COIN_TRANSACTIONS_TABLE,
    TABLE_SORTING_STARTED,
    HISTORY_DATA_FETCH,
    HISTORY_DATA_FETCH_STARTED
} from '../actions';
import {getItemPrice} from '../utils/utils';
import {createReducer} from './utils';

const initialState = {
    investments: [],
    selectedTransaction: {},
    selectedTransactionFormState: {},
    holdings: [],
    totalPortfolioValue: null,
    transactionsByCoin: [],
    transactionsByCoinStats: [],
    transactionsByCoinHistory: [ [0, 0] ],
    historySorting: { //for table sorting on investments page
        order: 'asc',
        orderBy: 'dateBought'
    },
    holdingsSorting: { //for table sorting on investments page
        order: 'desc',
        orderBy: 'totalUsdValue'
    },
    coinTransactionSorting: {
        order: 'desc',
        orderBy: 'dateBought'
    },
    isSorting: false,
    isFetching: false,
    isFetchingCoinHistory: false,
    error: null
};

const handlers = {
    [INVESTMENTS_FETCH]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isFetching: false,
                isSorting: false,
                error: null,
                investments: _.orderBy(action.payload.investments, [state.historySorting.orderBy], [state.historySorting.order]),
                holdings: _.orderBy(action.payload.holdings, [state.holdingsSorting.orderBy], [state.holdingsSorting.order]),
                totalPortfolioValue: action.payload.totalPortfolioValue
            };
        }
        return {
            ...state,
            isSorting: false,
            isFetching: false,
            error: action.payload
        };
    },
    [INVESTMENTS_BY_COIN_FETCH]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isSorting: false,
                isFetching: false,
                error: null,
                transactionsByCoinStats: action.payload.stats[0],
                transactionsByCoin: _.orderBy(action.payload.transactions, [state.coinTransactionSorting.orderBy], [state.coinTransactionSorting.order]),
            };
        }
        return {
            ...state,
            isSorting: false,
            isFetching: false,
            error: action.payload
        };
    },
    [HISTORY_DATA_FETCH]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isFetchingCoinHistory: false,
                error: null,
                transactionsByCoinHistory: action.payload.history
            };
        }
        return {
            ...state,
            isFetchingCoinHistory: false,
            error: action.payload
        };
    },
    [SINGLE_INVESTMENTS_FETCH]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                isSorting: false,
                isFetching: false,
                error: null,
                selectedTransaction: action.payload.investment,
                selectedTransactionFormState: {
                    action: action.payload.investment.action,
                    priceType: action.payload.investment.priceType,
                    currency: action.payload.investment.currency,
                    dateBought: new Date(action.payload.investment.dateBought),
                    coin: `${action.payload.investment.coin.name} (${action.payload.investment.coin.symbol})`,
                    price: getItemPrice(action.payload.investment.priceType, action.payload.investment.currency, action.payload.investment.amount, action.payload.investment.usdPricePerItem, action.payload.investment.btcPricePerItem ),
                    amount: action.payload.investment.amount,
                    note: action.payload.investment.note,
                    connectTransaction: !!action.payload.investment.connectedTransaction
                }
            };
        }
        return {
            ...state,
            isSorting: false,
            isFetching: false,
            error: action.payload
        };
    },
    [INVESTMENT_ADDED]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                // investments: [...state.investments, action.payload.investment],
                isSorting: false,
                error: null,
                isFetching: false
            };
        }
        return {
            ...state,
            isFetching: false,
            isSorting: false,
            error: action.payload
        };
    },
    [INVESTMENT_UPDATED]: (state = initialState, action) => {
        if (!action.error) {
            return {
                ...state,
                selectedTransaction: action.payload.investment,
                isSorting: false,
                error: null,
                isFetching: false
            };
        }
        return {
            ...state,
            isSorting: false,
            isFetching: false,
            error: action.payload
        };
    },
    [INVESTMENT_REMOVED]: (state = initialState, action) => {
        if (!action.error) {

            const indexToDelete = state.investments.findIndex(investment => {
                return investment.id === action.id
            });

            return {
                ...state.investments.splice(indexToDelete, 1),
                isSorting: false,
                isFetching: false
            };
        }
        return {
            ...state,
            isSorting: false,
            isFetching: false,
            error: action.payload
        };
    },
    [INVESTMENTS_FETCH_STARTED]: (state = initialState) => {
        return {
            ...state,
            isSorting: false,
            isFetching: true
        };
    },
    [HISTORY_DATA_FETCH_STARTED]: (state = initialState) => {
        return {
            ...state,
            isFetchingCoinHistory: true
        };
    },
    [SORT_HISTORY_TABLE]: (state = initialState, action) => {
        const order = state.historySorting.orderBy === action.payload.orderBy && state.historySorting.order === 'desc' ? 'asc' : 'desc';
        return {
            ...state,
            historySorting: {
                orderBy: action.payload.orderBy,
                order: order
            },
            isSorting: false,
            investments: _.orderBy(state.investments, [action.payload.orderBy], [order])
        };
    },
    [SORT_HOLDINGS_TABLE]: (state = initialState, action) => {
        const order = state.holdingsSorting.orderBy === action.payload.orderBy && state.holdingsSorting.order === 'desc' ? 'asc' : 'desc';
        return {
            ...state,
            holdingsSorting: {
                orderBy: action.payload.orderBy,
                order: order
            },
            isSorting: false,
            holdings: _.orderBy(state.holdings, [action.payload.orderBy], [order])
        };
    },
    [SORT_COIN_TRANSACTIONS_TABLE]: (state = initialState, action) => {
        const order = state.coinTransactionSorting.orderBy === action.payload.orderBy && state.coinTransactionSorting.order === 'desc' ? 'asc' : 'desc';
        return {
            ...state,
            coinTransactionSorting: {
                orderBy: action.payload.orderBy,
                order: order
            },
            isSorting: false,
            transactionsByCoin: _.orderBy(state.transactionsByCoin, [action.payload.orderBy], [order])
        };
    },
    [TABLE_SORTING_STARTED]: (state = initialState) => {
        return {
            ...state,
            isSorting: true
        };
    }
};

export default createReducer(initialState, handlers);