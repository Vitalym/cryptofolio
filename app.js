'use strict';
const express = require('express');
const config = require('./config/config');
const glob = require('glob');
const mongoose = require('mongoose');
const http = require('http');
const Promise = require("bluebird");
const spdy = require('spdy');
const fs = require('fs');

//winston setup
const winston = require('winston');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '/log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-server.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

//promisify mongoose
Promise.promisifyAll(require("mongoose"));
//mongoose.Promise = require('bluebird'); <-- maybe

mongoose.connect(config.db);
const db = mongoose.connection;
db.on('error', function () {
    throw new Error('unable to connect to database at ' + config.db);
});

const models = glob.sync(config.root + '/app/models/*.js');
models.forEach((model) => {
    require(model);
});
const app = express();

//socket io goes here
const server = http.Server(app);
const io = require('socket.io')(server);
const externalApi = require('./app/external/currencies');
const seed = require('./app/seed/seed');

module.exports = require('./config/express')(app, config);

//was app before SERVER
server.listen(config.port, () => {
    logger.info('Express server listening on port ' + config.port);
});

externalApi.getBitcoinHistory();
externalApi.getCurrenciesList();

const schedule = require('node-schedule');
const rule = new schedule.RecurrenceRule();
rule.minute = new schedule.Range(0, 59, 10);

schedule.scheduleJob(rule, externalApi.updateCurrenciesList);

const btcPriceUpdateRule = new schedule.RecurrenceRule();
btcPriceUpdateRule.minute = 59;
btcPriceUpdateRule.hour = 23;
schedule.scheduleJob(btcPriceUpdateRule, externalApi.writeBitcoinPrice);
/*seed.seedDevDatabase().then(result => {
    console.log(result);
})
    .catch(err => {
        console.log(err);
    });*/

//TODO remove later
io.on('connection', (socket) => {

    socket.emit('connected', {hello: 'world'});
});
