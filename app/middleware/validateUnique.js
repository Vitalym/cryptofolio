/**
 * Created by Виталий on 24.07.2017.
 */
"use strict";
const mongoose = require('mongoose'),
    User = mongoose.model('User'),
    errorFactory = require('../helpers/errorFactory');

//winston setup
const winston = require('winston');
const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat
        })
    ]
});
logger.level = 'debug';

module.exports = {
    userEmail: (req, res, next) => {
        if (!req.body.email) {
            let error = errorFactory.missingParam();
            return next(error);
        }
        User.findOne({email: req.body.email.toLowerCase().trim()})
            .exec()
            .then(user => {
                if (!user) {
                    next();
                } else {
                    let error = errorFactory.unique(`We already have a user associated with this email`);
                    return next(error);
                }
            })
            .catch(err => {
                return next(err);
            })
    }
};