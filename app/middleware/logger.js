/**
 * Created by decipher on 28.2.17.
 */
//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      colorize: true,
      timestamp: tsFormat,
      level: 'debug'
    }),
    new (require('winston-daily-rotate-file'))({
      filename: `${logDir}/-api-log.log`,
      timestamp: tsFormat,
      datePattern: 'yyyy-MM-dd',
      prepend: true,
      level: env === 'development' ? 'debug' : 'info'
    })
  ]
});

module.exports = {
  logger: (req, res, next) => {
    logger.debug(new Date(), req.method, req.url);
    next();
  }
};
