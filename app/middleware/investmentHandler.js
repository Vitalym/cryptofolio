/**
 * Created by Виталий on 04.08.2017.
 */
const mongoose = require('mongoose'),
    async = require('async'),
    redis = require('../redis/redis'),
    moment = require('moment'),
    Investment = mongoose.model('Investment'),
    Currency = mongoose.model('Currency'),
    BitcoinPrice = mongoose.model('BitcoinPrice'),
    investmentHelpers = require('../helpers/investments'),
    errorFactory = require('../helpers/errorFactory'),
    errorHandler = require('../helpers/errorHandler');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-investments.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

module.exports = {
    populateInvestment: (req, res, next) => {
        let investments, holdings, totalPortfolioValue;
        return investmentHelpers.populateInvestmentsHistory(req.investments)
            .then(populatedInvestments => {
                investments = populatedInvestments;
                return investmentHelpers.mergeUserHoldings(investments);
            })
            .then(mergedHoldings => {
                return investmentHelpers.calculateUserHoldings(mergedHoldings);
            })
            .then(userHoldings => {
                return investmentHelpers.removeZeroValueHoldings(userHoldings);
            })
            .then(clearedHoldings => {
                holdings = clearedHoldings;
                return investmentHelpers.calculateTotalValue(holdings);
            })
            .then(totalValue => {
                totalPortfolioValue = totalValue;
                return investmentHelpers.calculateShare(holdings, totalValue.totalUSDValue)
            })
            .then(holdingsWithShares => {

                let hrend = process.hrtime(req.hrstart);
                logger.info("Total execution time 'populateInvestment' (hr): %ds %dms", hrend[0], hrend[1] / 1000000);

                let data = {
                    holdings: holdingsWithShares,
                    investments: investments,
                    totalPortfolioValue: totalPortfolioValue,
                };

                redis.setex(`investments-${req.user._id}`, 5, JSON.stringify(data));

                return res.status(200).json({
                    holdings: holdingsWithShares,
                    investments: investments,
                    totalPortfolioValue: totalPortfolioValue,
                    token: req.token
                });
            })
            .catch(err => {
                return next(err)
            });
    },

    populateHoldingsAllocation: (req, res, next) => {
        let investments, holdings, totalPortfolioValue;
        return investmentHelpers.populateInvestmentsHistory(req.investments)
            .then(populatedInvestments => {
                investments = populatedInvestments;
                return investmentHelpers.mergeUserHoldings(investments);
            })
            .then(mergedHoldings => {
                return investmentHelpers.calculateUserHoldings(mergedHoldings);
            })
            /*  .then(userHoldings => {
                  return investmentHelpers.removeZeroValueHoldings(userHoldings);
              })*/
            .then(userHoldings => {
                holdings = userHoldings;
                return investmentHelpers.calculateTotalValue(holdings);
            })
            .then(totalValue => {
                totalPortfolioValue = totalValue;
                return investmentHelpers.calculateShare(holdings, totalValue.totalUSDValue)
            })
            .then(holdingsWithShares => {
                //calculate target allocation
                return investmentHelpers.addTargetAllocation(holdingsWithShares, req.user.currentPortfolio.targetAllocation)
            })
            .then(holdingsWithAllocation => {
                return investmentHelpers.calculateAmountToBuy(holdingsWithAllocation, totalPortfolioValue.totalUSDValue, totalPortfolioValue.totalBTCValue)
            })
            .then(holdingsWithRebalance => {
                return res.status(200).json({
                    //sorted: sorted,
                    holdings: holdingsWithRebalance,
                    investments: investments,
                    totalPortfolioValue: totalPortfolioValue,
                    token: req.token
                });
            })
            .catch(err => {
                return next(err)
            });
    },

    populateCoinTransactions: (req, res, next) => {
        let transactions, stats, history = {
            price: null,
            market_cap: null
        };
        return investmentHelpers.populateTransactionHistory(req.transactions)
            .then(populatedTransactions => {
                transactions = populatedTransactions;
                return investmentHelpers.mergeTransactionsValue(transactions);
            })
            .then(mergedTransaction => {
                return investmentHelpers.calculateTotalTransactionsValue(mergedTransaction);
            })
            .then(totalValue => {
                stats = totalValue;
                //TODO fetch history
                let hrend = process.hrtime(req.hrstart);
                logger.info("Total execution time 'populateInvestment' (hr): %ds %dms", hrend[0], hrend[1] / 1000000);
                return res.status(200).json({
                    //sorted: sorted,
                    transactions: req.transactions,
                    stats: stats,
                    token: req.token
                });
            })
            .catch(err => {
                return next(err);
            });
    },

    //check that user can't sell more than he has
    checkSellAmount: (req, res, next) => {
        if (req.body.action !== 'sell') {
            return next();
        } else {
            return investmentHelpers.calculateMaxSellAmount(req.body.normalizedCoin.id, req.body.amount, req.user._id)
                .then(result => {
                    return next();
                })
                .catch(err => {
                    return next(err);
                })
        }
    },

    getBTCPrice: (req, res, next) => {
        //get btc price by the date of transaction
        let transactionDate = req.body.dateBought || new Date();
        //check if date is today
        let today = moment();
        let dateBought = moment(transactionDate);
        let formattedDateBought = dateBought.startOf('day');

        if (today.isSame(dateBought, 'd')) {
            // They are on the same day, so we pick current price from redis
            redis.get('bitcoinPrice', (err, result) => {
                if (err) {
                    return next(err);
                }
                if (!result) {
                    req.btcPrice = 0; //TODO probably shall be an error here
                    return next();
                }

                let price = JSON.parse(result);
                req.btcPrice = JSON.parse(result);
                return next();

            });
        } else {
            // They are not on the same day, so we try to find this date in mongoose
            BitcoinPrice.findOne({formattedDate: formattedDateBought}).exec()
                .then(result => {
                    if (!result) {
                        req.btcPrice = 0;
                        return next();
                    }
                    req.btcPrice = result.price;
                    return next();
                })
                .catch(err => {
                    return next(err);
                })
        }

    },

    calculateTransactionPrices: (req, res, next) => {
        //calculate price per item for the transaction
        let btcActualPrice = req.btcPrice; //btc price at the time of transaction
        if (req.body.currency === 'USD') {
            req.usdPricePerItem = req.body.priceType === 'per coin' ? req.body.price : req.body.price / req.body.amount;
            req.btcPricePerItem = req.body.priceType === 'per coin' ? req.body.price / btcActualPrice : (req.body.price / req.body.amount) / btcActualPrice;
            return next();
        } else if (req.body.currency === 'BTC') {
            req.btcPricePerItem = req.body.priceType === 'per coin' ? req.body.price : req.body.price / req.body.amount;
            req.usdPricePerItem = req.body.priceType === 'per coin' ? req.body.price * btcActualPrice : (req.body.price / req.body.amount) * btcActualPrice;
            return next();
        } else {
            let error = errorFactory.notAllowed(`unsupported currency`);
            return next(error);
        }
    },

    recalculateBTCHoldings: (req, res, next) => {
        // if user is buying or selling with btc he already has, recalculate btc holdings
        if (!req.body.connectTransaction) {
            return next();
        } else if (req.body.normalizedCoin.id === 'bitcoin') {
            return next();
        } else {
            let newInvestment = new Investment();

            newInvestment.coin = {
                id: 'bitcoin',
                name: 'Bitcoin',
                symbol: 'BTC'
            };
            newInvestment.dateBought = req.body.dateBought || new Date();
            newInvestment.btcPricePerItem = 1; // price per item will be 1 btc per item, apparently...
            newInvestment.usdPricePerItem = req.btcPrice; // actual btc price
            newInvestment.usdAcquisitionCost = req.body.amount * parseFloat(req.usdPricePerItem); // ?????
            newInvestment.btcAcquisitionCost = req.body.amount * parseFloat(req.btcPricePerItem); //????
            newInvestment.note = req.body.note; // TODO add note 'exchanged'
            newInvestment.amount = req.body.amount * req.btcPricePerItem; //amount of btc equals btc price per item * amount
            newInvestment.action = req.body.action === 'buy' ? 'sell' : 'buy';
            newInvestment.user = req.user._id;
            newInvestment.currency = req.body.currency || 'USD';
            newInvestment.portfolio = req.user.currentPortfolio;

            newInvestment.save()
                .then(investment => {
                    req.connectedTransaction = investment._id;
                    return next();
                })
                .catch(err => {
                    return next(err)
                });

        }
    },

    getOriginalTransaction: (req, res, next) => {
        //get original transaction for edit route
        Investment.findById(req.params.id).exec()
            .then(investment => {
               if(!investment) {
                   let error = errorFactory.notFound();
                   return next(error);
               }

               //determine action
                if(req.body.coin.id === 'bitcoin'){
                   //in that case we remove connected transaction if any
                    if(investment.connectedTransaction){
                        req.connectedTransaction = investment.connectedTransaction;
                        req.editAction = 'delete';
                        return next ();
                    } else {
                        return next ()
                    }
                } else if(investment.connectedTransaction){
                   if(req.body.connectTransaction){
                       req.connectedTransaction = investment.connectedTransaction;
                       req.editAction = 'edit';
                       return next ();
                   } else {
                       req.connectedTransaction = investment.connectedTransaction;
                       req.editAction = 'delete';
                       return next ();
                   }
                } else {
                    if(req.body.connectTransaction){
                        req.editAction = 'create';
                        return next ();
                    } else {
                        return next ();
                    }
                }
            })
            .catch(err => {
                return next(err)
            });
    },

    editConnectedTransaction: (req, res, next) => {
        if(!req.editAction){
            return next();
        }

        switch (req.editAction){
            case 'edit':
                const update = {
                    $set: {
                        dateBought: req.body.dateBought || new Date(),
                        btcPricePerItem: 1,
                        usdPricePerItem: req.btcPrice,
                        usdAcquisitionCost: req.body.amount * parseFloat(req.usdPricePerItem),
                        btcAcquisitionCost: req.body.amount * req.btcPricePerItem,
                        note: req.body.note,
                        action: req.body.action === 'buy' ? 'sell' : 'buy',
                        amount: req.body.amount * req.btcPricePerItem,
                        currency: req.body.currency || 'USD'
                    }
                };

                return Investment.findByIdAndUpdate(req.connectedTransaction, update, {upsert: true, new: true})
                    .exec()
                    .then(investment => {
                        return next();
                    })
                    .catch(err => {
                        return next(err)
                    });
            case 'create':
                let newInvestment = new Investment();

                newInvestment.coin = {
                    id: 'bitcoin',
                    name: 'Bitcoin',
                    symbol: 'BTC'
                };
                newInvestment.dateBought = req.body.dateBought || new Date();
                newInvestment.btcPricePerItem = 1; // price per item will be 1 btc per item, apparently...
                newInvestment.usdPricePerItem = req.btcPrice; // actual btc price
                newInvestment.usdAcquisitionCost = req.body.amount * parseFloat(req.usdPricePerItem); // ?????
                newInvestment.btcAcquisitionCost = req.body.amount * parseFloat(req.btcPricePerItem); //????
                newInvestment.note = req.body.note; // TODO add note 'exchanged'
                newInvestment.amount = req.body.amount * req.btcPricePerItem; //amount of btc equals btc price per item * amount
                newInvestment.action = req.body.action === 'buy' ? 'sell' : 'buy';
                newInvestment.user = req.user._id;
                newInvestment.portfolio = req.user.currentPortfolio;
                newInvestment.currency = req.body.currency || 'USD';

                return newInvestment.save()
                    .then(investment => {
                        req.connectedTransaction = investment._id;
                        return next();
                    })
                    .catch(err => {
                        return next(err)
                    });
            case 'delete':
                return Investment.findByIdAndRemove(req.connectedTransaction)
                    .exec()
                    .then(result => {
                        // we will set connected transaction to null in main action
                        return next();
                    })
                    .catch(err => {
                        return next(err)
                    });

            default:
                return next();
        }

    }
};