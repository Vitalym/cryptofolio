'use strict';
const redis = require('../redis/redis'),
    config = require('../../config/config'),
    errorFactory = require('../helpers/errorFactory');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-redis.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

module.exports = {
    // route middleware to get redis cached data if available

    investmentsCache: (req, res, next) => {
        let hrstart = process.hrtime();

        redis.get(`investments-${req.user._id}`, (err, result) => {
            if(err) {
                return next(err);
            }
            if(!result){
                console.log('no result');
                req.hrstart = hrstart;
                return next();
            }

            console.log('obtaining from cache');

            let data;

            try {
                data = JSON.parse(result);
            } catch (e) {
                return next();
            }

            let response = Object.assign(data, {token: req.token});

            let hrend = process.hrtime(hrstart);
            logger.info("Execution time 'getInvestments' from redis cache (hr): %ds %dms", hrend[0], hrend[1]/1000000);

            return res.status(200).json(response);

        });
    }
};