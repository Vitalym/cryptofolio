/**
 * Created by decipher on 13.1.17.
 */
'use strict';
const jwt = require('jsonwebtoken'),
    moment = require('moment'),
    config = require('../../config/config'),
    errorFactory = require('../helpers/errorFactory'),
    mongoose = require('mongoose'),
    User = mongoose.model('User');

module.exports = {
    // route middleware to ensure user is logged in

    ensureAuthenticated: (req, res, next) => {

        if (!req.headers.authorization) {
            let error = errorFactory.unauthorized();
            return next(error);
        }
        var token = req.headers.authorization.split(' ')[1];

        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                if (err.name == 'TokenExpiredError') {
                    let error = errorFactory.unauthorized('Token has expired');
                    return next(error);
                } else {
                    console.log('token is invalid');
                    let error = errorFactory.unauthorized('Invalid token');
                    return next(error);
                }
            }
            //this might be not needed as jsonwebtoken would throw TokenExpiredError on the top
            if (decoded.exp <= moment().unix()) {
                let error = errorFactory.unauthorized('Token has expired');
                return next(error);
            }

            req.user = decoded.data;

            //check that user registered in token exists in database
            User.findById(req.user._id)
                .exec()
                .then(user => {
                    if (!user) {
                        let error = errorFactory.unauthorized('User not found');
                        return next(error);
                    }

                    //refresh token
                    req.token = jwt.sign({
                        data: decoded.data,
                        exp: Math.floor(moment() / 1000) + (60 * 60 * 24)
                    }, config.secret);

                    return next();
                })
                .catch(err => {
                    return next(err);
                });
        })
    }
};
