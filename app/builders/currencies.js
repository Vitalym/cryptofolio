/**
 * Created by Виталий on 01.08.2017.
 */
var request = require('request-promise');

module.exports = {
    getCurrenciesList: () => {
        const options = {
            method: 'GET',
            uri: 'https://api.coinmarketcap.com/v1/ticker/'
        };

        request(options)
            .then(response => {
                // Request was successful, use the response object at will
            })
            .catch(err => {
                // Something bad happened, handle the error
                //TODO retry on timeout until successful, save error message
            })
    }
};