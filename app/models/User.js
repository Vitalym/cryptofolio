/**
 * Created by Виталий on 09.01.2017.
 */
'use strict';
const mongoose = require('mongoose'),
    bcrypt = require('bcryptjs'),
    Schema = mongoose.Schema,
    SALT_WORK_FACTOR = 10;

const UserSchema = new Schema({
    username: {
        type: String,
        trim: true,
        lowercase: true
    },
    image: {type: String},
    email: {
        type: String,
        lowercase: true,
        trim: true
    },
    settings: {
        currency: {type: String, default: 'USD'},
        hideZeroBalances: {type: Boolean, default: true}
    },
    facebook: {type: String},
    displayName: {type: String},
    currentPortfolio: {type: Schema.Types.ObjectId, ref: 'Portfolio'},
    password: {type: String},
    lastLogin: {},
    isVerified: {type: Boolean, default: false},
    verificationToken: {type: String},
    resetToken: {type: String},
    isAllowedToReset: {type: Boolean, default: false},
    created: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
    removed: {type: Boolean, default: false}
});

//hash password with bcrypt
UserSchema.pre('save', hashPassword);

UserSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

function hashPassword(next) {
    let user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    if (!user.password) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);

        // hash the password along with our new salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
}

mongoose.model('User', UserSchema);



