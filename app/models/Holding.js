/**
 * Created by dev on 30.8.17.
 */
'use strict';
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const HoldingSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    portfolio: {type: Schema.Types.ObjectId, ref: 'Portfolio'},
    coin: {
        id: {type: String},
        symbol: {type: String},
        name: {type: String}
    },
    usdAcquisitionCost: {type: Number},
    btcAcquisitionCost: {type: Number},
    totalAmount: {type: Number},
    totalBuyAmount: {type: Number},
    itemsUsdAcquisitionCost: {type: Number},
    itemsBtcAcquisitionCost: {type: Number},
    totalUsdSpent: {type: Number},
    totalBtcSpent: {type: Number},
    averageUsdAcquisitionCost: {type: Number},
    averageBtcAcquisitionCost: {type: Number},
    totalUsdProfitPercentage: {type: Number},
    totalBtcProfitPercentage: {type: Number},
    created: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date
    },
    removed: {type: Boolean, default: false}
});

mongoose.model('Holding', HoldingSchema);