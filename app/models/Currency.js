/**
 * Created by dev on 2.8.17.
 */
'use strict';
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const CurrencySchema = new Schema({
    id: {type: String},
    name: {type: String},
    symbol: {type: String},
    rank: {type: String},
    price_usd: {type: String},
    price_btc: {type: String},
    market_cap_usd: {type: String},
    available_supply: {type: String},
    total_supply: {type: String},
    percent_change_1h: {type: String},
    percent_change_24h: {type: String},
    percent_change_7d: {type: String},
    last_updated: {type: String},
    history: {
        market_cap: [],
        price: []
    }
});

CurrencySchema.index({id: 'text', name: 'text', symbol: 'text'});

mongoose.model('Currency', CurrencySchema);
