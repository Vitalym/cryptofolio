'use strict';
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const UserNotificationSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    alert: {type: Schema.Types.ObjectId, ref: 'Alert'},
    created: {
        type: Date,
        default: Date.now
    },
    value: {type: Number},
    triggerType: {type: String},
    currency: {type: String}
});

mongoose.model('UserNotification', UserNotificationSchema);