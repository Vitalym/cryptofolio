'use strict';
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const AlertSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    created: {
        type: Date,
        default: Date.now
    },
    value: {type: Number},
    updated_at: {
        type: Date,
        default: Date.now
    },
    triggerType: {type: String},
    currency: {type: String}
});

mongoose.model('Alert', AlertSchema);