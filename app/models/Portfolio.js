/**
 * Created by Виталий on 09.01.2017.
 */
'use strict';
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const PortfolioSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    name: {
        type: String,
        trim: true,
        lowercase: true
    },
    targetAllocation: {},
    rebalancePeriod: {type: String},
    rebalanceDate: {type: Date},
    autoRebalance: {type: Boolean, default: false}, //wether we should remind user to rebalance portfolio if rebalance period is set
    created: {
        type: Date,
        default: Date.now
    },
    updated_at: {type: Date},
    removed: {type: Boolean, default: false}
});

mongoose.model('Portfolio', PortfolioSchema);



