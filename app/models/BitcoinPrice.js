/**
 * Created by Виталий on 27.08.2017.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const BitcoinPriceSchema = new Schema({
    price: {type: Number},
    date: {type: Date},
    formattedDate: {type: Date}
});

mongoose.model('BitcoinPrice', BitcoinPriceSchema);