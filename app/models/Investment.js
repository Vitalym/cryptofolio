/**
 * Created by Виталий on 09.01.2017.
 */
'use strict';
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const InvestmentSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    portfolio: {type: Schema.Types.ObjectId, ref: 'Portfolio'},
    coin: {
        id: {type: String},
        symbol: {type: String},
        name: {type: String}
    },
    dateBought: {
        type: Date,
        default: Date.now
    },
    usdPricePerItem: {type: Number},
    btcPricePerItem: {type: Number},
    usdAcquisitionCost: {type: Number},
    btcAcquisitionCost: {type: Number},
    connectedTransaction: {type: Schema.Types.ObjectId, ref: 'Investment'}, //connected transaction for the exchange
    currency: {type: String},
    priceType: {type: String},
    amount: {type: Number},
    note: {type: String},
    created: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
    action: {type: String},
    log: {type: Array, default: []},
    removed: {type: Boolean, default: false}
});

mongoose.model('Investment', InvestmentSchema);



