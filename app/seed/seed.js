/**
 * Created by Виталий on 04.08.2017.
 */
const mongoose = require('mongoose'),
    async = require('async'),
    ObjectId = mongoose.Types.ObjectId,
    Currency = mongoose.model('Currency'),
    Portfolio = mongoose.model('Portfolio'),
    User = mongoose.model('User'),
    Investment = mongoose.model('Investment'),
    errorFactory = require('../helpers/errorFactory'),
    errorHandler = require('../helpers/errorHandler');

const UserId = ObjectId();
const UserPortfolioId = ObjectId();

const Seed = {
    seedDevDatabase: () => {
        return User.remove({}).exec()
            .then(result => {
                return Seed.seedUsers();
            })
            .then(result => {
                return Portfolio.remove({}).exec();
            })
            .then(result => {
                return Seed.seedPortfolio();
            })
            .then(result => {
                return Investment.remove({}).exec();
            })
            .then(result => {
                return Seed.seedInvestments()
            })
            .then(result => {
                return 'db seeded';
            })
            .catch(err => {
                console.log(err);
                return(err);
            });
    },

    seedUsers: () => {
        const users = [
            {
                _id: UserId,
                currentPortfolio: UserPortfolioId,
                username: "decipher",
                password: "123456",
                email: "vitaliy.maltsev@gmail.com",
                settings: {
                    currency: 'USD'
                },
                isVerified: true
            }
        ];
        return new Promise((resolve, reject) => {
            for (let user of users) {
                let newUser = new User(user);
                newUser.save();
            }

            return resolve('users seeded');
        })
    },

    seedPortfolio: () => {
        const portfolios = [
            {
                _id: UserPortfolioId,
                user: UserId,
                targetAllocation: [
                    {coin: 'bitcoin', value: '40'},
                    {coin: 'monero', value: '8'},
                    {coin: 'ark', value: '7'},
                    {coin: 'stox', value: '4'},
                    {coin: 'district0x', value: '7'},
                    {coin: 'neo', value: '4.5'},
                    {coin: 'litecoin', value: '3'},
                    {coin: 'vertcoin', value: '3'},
                    {coin: 'tenx', value: '4'},
                    {coin: 'iota', value: '5'},
                    {coin: 'dash', value: '3'},
                    {coin: 'zcash', value: '3'},
                    {coin: 'ripple', value: '1.7187210985732344'},
                    {coin: 'rise', value: '2.19'},
                    {coin: 'factom', value: '4.5'},
                ]

            }
        ];
        return new Promise((resolve, reject) => {
            for (let portfolio of portfolios) {
                let newPortfolio = new Portfolio(portfolio);
                newPortfolio.save();
            }

            return resolve('users seeded');
        })
    },

    seedInvestments: () => {
        const investments = [
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "BTC",
                    "name": "Bitcoin",
                    "id": "bitcoin"
                },
                "dateBought": new Date(2017, 6, 20),
                "created": new Date(),
                "amount": 0.22,
                "price": 2648.05027,
                "pricePerItem": 2648.05027,
                "usdPricePerItem": 2648.05027,
                "btcPricePerItem": 1,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
         /*   {
          "user": UserId,
          "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "BTC",
                    "name": "Bitcoin",
                    "id": "bitcoin"
                },
                "dateBought": new Date(2017, 6, 24),
                "created": new Date(),
                "amount": 0.1431,
                "price": 2655.1,
                "pricePerItem": 2655.1,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },*/
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "LTC",
                    "name": "Litecoin",
                    "id": "litecoin"
                },
                "dateBought": new Date(2017, 6, 19),
                "created": new Date(),
                "amount": 1.29505563,
                "price": 44.45,
                "pricePerItem": 44.45,
                "usdPricePerItem": 44.45, //2265 per btc
                "btcPricePerItem": 0.0196247240618,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "XRP",
                    "name": "Ripple",
                    "id": "ripple"
                },
                "dateBought": new Date(2017, 6, 29),
                "created": new Date(),
                "amount": 204,
                "price": 0.167,
                "pricePerItem": 0.167, //2723 per btc
                "usdPricePerItem": 0.167,
                "btcPricePerItem": 0.00006132941,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "DASH",
                    "name": "Dash",
                    "id": "dash"
                },
                "dateBought": new Date(2017, 6, 26),
                "created": new Date(),
                "amount": 0.1789,
                "price": 190,
                "pricePerItem": 190,
                "usdPricePerItem": 190, //2592 per btc
                "btcPricePerItem": 0.07330246913,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "XMR",
                    "name": "Monero",
                    "id": "monero"
                },
                "dateBought": new Date(2017, 6, 29),
                "created": new Date(),
                "amount": 0.7816,
                "price": 43,
                "pricePerItem": 43,
                "usdPricePerItem": 43, //2592 per btc
                "btcPricePerItem": 0.01658950617,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "XMR",
                    "name": "Monero",
                    "id": "monero"
                },
                "dateBought": new Date(2017, 7, 1),
                "created": new Date(),
                "amount": 0.86330935,
                "price": 41.7,
                "pricePerItem": 41.7,
                "usdPricePerItem": 41.7, //2869 per btc
                "btcPricePerItem": 0.01453468107,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "XMR",
                    "name": "Monero",
                    "id": "monero"
                },
                "dateBought": new Date(2017, 7, 1),
                "created": new Date(),
                "amount": 1.1804,
                "price": 41,
                "pricePerItem": 41,
                "usdPricePerItem": 41.7, //2869 per btc
                "btcPricePerItem": 0.01429069362,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "MIOTA",
                    "name": "IOTA",
                    "id": "iota"
                },
                "dateBought": new Date(2017, 6, 29),
                "created": new Date(),
                "amount": 54.77099,
                "price": 0.2612,
                "pricePerItem": 0.2612,
                "usdPricePerItem": 0.2612, //2592 per btc
                "btcPricePerItem": 0.0001007716,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
/*            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "ZEC",
                    "name": "Zcash",
                    "id": "zcash"
                },
                "dateBought": new Date(2017, 6, 29),
                "created": new Date(),
                "amount": 0.19428,
                "price": 175,
                "pricePerItem": 175,
                "usdPricePerItem": 175, //2592 per btc
                "btcPricePerItem": 0.06751543209,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "STX",
                    "name": "Stox",
                    "id": "stox"
                },
                "dateBought": new Date(2017, 7, 3),
                "created": new Date(),
                "amount": 32.9524,
                "price": 1.06820745024581,
                "pricePerItem": 1.06820745024581,
                "usdPricePerItem": 1.06820745024581, //2709 per btc
                "btcPricePerItem": 0.00039431799,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "VTC",
                    "name": "Vertcoin",
                    "id": "vertcoin"
                },
                "dateBought": new Date(2017, 7, 7),
                "created": new Date(),
                "amount": 110,
                "price": 0.52188,
                "pricePerItem": 0.52188,
                "usdPricePerItem": 1.06820745024581, //3185 per btc
                "btcPricePerItem": 0.00015558,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "PAY",
                    "name": "TenX",
                    "id": "tenx"
                },
                "dateBought": new Date(2017, 7, 7),
                "created": new Date(),
                "amount": 13,
                "price": 1.90685,
                "pricePerItem": 1.90685,
                "usdPricePerItem": 1.90685, //3185 per btc
                "btcPricePerItem": 0.00057100,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "NEO",
                    "name": "NEO",
                    "id": "neo"
                },
                "dateBought": new Date(2017, 7, 7),
                "created": new Date(),
                "amount": 3,
                "price": 17.39189,
                "pricePerItem": 17.39189,
                "usdPricePerItem": 17.39189,
                "btcPricePerItem": 0.00518862,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "ARK",
                    "name": "Ark",
                    "id": "ark"
                },
                "dateBought": new Date(2017, 7, 14),
                "created": new Date(),
                "amount": 83,
                "price": 1.2,
                "pricePerItem": 1.2,
                "usdPricePerItem": 1.2,
                "btcPricePerItem": 0.00028894,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "DNT",
                    "name": "District0x",
                    "id": "district0x"
                },
                "dateBought": new Date(2017, 7, 14),
                "created": new Date(),
                "amount": 598.5,
                "price": 0.1634,
                "pricePerItem": 0.1634,
                "usdPricePerItem": 0.1634,
                "btcPricePerItem": 0.00004044554,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "RISE",
                    "name": "Rise",
                    "id": "rise"
                },
                "dateBought": new Date(2017, 7, 14),
                "created": new Date(),
                "amount": 478,
                "price": 0.125563,
                "pricePerItem": 0.125563,
                "usdPricePerItem": 0.125563,
                "btcPricePerItem": 0.00002964,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "NEO",
                    "name": "NEO",
                    "id": "neo"
                },
                "dateBought": new Date(2017, 7, 17),
                "created": new Date(),
                "amount": 1,
                "price": 41.6,
                "pricePerItem": 41.6,
                "usdPricePerItem": 41.6,
                "btcPricePerItem": 0.00955000,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "sell"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "BTC",
                    "name": "Bitcoin",
                    "id": "bitcoin"
                },
                "dateBought": new Date(2017, 7, 20),
                "created": new Date(),
                "amount": 0.02,
                "price": 4158,
                "pricePerItem": 4158,
                "usdPricePerItem": 4158,
                "btcPricePerItem": 1,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "sell"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "FCT",
                    "name": "Factom",
                    "id": "factom"
                },
                "dateBought": new Date(2017, 7, 20),
                "created": new Date(),
                "amount": 3.52436815,
                "price": 23.25,
                "pricePerItem": 23.25,
                "usdPricePerItem": 23.25,
                "btcPricePerItem": 0.00557100,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "NEO",
                    "name": "NEO",
                    "id": "neo"
                },
                "dateBought": new Date(2017, 7, 20),
                "created": new Date(),
                "amount": 0.5,
                "price": 39,
                "pricePerItem": 39,
                "usdPricePerItem": 39,
                "btcPricePerItem": 0.00936000,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "XMR",
                    "name": "Monero",
                    "id": "monero"
                },
                "dateBought": new Date(2017, 7, 21),
                "created": new Date(),
                
                "amount": 0.91,
                "price": 82,
                "pricePerItem": 82,
                "usdPricePerItem": 82,
                "btcPricePerItem": 0.0208386277,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "sell"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "SC",
                    "name": "Siacoin",
                    "id": "siacoin"
                },
                "dateBought": new Date(2017, 7, 11),
                "created": new Date(),

                "amount": 10169.41393281,
                "price": 0.0090732697,
                "pricePerItem": 0.0090732697,
                "usdPricePerItem": 0.0090732697,
                "btcPricePerItem": 0.00000250,
                "currency": "USD",
                    "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "XRP",
                    "name": "Ripple",
                    "id": "ripple"
                },
                "dateBought": new Date(2017, 7, 23),
                "created": new Date(),

                "amount": 150,
                "price": 0.23983,
                "pricePerItem": 0.23983,
                "usdPricePerItem": 0.23983,
                "btcPricePerItem": 0.00005894077,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "sell"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "XRP",
                    "name": "Ripple",
                    "id": "ripple"
                },
                "dateBought": new Date(2017, 7, 23),
                "created": new Date(),

                "amount": 134,
                "price": 0.21802,
                "pricePerItem": 0.21802,
                "usdPricePerItem": 0.21802,
                "btcPricePerItem": 0.00005362496,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "MIOTA",
                    "name": "IOTA",
                    "id": "iota"
                },
                "dateBought": new Date(2017, 7, 23),
                "created": new Date(),

                "amount": 54,
                "price": 0.91500,
                "pricePerItem": 0.91500,
                "usdPricePerItem": 0.91500,
                "btcPricePerItem": 0.00022487097,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "VTC",
                    "name": "Vertcoin",
                    "id": "vertcoin"
                },
                "dateBought": new Date(2017, 7, 26),
                "created": new Date(),

                "amount": 40,
                "price": 0.00022600,
                "pricePerItem": 0.931798,
                "usdPricePerItem": 0.931798,
                "btcPricePerItem": 0.00022600,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "sell"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "OMG",
                    "name": "OmiseGo",
                    "id": "omisego"
                },
                "dateBought": new Date(2017, 7, 26),
                "created": new Date(),

                "amount": 3.8,
                "price": 8.184,
                "pricePerItem": 8.184,
                "usdPricePerItem": 8.184,
                "btcPricePerItem": 0.0019849624,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "TNT",
                    "name": "Tierion",
                    "id": "tierion"
                },
                "dateBought": new Date(2017, 6, 27),
                "created": new Date(),

                "amount": 709.60269138,
                "price": 8.184,
                "pricePerItem": 0.071,
                "usdPricePerItem": 0.071,
                "btcPricePerItem": 0.00002681268,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "NEO",
                    "name": "NEO",
                    "id": "neo"
                },
                "dateBought": new Date(2017, 7, 29),
                "created": new Date(),
                "amount": 2.5,
                "pricePerItem": 32.09,
                "usdPricePerItem": 32.09,
                "btcPricePerItem": 0.00518862,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "sell"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "ETH",
                    "name": "Ethereum",
                    "id": "ethereum"
                },
                "dateBought": new Date(2017, 7, 29),
                "created": new Date(),
                "amount": 0.24556080,
                "pricePerItem": 356.69,
                "usdPricePerItem": 356.69,
                "btcPricePerItem": 0.081243,
                "currency": "USD",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "NAV",
                    "name": "NAV Coin",
                    "id": "nav-coin"
                },
                "dateBought": new Date(2017, 8, 4),
                "created": new Date(),
                "amount": 64.25120773,
                "usdPricePerItem": 1.42,
                "btcPricePerItem": 0.00031050,
                "currency": "BTC",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "NAV",
                    "name": "NAV Coin",
                    "id": "nav-coin"
                },
                "dateBought": new Date(2017, 8, 4),
                "created": new Date(),
                "amount": 64.25120773,
                "usdPricePerItem": 1.2504672,
                "btcPricePerItem": 0.00028946,
                "currency": "BTC",
                "priceType": "per coin",
                "note": "",
                "action": "sell"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "RISE",
                    "name": "Rise",
                    "id": "rise"
                },
                "dateBought": new Date(2017, 8, 5),
                "created": new Date(),
                "amount": 240,
                "usdPricePerItem": 0.36,
                "btcPricePerItem": 0.00007810,
                "currency": "BTC",
                "priceType": "per coin",
                "note": "",
                "action": "sell"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "PAY",
                    "name": "TenX",
                    "id": "tenx"
                },
                "dateBought": new Date(2017, 8, 5),
                "created": new Date(),
                "amount": 13,
                "usdPricePerItem": 2.937816,
                "btcPricePerItem": 0.00068005,
                "currency": "BTC",
                "priceType": "per coin",
                "note": "",
                "action": "sell"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "LSK",
                    "name": "Lisk",
                    "id": "lisk"
                },
                "dateBought": new Date(2017, 8, 5),
                "created": new Date(),
                "amount": 10.68826345,
                "usdPricePerItem": 6.07,
                "btcPricePerItem": 0.00139990,
                "currency": "BTC",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            },
            {
                "user": UserId,
                "portfolio": UserPortfolioId,
                "coin": {
                    "symbol": "OMG",
                    "name": "OmiseGO",
                    "id": "omisego"
                },
                "dateBought": new Date(2017, 8, 5),
                "created": new Date(),
                "amount": 6.44935345,
                "usdPricePerItem": 10.06,
                "btcPricePerItem": 0.00232000,
                "currency": "BTC",
                "priceType": "per coin",
                "note": "",
                "action": "buy"
            }*/
        ];
        return new Promise((resolve, reject) => {
            for (let investment of investments) {
                let newInvestment = new Investment(investment);
                newInvestment.save();
            }

            return resolve('investments history seeded');
        })
    }
};

module.exports = Seed;