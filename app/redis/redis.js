    /**
 * Created by Виталий on 27.08.2017.
 */
//redis singleton
const redis = require('redis');
const port = process.env.REDIS_PORT || 6379;
const host = process.env.REDIS_HOST || 'localhost';
const url = process.env.REDIS_URL;
//module.exports = redis.createClient(port, host);
    module.exports = process.env.NODE_ENV !== 'production' ? redis.createClient(port, host) : redis.createClient(url);