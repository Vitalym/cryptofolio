/**
 * Created by decipher on 16.3.17.
 */
  'use strict';
//winston setup
const winston = require('winston');
const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      colorize: true,
      timestamp: tsFormat
    })
  ]
});

const MailListener = require("mail-listener2");

let mailListener = new MailListener({
  username: "metidateam@gmail.com",
  password: "vitmal1991",
  host: "imap.gmail.com",
  port: 993, // imap port
  tls: true,
  connTimeout: 10000, // Default by node-imap
  authTimeout: 5000, // Default by node-imap,
  debug: console.log, // Or your custom function with only one incoming argument. Default: null
  tlsOptions: { rejectUnauthorized: false },
  mailbox: "INBOX", // mailbox to monitor
  markSeen: true, // all fetched email willbe marked as seen and not fetched next time
  fetchUnreadOnStart: true // use it only if you want to get all unread email on lib start. Default is `false`,
  //mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
  //attachments: true, // download attachments as they are encountered to the project directory
  //attachmentOptions: { directory: "attachments/" } // specify a download directory for attachments
});

mailListener.on("server:connected", function(){
  logger.debug("imapConnected");
});

mailListener.on("server:disconnected", function(){
  logger.warn("imapDisconnected");
  mailListener.start();
});

mailListener.on("error", function(err){
  logger.error(err);
});

mailListener.on("mail", function(mail, seqno, attributes){
  // do something with mail object including attachments
  console.log("emailParsed");
  console.log(mail.subject);
  
  // mail processing code goes here
});

mailListener.on("attachment", function(attachment){
  console.log(attachment.path);
});

module.exports = {
  start: () => {
    mailListener.start(); // start listening
  }
};



// stop listening
//mailListener.stop();


// it's possible to access imap object from node-imap library for performing additional actions. E.x.
//mailListener.imap.move(:msguids, :mailboxes, function(){})
