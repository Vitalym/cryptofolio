/**
 * Created by Виталий on 18.03.2017.
 */
'use strict';
//winston setup
const winston = require('winston');
const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      colorize: true,
      timestamp: tsFormat
    })
  ]
});

const MailListener = require("mail-listener2");
const mongoose = require('mongoose'),
  async = require('async'),
  Listener = mongoose.model('MailListener'),
  Entry = mongoose.model('Entry');

module.exports = {
  createNewMailListener: (username, password, host, team, group) => {
    return new Promise((resolve, reject) => {

      if(!username){
        //integration is not email, we just return empty object. BICYCLES EVERYWHERE!!!11
        resolve('done');
      } else {
        //save listener object to db
        let listener = new Listener();
        listener.username = username;
        listener.password = password;
        listener.host = host;
        listener._team = team;
        listener.userGroup = group;

        listener.save().then(response => {
          logger.debug('listener saved successfully');
          module.exports.restartListeners();
          resolve('done');
        }).catch(error => {
          logger.error(error);
          reject(error);
        })
      }
    });
  },

  startListeners: () => {
    console.log('starting listeners...');
    Listener.find({}).exec().then(listeners => {
      async.each(listeners, (listener, callback) => {
        console.log(listener);
        let mailListener = new MailListener({
          username: listener.username,
          password: listener.password,
          host: listener.host,
          port: 993, // imap port
          tls: true,
          connTimeout: 10000, // Default by node-imap
          authTimeout: 5000, // Default by node-imap,
          debug: console.log, // Or your custom function with only one incoming argument. Default: null
          tlsOptions: {rejectUnauthorized: false},
          mailbox: "INBOX", // mailbox to monitor
          markSeen: true, // all fetched email willbe marked as seen and not fetched next time
          fetchUnreadOnStart: true // use it only if you want to get all unread email on lib start. Default is `false`,
          //mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
          //attachments: true, // download attachments as they are encountered to the project directory
          //attachmentOptions: { directory: "attachments/" } // specify a download directory for attachments
        });

        mailListener.on("server:connected", () => {
          logger.debug("imapConnected");
        });

        mailListener.on("server:disconnected", () => {
          logger.warn("imapDisconnected");
          mailListener.start();
        });

        mailListener.on("error", (err) => {
          logger.error(err);
          //mailListener.start();
        });

        mailListener.on("mail", (mail, seqno, attributes) => {
          // do something with mail object including attachments
          console.log("emailParsed");
          console.log(mail.subject);
          console.log(mail.from);
          console.log(listener.userGroup);
          // mail processing code goes here

          let newEntry = new Entry();
          newEntry.subject = mail.subject || 'new submission from ' + mail.from;
          //newEntry.author = 'Metida' // TODO create author for  usergroup or use author who created integration
          newEntry._team = listener._team;
          newEntry._userGroup = listener.userGroup;

        });

        mailListener.on("attachment", (attachment) => {
          console.log(attachment.path);
        });
        console.log('almost starting...');
        mailListener.start();
        callback();
      }, (error) => {
        if (error) {
          logger.error(error);
        }
        logger.debug('started');
      });
    }).catch(error => {
      logger.error(error);
    });
  },

  restartListeners: () => {
    module.exports.startListeners();
  }
};
