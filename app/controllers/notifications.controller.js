'use strict';
const express = require('express'),
    mongoose = require('mongoose'),
    async = require('async'),
    redis = require('../redis/redis'),
    User = mongoose.model('User'),
    UserNotification = mongoose.model('UserNotification'),
    Holding = mongoose.model('Holding'),
    Currency = mongoose.model('Currency'),
    helpers = require('../helpers/methods'),
    constants = require('../helpers/constants'),
    errorFactory = require('../helpers/errorFactory'),
    errorHandler = require('../helpers/errorHandler');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-investments.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

module.exports = {

    //get all transactions with portfolio value
    getNotifications: (req, res, next) => {
        let hrstart = process.hrtime();
        UserNotification.find({user: req.user._id})
            .populate(constants.NOTIFICATION_POPULATION)
            .lean() //allow further data manipulation
            .exec()
            .then(notifications => {
                let hrend = process.hrtime(hrstart);
                logger.info("Intermediate execution time 'getInvestments' (hr): %ds %dms", hrend[0], hrend[1] / 1000000);
                return res.status(200).json({
                    notifications: notifications,
                    token: req.token
                });
            })
            .catch(err => {
                return next(err)
            });
    }
};