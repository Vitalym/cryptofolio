/**
 * Created by Виталий on 28.08.2017.
 */
'use strict';
const express = require('express'),
    mongoose = require('mongoose'),
    async = require('async'),
    config = require('../../config/config'),
    jwt = require('jsonwebtoken'),
    bcrypt = require('bcryptjs'),
    SALT_WORK_FACTOR = 10,
    User = mongoose.model('User'),
    Investment = mongoose.model('Investment'),
    Currency = mongoose.model('Currency'),
    helpers = require('../helpers/methods'),
    constants = require('../helpers/constants'),
    investmentsHelpers = require('../helpers/investments'),
    mailer = require('../helpers/mailer'),
    errorFactory = require('../helpers/errorFactory'),
    errorHandler = require('../helpers/errorHandler');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-users.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

module.exports = {
    updateCurrencySettings: (req, res, next) => {
        if (!req.body.currency) {
            let error = errorFactory.missingParam(`Currency is required`);
            return next(error);
        }

        const validCurrencies = ['USD', 'BTC'];

        if (!validCurrencies.includes(req.body.currency)) {
            let error = errorFactory.invalidField(`Unsupported currency`);
            return next(error);
        }
        const update = {
            $set: {
                'settings.currency': req.body.currency
            }
        };
        User.findByIdAndUpdate(req.user._id, update, {upsert: true, new: true})
            .populate(constants.USER_POPULATION)
            .exec()
            .then(user => {
                if (!user) {
                    let error = errorFactory.notFound();
                    return next(error);
                }
                let token = jwt.sign({
                    data: user,
                    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                }, config.secret);
                logger.info(`${user.email ? user.email : user.facebook} changed his currency preferences`);
                return res.status(200).json({
                    success: true,
                    message: 'Settings updated',
                    token: token
                });
            })
            .catch(err => {
                return next(err)
            });
    },

    updateZeroBalanceSettings: (req, res, next) => {
        console.log(req.body);

        const update = {
            $set: {
                'settings.hideZeroBalances': req.body.value
            }
        };
        User.findByIdAndUpdate(req.user._id, update, {upsert: true, new: true})
            .populate(constants.USER_POPULATION)
            .exec()
            .then(user => {
                if (!user) {
                    let error = errorFactory.notFound();
                    return next(error);
                }
                let token = jwt.sign({
                    data: user,
                    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                }, config.secret);
                logger.info(`${user.email ? user.email : user.facebook} changed his currency preferences`);
                return res.status(200).json({
                    success: true,
                    message: 'Settings updated',
                    token: token
                });
            })
            .catch(err => {
                return next(err)
            });
    },

    updateUserEmail: (req, res, next) => {
        if (!req.body.email) {
            let error = errorFactory.missingParam(`Email is required`);
            return next(error);
        }

        const update = {
            $set: {
                'email': req.body.email
            }
        };
        User.findByIdAndUpdate(req.user._id, update, {upsert: true, new: true})
            .populate(constants.USER_POPULATION)
            .exec()
            .then(user => {
                if (!user) {
                    let error = errorFactory.notFound();
                    return next(error);
                }
                let token = jwt.sign({
                    data: user,
                    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                }, config.secret);
                logger.info(`${user.email ? user.email : user.facebook} changed his currency preferences`);
                return res.status(200).json({
                    success: true,
                    message: 'Settings updated',
                    token: token
                });
            })
            .catch(err => {
                return next(err)
            });
    },

    updateUserPassword: (req, res, next) => {
        if (!req.body.password) {
            let error = errorFactory.missingParam(`Password is required`);
            return next(error);
        }

        hashPassword(req.body.password)
            .then(hash => {
                console.log(hash);
                const update = {
                    $set: {
                        'password': hash
                    }
                };
                return User.findByIdAndUpdate(req.user._id, update, {upsert: true, new: true})
                    .populate(constants.USER_POPULATION)
                    .exec();
            })
            .then(user => {
                if (!user) {
                    let error = errorFactory.notFound();
                    return next(error);
                }
                let token = jwt.sign({
                    data: user,
                    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                }, config.secret);
                logger.info(`${user.email ? user.email : user.facebook} changed his currency preferences`);
                return res.status(200).json({
                    success: true,
                    message: 'Settings updated',
                    token: token
                });
            })
            .catch(err => {
                return next(err)
            });
    }
};

function hashPassword(password) {
    return new Promise((resolve, reject) => {
        // generate a salt
        bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
            if (err) return reject(err);

            // hash the password along with our new salt
            bcrypt.hash(password, salt, (err, hash) => {
                if (err) return reject(err);

                return resolve(hash);
            });
        });
    });
}