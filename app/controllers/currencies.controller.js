/**
 * Created by Виталий on 01.08.2017.
 */
'use strict';
const express = require('express'),
    mongoose = require('mongoose'),
    async = require('async'),
    jsonfile = require('jsonfile'),
    User = mongoose.model('User'),
    redis = require('../redis/redis'),
    Currency = mongoose.model('Currency'),
    helpers = require('../helpers/methods'),
    constants = require('../helpers/constants'),
//mailer = require('../helpers/mailer'),
    errorFactory = require('../helpers/errorFactory'),
    errorHandler = require('../helpers/errorHandler');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-investments.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

module.exports = {
    getCurrencyNames: (req, res, next) => {
        let coins;
        redis.get('coinNames', (err, data) => {
            if (err) {
                return next(err);
            }

            coins = data;
            redis.get('coinTimestamp', (err, timestamp) => {
                if (err) {
                    return next(err);
                }

                return res.status(200).json({
                    coins: coins,
                    timestamp: timestamp,
                    token: req.token
                });
            });
        });
    },

    getCurrencies: (req, res, next) => {
        const skip = parseInt(req.query.skip) || 0;
        const limit = parseInt(req.query.limit) || 50;
        redis.get(`coinData-${skip}`, (err, data) => {
            if (err) {
                return next(err);
            }

            return res.status(200).json({
                coinData: data,
                token: req.token
            });
        });
        /*  Currency.find({})
         //.sort('-market_cap_usd')
         .limit(limit)
         .skip(skip)
         .exec()
         .then(currencies => {
         return res.status(200).json({
         coinData: currencies,
         token: req.token
         });
         })
         .catch(err => {
         return next(err)
         });*/
    },

    searchCurrencies: (req, res, next) => {
        //let criteria = req.body.query
        /*Currency.find({"$or": [ {
            "id" : { $search: criteria }}, { "name" : { $search: criteria }}, { "symbol" : { $search: criteria }}]});*/
        Currency.find({$text: {$search: req.body.query, $caseSensitive: false}})
            .exec()
            .then(result => {
                return res.status(200).json({
                    result: result,
                    token: req.token
                });
            });
    }
};

function transformCurrencyNames(coin, callback) {
    process.nextTick(() => {
        let newCoin = {
            value: {
                id: coin.id,
                symbol: coin.symbol,
                name: coin.name
            },

            key: coin.id,
            label: `${coin.name} (${coin.symbol})`
        };
        return callback(null, newCoin);
    })

}