/**
 * Created by dev on 1.8.17.
 */
'use strict';
const express = require('express'),
    mongoose = require('mongoose'),
    async = require('async'),
    jwt = require('jsonwebtoken'),
    config = require('../../config/config'),
    User = mongoose.model('User'),
    Investment = mongoose.model('Investment'),
    Currency = mongoose.model('Currency'),
    Portfolio = mongoose.model('Portfolio'),
    helpers = require('../helpers/methods'),
    constants = require('../helpers/constants'),
    investmentsHelpers = require('../helpers/investments'),
    mailer = require('../helpers/mailer'),
    errorFactory = require('../helpers/errorFactory'),
    errorHandler = require('../helpers/errorHandler');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-investments.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

module.exports = {
    //get holdings with rebalancing information
    getHoldingsAllocation: (req, res, next) => {
        Investment.find({user: req.user._id})
            .populate(constants.INVESTMENT_POPULATION)
            .lean() //allow further data manipulation
            .exec()
            .then(investments => {
                req.investments = investments;
                return next();
            })
            .catch(err => {
                return next(err)
            });
    },

    //create new allocation schema
    setAssetAllocation: (req, res, next) => {
        const update = {
            $set: {
                targetAllocation: req.body.allocation
            }
        };

        return Portfolio.findByIdAndUpdate(req.user.currentPortfolio._id, update, {new: true, upsert: true})
            .exec()
            .then(portfolio => {
                //update user
                return User.findById(req.user._id)
                    .populate(constants.USER_POPULATION)
                    .exec()
            })
            .then(user => {
                req.token = jwt.sign({
                    data: user,
                    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                }, config.secret);
                logger.info(`${user.email} updated portfolio allocation`);
                return Investment.find({user: req.user._id})
                    .populate(constants.INVESTMENT_POPULATION)
                    .lean() //allow further data manipulation
                    .exec()
            })
            .then(investments => {
                req.investments = investments;
                return next();
            })
            .catch(err => {
                return next(err)
            });
    },
};