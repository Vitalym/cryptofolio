/**
 * Created by dev on 1.8.17.
 */
'use strict';
const express = require('express'),
    mongoose = require('mongoose'),
    async = require('async'),
    redis = require('../redis/redis'),
    User = mongoose.model('User'),
    Investment = mongoose.model('Investment'),
    Holding = mongoose.model('Holding'),
    Currency = mongoose.model('Currency'),
    helpers = require('../helpers/methods'),
    constants = require('../helpers/constants'),
    investmentsHelpers = require('../helpers/investments'),
    mailer = require('../helpers/mailer'),
    errorFactory = require('../helpers/errorFactory'),
    errorHandler = require('../helpers/errorHandler');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-investments.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

module.exports = {

    //get all transactions with portfolio value
    getInvestments: (req, res, next) => {
        let hrstart = process.hrtime();
        Investment.find({user: req.user._id})
            .populate(constants.INVESTMENT_POPULATION)
            .lean() //allow further data manipulation
            .exec()
            .then(investments => {
                req.investments = investments;
                let hrend = process.hrtime(hrstart);
                logger.info("Intermediate execution time 'getInvestments' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                return next();
            })
            .catch(err => {
                return next(err)
            });
    },

    //get single transaction by id
    getInvestment: (req, res, next) => {
        Investment.findOne({
            $and: [
                {user: req.user._id},
                {_id: req.params.id}
            ]
        })
            .populate(constants.INVESTMENT_POPULATION)
            .exec()
            .then(investment => {
                return res.status(200).json({
                    investment: investment,
                    token: req.token
                });
            })
            .catch(err => {
                return next(err)
            });
    },

    //get transactions by coin
    getInvestmentsByCoin: (req, res, next) => {
        //todo check that param is string
        let hrstart = process.hrtime();

        if(!req.params.id || typeof req.params.id !== 'string'){
            let error = errorFactory.notFound();
            return next(error);
        }

        Investment.find({
            $and: [
                {user: req.user._id},
                {'coin.id': req.params.id}
            ]
        })
            .populate(constants.INVESTMENT_POPULATION)
            .lean() //allow further data manipulation
            .exec()
            .then(transactions => {
                //todo calculate total, pass to middleware
                req.hrstart = hrstart;
                let hrend = process.hrtime(hrstart);
                logger.info("Intermediate execution time 'getInvestmentsByCoin' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                req.transactions = transactions;
                return next();
            })
            .catch(err => {
                return next(err)
            });
    },

    getHistoryDataByCoin: (req, res, next) => {
        if(!req.params.id || typeof req.params.id !== 'string'){
            let error = errorFactory.notFound(`Error getting price history data :(`);
            return next(error);
        }
        let hrstart = process.hrtime();
        return investmentsHelpers.getCoinHistory(req.params.id)
            .then(response => {
                if(!response){
                    let error = errorFactory.notFound(`Error getting price history data :(`);
                    return next(error);
                }
                req.hrstart = hrstart;
                let history = {
                    price:  response.price,
                    market_cap: response.market_cap
                };
                let hrend = process.hrtime(hrstart);
                logger.info("Total execution time 'getHistoryDataByCoin' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                return res.status(200).json({
                    //sorted: sorted,
                    history: history,
                    token: req.token
                });
            })
            .catch(err => {
                return next(err)
            });
    },

    addInvestment: (req, res, next) => {

        //TODO add validation here
        let savedInvestment;
        let newInvestment = new Investment();

        newInvestment.coin = {
            id: req.body.normalizedCoin.id,
            name: req.body.normalizedCoin.name,
            symbol: req.body.normalizedCoin.symbol
        };
        newInvestment.dateBought = req.body.dateBought || new Date();
        newInvestment.priceType = req.body.priceType;
        newInvestment.btcPricePerItem = req.btcPricePerItem;
        newInvestment.usdPricePerItem = req.usdPricePerItem;
        newInvestment.usdAcquisitionCost = req.body.amount * parseFloat(req.usdPricePerItem);
        newInvestment.btcAcquisitionCost = req.body.amount * parseFloat(req.btcPricePerItem);
        newInvestment.note = req.body.note;
        newInvestment.amount = req.body.amount;
        newInvestment.action = req.body.action || 'buy';
        newInvestment.user = req.user._id;
        newInvestment.currency = req.body.currency;
        newInvestment.portfolio = req.user.currentPortfolio;
        if(req.connectedTransaction){
            newInvestment.connectedTransaction = req.connectedTransaction;
        }

        newInvestment.save()
            .then(investment => {
                savedInvestment = investment;
                logger.info(`New investment ${req.user.email} added`);

                redis.del(`investments-${req.user._id}`);

                return res.status(200).json({
                    message: 'New transaction saved',
                    investment: savedInvestment,
                    token: req.token
                });
            })
            .catch(err => {
                return next(err)
            });
    },

    editInvestment: (req, res, next) => {
        let update = {
            $set: {
                coin: req.body.normalizedCoin,
                dateBought: req.body.dateBought,
                priceType: req.body.priceType,
                usdPricePerItem: req.usdPricePerItem,
                btcPricePerItem: req.btcPricePerItem,
                usdAcquisitionCost: req.body.amount * parseFloat(req.usdPricePerItem),
                btcAcquisitionCost: req.body.amount * parseFloat(req.btcPricePerItem),
                currency: req.body.currency,
                amount: req.body.amount,
                action: req.body.action,
                note: req.body.note
            }
        };

        if(req.editAction === 'create'){
            //add connected transaction
            update.$set.connectedTransaction = req.connectedTransaction;
        } else if (req.editAction === 'delete'){
            update.$set.connectedTransaction = null;
        }

        Investment.findOneAndUpdate({
            $and: [
                {user: req.user._id},
                {_id: req.params.id}
            ]
        }, update, {new: true, upsert: true})
            .populate(constants.INVESTMENT_POPULATION)
            .exec()
            .then(investment => {
                redis.del(`investments-${req.user._id}`); //clear redis cache
                return res.status(200).json({
                    message: 'Transaction updated',
                    investment: investment,
                    token: req.token
                });
            })
            .catch(err => {
                return next(err)
            });

    },

    removeInvestment: (req, res, next) => {
        Investment.remove({
            $and: [
                {_id: req.params.id},
                {user: req.user._id}
            ]
        })
            .exec()
            .then(investment => {
                return res.status(200).json({
                    message: 'Transaction removed',
                    investment: investment,
                    token: req.token
                });
            })
            .catch(err => {
                return next(err)
            });
    }
};