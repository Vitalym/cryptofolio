/**
 * Created by Виталий on 20.02.2017.
 */
'use strict';
const express = require('express'),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,
    jwt = require('jsonwebtoken'),
    async = require('async'),
    request = require('request'),
    qs = require('querystring'),
    config = require('../../config/config'),
    User = mongoose.model('User'),
    Portfolio = mongoose.model('Portfolio'),
    helpers = require('../helpers/methods'),
    constants = require('../helpers/constants'),
    mailer = require('../helpers/mailer'),
    errorFactory = require('../helpers/errorFactory'),
    errorHandler = require('../helpers/errorHandler');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-auth-results.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

module.exports = {
    //user signup
    signup: (req, res, next) => {
        //check for empty fields
        console.log(req.body);
        if (!req.body.password) {
            let error = errorFactory.missingParam();
            return next(error);
        }

        const UserPortfolioId = ObjectId();

        let newUser = new User();
        const verificationToken = helpers.generateVerificationToken();

        newUser.displayName = req.body.displayName || 'user';
        newUser.currentPortfolio = UserPortfolioId;
        newUser.image = '';
        newUser.email = req.body.email;
        newUser.password = req.body.password;
        newUser.isVerified = false;
        newUser.verificationToken = verificationToken;

        //store user value
        let userResult;

        return newUser.save()
            .then(user => {
                userResult = user;
                let userPortfolio = new Portfolio();
                userPortfolio._id = UserPortfolioId;
                userPortfolio.user = user._id;
                return userPortfolio.save();
            })
            .then(portfolio => {
                logger.info(`New user: ${userResult.username} ${userResult.email}`);
                /*   return mailer.sendVerificationEmail({
                 email: req.body.email,
                 firstName: req.body.username,
                 token: verificationToken
                 })
                 })
                 .then(info => {
                 logger.debug(info);*/
                let token = jwt.sign({
                    data: userResult,
                    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                }, config.secret);
                return res.status(200).json({
                    success: true,
                    message: 'Registration successful',
                    token: token
                });
            })
            .catch(err => {
                return next(err);
            });
    },

    login: (req, res, next) => {

        if (!req.body.email || !req.body.password) {
            let error = errorFactory.authFailed();
            return next(error);
        }

        User.findOne({email: req.body.email})
            .populate(constants.USER_POPULATION)
            .exec()
            .then(user => {
                if (!user) {
                    let error = errorFactory.authFailed();
                    return next(error);
                } else if (user) {
                    if (!user.password) { //user is not yet verified
                        let error = errorFactory.authFailed();
                        return next(error);
                    }
                    user.comparePassword(req.body.password, (err, isMatch) => {
                        if (err) return next(err);
                        if (!isMatch) {
                            let error = errorFactory.authFailed();
                            return next(error);
                        } else {
                            let token = jwt.sign({
                                data: user,
                                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                            }, config.secret);
                            logger.info(`${user.email} logged in`);
                            return res.status(200).json({
                                success: true,
                                message: 'Authentication successful',
                                token: token
                            });
                        }
                    });
                }
            })
            .catch(err => {
                return next(err);
            });
    },

    loginViaFacebook: (req, res, next) => {
        //first we check if there is an email with the profile
        if(req.body.email){
            User.findOne({email: req.body.email})
                .exec()
                .then(existingUser => {
                    //if user exists, we check if he already has facebook integration
                    if (existingUser) {
                        console.log('existing user');
                        if(existingUser.facebook && existingUser.facebook === req.body.id){
                            let token = jwt.sign({
                                data: existingUser,
                                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                            }, config.secret);
                            logger.info(`${existingUser.email ? existingUser.email : existingUser.facebook} logged in via facebook`);
                            return res.status(200).json({
                                success: true,
                                message: 'Logged in successfully',
                                token: token
                            });
                        } else {
                            const update = {
                                $set: {
                                    facebook: req.body.id
                                }
                            };
                            User.findByIdAndUpdate(existingUser._id, update, {new: true, upsert: true})
                                .exec()
                                .then(user => {
                                    let token = jwt.sign({
                                        data: user,
                                        exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                                    }, config.secret);
                                    logger.info(`${user.email ? user.email : user.facebook} logged in via facebook`);
                                    return res.status(200).json({
                                        success: true,
                                        message: 'Logged in successfully',
                                        token: token
                                    });
                                })
                                .catch(err => {
                                    return next(err);
                                });
                        }
                    } else {
                        let userResult;
                        const UserPortfolioId = ObjectId();

                        let user = new User();
                        user.email = req.body.email ? req.body.email : '';
                        user.facebook = req.body.id;
                        user.currentPortfolio = UserPortfolioId;
                        user.picture = 'https://graph.facebook.com/' + req.body.id + '/picture?type=large';
                        user.displayName = req.body.name;
                        user.save()
                            .then(user => {
                                userResult = user;
                                let userPortfolio = new Portfolio();
                                userPortfolio._id = UserPortfolioId;
                                userPortfolio.user = userResult._id;
                                return userPortfolio.save();
                            })
                            .then(portfolio => {
                                let token = jwt.sign({
                                    data: userResult,
                                    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                                }, config.secret);
                                logger.info(`${userResult.email ? userResult.email : userResult.facebook} signed up via facebook`);
                                return res.status(200).json({
                                    success: true,
                                    message: 'Authentication successful',
                                    token: token
                                });
                            })
                            .catch(err => {
                                return next(err);
                            });
                    }
                })
                .catch(err => {
                    return next(err);
                });
        } else {
            //if there is no email, we check user existance by facebook id
            User.findOne({facebook: req.body.id})
                .exec()
                .then(existingUser => {
                    if (existingUser) {
                        let token = jwt.sign({
                            data: existingUser,
                            exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                        }, config.secret);
                        logger.info(`${existingUser.email ? existingUser.email : existingUser.facebook} logged in via facebook`);
                        return res.status(200).json({
                            success: true,
                            message: 'Logged in successfully',
                            token: token
                        });
                    }
                    let userResult;
                    const UserPortfolioId = ObjectId();

                    let user = new User();
                    user.email = req.body.email ? req.body.email : '';
                    user.facebook = req.body.id;
                    user.currentPortfolio = UserPortfolioId;
                    user.picture = 'https://graph.facebook.com/' + req.body.id + '/picture?type=large';
                    user.displayName = req.body.name;
                    user.save()
                        .then(user => {
                            user = userResult;
                            let userPortfolio = new Portfolio();
                            userPortfolio._id = UserPortfolioId;
                            userPortfolio.user = user._id;
                            return userPortfolio.save();
                        })
                        .then(portfolio => {
                            let token = jwt.sign({
                                data: userResult,
                                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 30)
                            }, config.secret);
                            logger.info(`${userResult.email ? userResult.email : userResult.facebook} signed up via facebook`);
                            return res.status(200).json({
                                success: true,
                                message: 'Authentication successful',
                                token: token
                            });
                        })
                        .catch(err => {
                            return next(err);
                        });
                })
                .catch(err => {
                    return next(err);
                });
        }
    },

    verify: (req, res, next) => {

        console.log(req.body);

        if (!req.body.verificationToken) {
            let error = errorFactory.invalidToken();
            return next(error);
        }

        User.findOne({verificationToken: req.body.verificationToken})
            .populate(constants.USER_POPULATION)
            .exec()
            .then(user => {
                if (!user) {
                    let error = errorFactory.invalidToken();
                    return next(error);
                }

                //if user is already verified, we just send it
                if (user.isVerified) {

                    return res.status(200).send({
                        success: true,
                        message: 'Verification successful',
                        email: user.email
                    });
                } else {
                    //otherwise, we save it as verified
                    user.isVerified = true;

                    let userPromise = user.save();

                    userPromise.then(user => {

                        return res.status(200).send({
                            success: true,
                            message: 'Verification successful',
                            email: user.email
                        });

                    }).catch(err => {
                        return next(err);
                    });
                }
            }).catch(err => {
            return next(err);
        });
    },

    requestResetPassword: (req, res, next) => {
        console.log(req.body.email)
        if (!req.body.email) {
            let error = errorFactory.missingParam();
            return next(error);
        }

        let userResult, resetToken;

        User.findOne({email: req.body.email})
            .exec()
            .then(user => {
                if (!user) {
                    let error = errorFactory.notFound(`Email ${req.body.email} was not found`);
                    return next(error);
                }

                if(!user.password){
                    //means he is probably logged in with facebook
                    let error = errorFactory.notFound(`Seems that you logged in to this account via Facebook`);
                    return next(error);
                }

                resetToken = helpers.generateVerificationToken();

                user.resetToken = resetToken;

                return user.save();
            })
            .then(user => {
                userResult = user;
                return mailer.sendResetEmail({
                    email: req.body.email,
                    token: resetToken
                })
            })
            .then(info => {
                logger.debug(info);
                return res.status(200).json({
                    success: true,
                    message: 'Email sent successfully',
                    user: userResult
                });
            }).catch(err => {
            return next(err);
        });
    },

    verifyResetToken: (req, res, next) => {

        console.log(req.body.resetToken)

        if (!req.body.resetToken) {
            let error = errorFactory.invalidToken('There is no reset token in the url. Please click or copy the link we emailed you to reset your password');
            return next(error);
        }

        User.findOne({resetToken: req.body.resetToken})
            .populate(constants.USER_POPULATION)
            .exec()
            .then(user => {
                if (!user) {
                    let error = errorFactory.invalidToken('There is no reset token in the url. Please click or copy the link we emailed you to reset your password');
                    return next(error);
                }

                //if reset token is already verified, we just send it
                if (user.isAllowedToReset) {

                    return res.status(200).send({
                        success: true,
                        message: 'You can reset password now',
                        user: user
                    });
                } else {
                    //otherwise, we save it as allowed to reset
                    user.isAllowedToReset = true;
                    user.password = null;

                    let userPromise = user.save();

                    userPromise.then(user => {

                        return res.status(200).send({
                            success: true,
                            message: 'You can reset password now',
                            user: user
                        });
                    }).catch(err => {
                        return next(err);
                    });
                }
            }).catch(err => {
            return next(err);
        });
    },

    resetPassword: (req, res, next) => {
        if (!req.body.password) {
            let error = errorFactory.missingParam();
            return next(error);
        }

        if (!req.body.resetToken) {
            let error = errorFactory.missingParam('Reset token is not found. Please use the link we emailed you to reset a password');
            return next(error);
        }

        let userResult;

        User.findOne({resetToken: req.body.resetToken})
            .populate(constants.USER_POPULATION)
            .exec()
            .then(user => {
                if (!user) {
                    let error = errorFactory.invalidToken(`Invalid reset token`);
                    return next(error);
                }

                if (!user.isAllowedToReset) {
                    let error = errorFactory.notAllowed(`Your reset token is not verified. Please use the link we emailed you to reset a password`);
                    return next(error);
                }

                user.password = req.body.password;
                user.resetToken = null; //clear reset token

                return user.save();
            })
            .then(user => {
                userResult = user;
                return mailer.sendPasswordResetEmail({
                    email: userResult.email
                })
            })
            .then(info => {
                logger.debug(info);
                return res.status(200).json({
                    success: true,
                    message: 'Password changed successfully',
                    user: userResult
                });
            }).catch(err => {
            return next(err);
        });
    },

    /* 
    

     changeEmail: (req, res, next) => {
     if (!req.body.firstName || !req.body.lastName || !req.body.oldEmail || !req.body.newEmail) {
     let error = errorFactory.missingParam();
     return next(error);
     }
     let verificationToken, userResult;

     User.findOne({email: req.body.oldEmail})
     .populate(constants.USER_POPULATION)
     .exec().then(user => {
     //console.log('1st');
     if (!user) {
     let error = errorFactory.notFound(`Email you're trying to change was not found`);
     return next(error);
     }

     verificationToken = helpers.generateVerificationToken();

     user.email = req.body.newEmail;
     user.verificationToken = verificationToken;
     return user.save();
     })
     .then(user => {
     //console.log('2nd');
     userResult = user;
     return mailer.sendVerificationEmail({
     email: req.body.newEmail,
     firstName: req.body.firstName,
     lastName: req.body.lastName,
     token: verificationToken
     })
     })
     .then(info => {
     //console.log('3rd');
     logger.debug(info);
     return res.status(200).json({
     success: true,
     message: 'Email changed',
     user: userResult
     });

     }).catch(err => {
     return next(err);
     });
     }*/
}
;
