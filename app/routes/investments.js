/**
 * Created by dev on 1.8.17.
 */
'use strict';
const express = require('express'),
    validateUnique = require('../middleware/validateUnique'),
    investmentController = require('../controllers/investment.controller'),
    redisCache = require('../middleware/redisCache'),
    investmentsHandler = require('../middleware/investmentHandler'),
    authHandler = require('../middleware/authHandler');

module.exports = function (app) {

    //get all user investments
    app.get('/api/investments', authHandler.ensureAuthenticated, redisCache.investmentsCache, investmentController.getInvestments, investmentsHandler.populateInvestment);

    //get transaction by id
    app.get('/api/investments/:id', authHandler.ensureAuthenticated, investmentController.getInvestment);

    //get transactions by coin
    app.get('/api/investments/coin/:id', authHandler.ensureAuthenticated, investmentController.getInvestmentsByCoin, investmentsHandler.populateCoinTransactions);

    //get history data by coin
    app.get('/api/investments/coin/history/:id', authHandler.ensureAuthenticated, investmentController.getHistoryDataByCoin);

    //add investment
    app.post('/api/investments',
        authHandler.ensureAuthenticated,
        investmentsHandler.checkSellAmount,
        investmentsHandler.getBTCPrice,
        investmentsHandler.calculateTransactionPrices,
        investmentsHandler.recalculateBTCHoldings,
        investmentController.addInvestment);
    //TODO  add investmentsHandler.recalculateBTCHoldings,

    //edit investment
    app.put('/api/investments/:id',
        authHandler.ensureAuthenticated,
        investmentsHandler.checkSellAmount,
        investmentsHandler.getBTCPrice,
        investmentsHandler.calculateTransactionPrices,
        investmentsHandler.getOriginalTransaction,
        investmentsHandler.editConnectedTransaction,
        investmentController.editInvestment);

    //sell investment
    app.put('/api/investments/sell', authHandler.ensureAuthenticated, investmentsHandler.checkSellAmount, investmentController.addInvestment);

    //delete investment
    app.delete('/api/investments/:id', authHandler.ensureAuthenticated, investmentController.removeInvestment);

};