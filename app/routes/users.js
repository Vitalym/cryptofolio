/**
 * Created by Виталий on 29.08.2017.
 */
'use strict';
const express = require('express'),
    validateUnique = require('../middleware/validateUnique'),
    usersController = require('../controllers/users.controller'),
    investmentsHandler = require('../middleware/investmentHandler'),
    authHandler = require('../middleware/authHandler');

module.exports = function (app) {

    //update user currency settings
   app.put('/api/user/settings/currency', authHandler.ensureAuthenticated, usersController.updateCurrencySettings);

    //update user "display zero balance settings"
    app.put('/api/user/settings/zerobalance', authHandler.ensureAuthenticated, usersController.updateZeroBalanceSettings);

    //update user email
   app.put('/api/user/email', authHandler.ensureAuthenticated, usersController.updateUserEmail);

    //update user password
    app.put('/api/user/password', authHandler.ensureAuthenticated, usersController.updateUserPassword);

    //update user settings
    //app.put('/api/user/settings', authHandler.ensureAuthenticated, usersController.updateUserSettings);

};