/**
 * Created by decipher on 10.1.17.
 */
'use strict';
const express = require('express'),
    validateUnique = require('../middleware/validateUnique'),
    authController = require('../controllers/auth.controller'),
    authHandler = require('../middleware/authHandler');

module.exports = function (app) {
    //register user
    app.post('/auth/signup', validateUnique.userEmail, authController.signup);

    //user login
    app.post('/auth/login', authController.login);

    //facebook login
    app.post('/auth/facebook', authController.loginViaFacebook);

    //new user - verify email address
    app.post('/auth/verify', authController.verify);

    //resend verification email
    //app.post('/auth/resend', authController.resend);

    //change email for verification links. applies only if user is in localstorage after login or signup
    //app.post('/auth/changeEmail', authController.changeEmail);

    //request reset password
     app.post('/auth/reset', authController.requestResetPassword);

    //verify reset token
    app.post('/auth/verifyResetToken', authController.verifyResetToken);

    //change password
    app.post('/auth/updatePassword', authController.resetPassword);
};
