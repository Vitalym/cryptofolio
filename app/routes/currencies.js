/**
 * Created by Виталий on 01.08.2017.
 */
'use strict';
const express = require('express'),
    validateUnique = require('../middleware/validateUnique'),
    currenciessController = require('../controllers/currencies.controller'),
    authHandler = require('../middleware/authHandler');

module.exports = function (app) {
    //get currency names
    app.get('/api/currency/names', authHandler.ensureAuthenticated, currenciessController.getCurrencyNames);

    //get currency data for the dashboard
    app.get('/api/currency', authHandler.ensureAuthenticated, currenciessController.getCurrencies);

    //text search currencies
    app.post('/api/currency/search', authHandler.ensureAuthenticated, currenciessController.searchCurrencies);
};
