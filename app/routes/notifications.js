'use strict';
const express = require('express'),
    notificationsController = require('../controllers/notifications.controller'),
    redisCache = require('../middleware/redisCache'),
    authHandler = require('../middleware/authHandler');

module.exports = function (app) {

    //get all user notifications
    app.get('/api/notifications', authHandler.ensureAuthenticated, notificationsController.getNotifications);
};