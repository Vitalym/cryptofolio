/**
 * Created by dev on 11.8.17.
 */
'use strict';
const express = require('express'),
    validateUnique = require('../middleware/validateUnique'),
    portfolioController = require('../controllers/portfolio.controller'),
    investmentsHandler = require('../middleware/investmentHandler'),
    authHandler = require('../middleware/authHandler');

module.exports = function (app) {

    //get all user holdings with allocation and rebalance information
    app.get('/api/holdingsAllocation', authHandler.ensureAuthenticated, portfolioController.getHoldingsAllocation, investmentsHandler.populateHoldingsAllocation);

    //add new allocation schema
    app.post('/api/investments/allocation', authHandler.ensureAuthenticated, portfolioController.setAssetAllocation, investmentsHandler.populateHoldingsAllocation);

};