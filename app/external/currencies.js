/**
 * Created by dev on 2.8.17.
 */
const request = require('request-promise-native'),
    express = require('express'),
    mongoose = require('mongoose'),
    async = require('async'),
    jsonfile = require('jsonfile'),
    moment = require('moment'),
    redis = require('../redis/redis'),
    Currency = mongoose.model('Currency'),
    BitcoinPrice = mongoose.model('BitcoinPrice');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-external-api.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

module.exports = {
    getCurrenciesList: () => {
        const options = {
            method: 'GET',
            uri: 'https://api.coinmarketcap.com/v1/ticker/'
        };

        let coinMarketCapResponse = [];
        let coinData = [];

        request(options)
            .then(response => {
                coinMarketCapResponse = JSON.parse(response);
                //store to redis
                redis.set('coinData', JSON.stringify(coinMarketCapResponse));
                //shitty redis pagination
                let arrayIndex = 0; //number of array to process
                let numberSorted = 50; //chunk size
                let coinDataChunk = [];
                let index = 0; //coin position in array
                async.each(coinMarketCapResponse, (coin, callback) => {
                    if (index < numberSorted) {
                        coinDataChunk.push(coin);
                    } else {
                        //chunk is full, we save it and go to next
                        redis.set(`coinData-${arrayIndex}`, JSON.stringify(coinDataChunk));
                        //clear the chunk array and update the index
                        numberSorted += 50;
                        coinDataChunk = [];
                        arrayIndex++;
                        //save item to new array
                        coinDataChunk.push(coin);
                    }
                    index++;
                    //if it's btc, set new today's price
                    callback();

                }, (err) => {
                    if (err) {
                        logger.error(err);
                    }
                    logger.info(`Currency list paginated`);
                    return Currency.remove({}).exec()
                });
                //return Currency.remove({})
            })
            .then(result => {
                logger.info(`Currency list cleared`);
                async.each(coinMarketCapResponse, (coin, callback) => {
                    let newCoin = new Currency();
                    newCoin.id = coin.id;
                    newCoin.name = coin.name;
                    newCoin.symbol = coin.symbol;
                    newCoin.rank = coin.rank;
                    newCoin.price_usd = coin.price_usd;
                    newCoin.price_btc = coin.price_btc;
                    newCoin.market_cap_usd = coin.market_cap_usd;
                    newCoin.available_supply = coin.available_supply;
                    newCoin.total_supply = coin.total_supply;
                    newCoin.percent_change_1h = coin.percent_change_1h;
                    newCoin.percent_change_24h = coin.percent_change_24h;
                    newCoin.percent_change_7d = coin.percent_change_7d;
                    newCoin.last_updated = coin.last_updated;

                    newCoin.save().then(result => {
                        coinData.push(result);
                        if (result.id === 'bitcoin') {
                            redis.set(`bitcoinPrice`, JSON.stringify(result.price_usd));
                        }
                        return callback();
                    })
                        .catch(err => {
                            console.log(err);
                            return callback();
                        });
                }, (err) => {
                    if (err) {
                        logger.error(err);
                    }
                    logger.info(`Currency list updated`);
                    return getCoinDataTimestamp().then(result => {
                        if (result > 24 || !result) {
                            return writeCoinDataToJson(coinMarketCapResponse);
                        }
                    });

                })
            })
            .catch(err => {
                logger.error(`Error while updating currencies list`, err);
                //TODO retry on timeout until successful, save error message
            })
    },

    updateCurrenciesList: () => {
        console.log('getting currencies...');
        const options = {
            method: 'GET',
            uri: 'https://api.coinmarketcap.com/v1/ticker/'
        };

        let coinMarketCapResponse = [];
        let coinData = [];
        console.log('getting currencies...');
        request(options)
            .then(response => {
                return new Promise((resolve, reject) => {
                    console.log('parsing response...')
                    coinMarketCapResponse = JSON.parse(response);
                    //store to redis
                    redis.set('coinData', JSON.stringify(coinMarketCapResponse));
                    //shitty redis pagination
                    let arrayIndex = 0; //number of array to process
                    let numberSorted = 50; //chunk size
                    let coinDataChunk = [];
                    let index = 0; //coin position in array
                    async.each(coinMarketCapResponse, (coin, callback) => {
                        if (index < numberSorted) {
                            coinDataChunk.push(coin);
                        } else {
                            //chunk is full, we save it and go to next
                            redis.set(`coinData-${arrayIndex}`, JSON.stringify(coinDataChunk));
                            //clear the chunk array and update the index
                            numberSorted += 50;
                            coinDataChunk = [];
                            arrayIndex++;
                            //save item to new array
                            coinDataChunk.push(coin);
                        }
                        index++;
                        //if it's btc, set new today's price
                        callback();

                    }, (err) => {
                        if (err) {
                            logger.error(err);
                            return resolve('done', err);
                        }
                        logger.info(`Currency list paginated`);
                        return resolve('done');
                    });
                })
            })
            .then(result => {
                logger.info(`Updating currency list ...`);
                async.each(coinMarketCapResponse, (coin, callback) => {
                    const update = {
                        $set: {
                            price_usd: coin.price_usd,
                            price_btc: coin.price_btc,
                            market_cap_usd: coin.market_cap_usd,
                            available_supply: coin.available_supply,
                            total_supply: coin.total_supply,
                            percent_change_1h: coin.percent_change_1h,
                            percent_change_24h: coin.percent_change_24h,
                            percent_change_7d: coin.percent_change_7d,
                            last_updated: coin.last_updated,
                        }
                    };
                    Currency.findOneAndUpdate({id: coin.id}, update, {upsert: true, new: true}).then(result => {
                        coinData.push(result);
                        if (result.id === 'bitcoin') {
                            redis.set(`bitcoinPrice`, JSON.stringify(result.price_usd));
                        }
                        return callback();
                    })
                        .catch(err => {
                            console.log(err);
                            return callback();
                        });
                }, (err) => {
                    if (err) {
                        logger.error(err);
                    }
                    logger.info(`Currency list updated`);
                    return getCoinDataTimestamp().then(result => {
                        if (result > 24 || !result) {
                            return writeCoinDataToJson(coinMarketCapResponse);
                        }
                    });

                })
            })
            .catch(err => {
                logger.error(`Error while updating currencies list`, err);
                //TODO retry on timeout until successful, save error message
            })
    },

    getBitcoinHistory: () => {
        //populate coin history 
        let options = {
            method: `GET`,
            uri: `http://www.coincap.io/history/BTC`
        };

        let history;

        request(options)
            .then(response => {
                history = JSON.parse(response);
                return BitcoinPrice.remove({}).exec()
            })
            .then(result => {
                async.each(history.price, (item, callback) => {
                    //write each price item to db
                    let newPriceEntry = new BitcoinPrice();
                    let date = moment(item[0]);
                    newPriceEntry.date = new Date(item[0]);
                    newPriceEntry.formattedDate = date.startOf('day');
                    newPriceEntry.price = item[1];

                    newPriceEntry.save().then(entry => {
                        callback();
                    })

                }, (error, result) => {
                    if (error) {
                        logger.error(`Error while fetching history`, err);
                        return;
                    }
                    logger.info(`btc price updated`, err);

                });

            })
            .catch(err => {
                logger.error(`Error while fetching history`, err);
                return;
            })
    },

    writeBitcoinPrice: (price) => {
        //write actual btc price to db in the end of a day
        //check if it's already written
        let today = moment();
        let formattedDate = today.startOf('day');
        BitcoinPrice.findOne({formattedDate: formattedDate}).exec()
            .then(result => {
                if (!result) {
                    //if not found, we save it
                    let newPriceEntry = new BitcoinPrice();
                    let date = moment();
                    newPriceEntry.date = new Date();
                    newPriceEntry.formattedDate = date.startOf('day');
                    newPriceEntry.price = price;

                    newPriceEntry.save().then(entry => {
                        logger.info('btc price updated');
                    })
                        .catch(err => {
                            logger.error(`Error while updating btc price`, err);
                        })
                } else {
                    //there is a save result, so we update it
                    const update = {
                        $set: {
                            price: price
                        }
                    };
                    BitcoinPrice.findOneAndUpdate({formattedDate: formattedDate}, update).exec()
                        .then(result => {
                            logger.info('btc price updated');
                        })
                        .catch(err => {
                            logger.error(`Error while updating btc price`, err);
                        })
                }

            })
            .catch(err => {
                logger.error(err);
            })


    }
};

function getCoinDataTimestamp() {
    return new Promise((resolve, reject) => {
        redis.get('coinTimestamp', (err, timestamp) => {
            if (err) {
                return reject(err);
            }
            let now = moment(new Date()); //todays date
            let end = moment(timestamp); // another date
            let duration = moment.duration(now.diff(end));
            return resolve(duration.asHours());

        });
    })
}

function writeCoinDataToJson(data) {
    return new Promise((resolve, reject) => {
        async.map(data, (coin, callback) => {
            process.nextTick(() => {
                let newCoin = {
                    value: {
                        id: coin.id,
                        symbol: coin.symbol,
                        name: coin.name
                    },

                    key: coin.id,
                    label: `${coin.name} (${coin.symbol})`
                };
                return callback(null, newCoin);
            })
        }, (error, result) => {
            if (error) {
                return reject(error)
            }
            //create json object
            let timestamp = new Date().toJSON();
            console.log(timestamp);
            redis.set('coinTimestamp', timestamp);
            redis.set('coinNames', JSON.stringify(result));
            logger.info(`coin names updated`);

            return resolve('done');

        });
    });
}