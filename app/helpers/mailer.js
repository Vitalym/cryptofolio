/**
 * Created by Виталий on 24.02.2017.
 */
'use strict';
const nodemailer = require('nodemailer'),
  aws = require('aws-sdk'),
  EmailTemplates = require('swig-email-templates'),
  htmlToText = require('html-to-text'),
  Promise = require("bluebird"),
  constants = require('./constants'),
  config = require('../../config/config');

//we use aws ses for now
aws.config.loadFromPath(require('path').join(__dirname, '../../config/awsConfig.json'));

const URL = config.url;

//winston setup
const winston = require('winston');
const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      colorize: true,
      timestamp: tsFormat
    })
  ]
});
logger.level = 'debug';

let renderTemplate = (template, options) => {
  let templates = new EmailTemplates({
    root: './emailTemplates',
    filters: {
      upper: function (str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
      },
      trim: function (str) {
        return str.substring(0, 256);
      }
    }
  });

  return new Promise((resolve, reject) => {
    return templates.render(template, options, function (err, html, text, subject) {
      if (err) {
        logger.debug(err);
        return reject(err);
      }

      let result = {
        html: html,
        text: text,
        subject: subject
      };
      return resolve(result);
    });
  })
};

let sendMail = (to, subject, html, text) => {
  let transporter = nodemailer.createTransport({
    SES: new aws.SES({
      apiVersion: '2010-12-01'
    })
  });
  let toAddress = process.env.NODE_ENV === 'production' ? to : 'vitaliy.maltsev@gmail.com'; //send on dev to me
  let fromAddress = 'vitaliy.maltsev@gmail.com'; // sent only to me for debugging reasons TODO REMOVE
  return new Promise((resolve, reject) => {
    return transporter.sendMail({
      from: fromAddress,
      to: toAddress, // sent only to me for debugging reasons TODO REMOVE
      subject: subject,
      text: text,
      html: html
    }).then(info => {
      return resolve(info);
    })
      .catch(err => {
        return reject(err);
      });
  });
};

module.exports = {
  sendVerificationEmail: (user) => {
    let options = {
      user: user,
      verificationLink: `${URL}/verify/${user.token}`
    };

    return renderTemplate('verificationEmail.html', options)
      .then(result => {
        return sendMail(
          user.email,
          'Please verify your Metida account',
          result.html,
          result.text
        );
      })
      .catch(err => {
        return err;
      });
  },

  sendResetEmail: (user) => {

    let options = {
      user: user,
      resetLink: `${URL}/reset/${user.token}`
    };

    return renderTemplate('resetEmail.html', options) //todo change template
      .then(result => {
        return sendMail(
          user.email,
         `You have requested to reset your Metida password`,
          result.html,
          result.text
        );
      })
      .catch(err => {
        return err;
      });
  },

  sendInvitationEmail: (user) => {
    let options = {
      user: user,
      userLink: `${URL}/users/${user.userId}`,
      verificationLink: `${URL}/verify/${user.token}`
    };

    return renderTemplate('invitationEmail.html', options)
      .then(result => {
        return sendMail(
          user.email,
          `You've been invited to join Metida ${user.team} team!`,
          result.html,
          result.text
        );
      })
      .catch(err => {
        return err;
      });
  },

  sendInvitationEmailNoVerify: (user) => {
    let options = {
      user: user,
      userLink: `${URL}/users/${user.userId}`,
      link: `${URL}/teams/select`
    };

    return renderTemplate('invitationEmailNoVerify.html', options) //todo change template
      .then(result => {
        return sendMail(
          user.email,
          `You've been invited to join Metida ${user.team} team!`,
          result.html,
          result.text
        );
      })
      .catch(err => {
        return err;
      });
  },

  sendPasswordResetEmail: (user) => {
    let options = {
      user: user
    };

    return renderTemplate('passwordResetSuccess.html', options) //todo change template
      .then(result => {
        return sendMail(
          user.email,
          `Your password has been changed!`,
          result.html,
          result.text
        );
      })
      .catch(err => {
        return err;
      });
  },

  sendNewEntryEmail: (data) => {
    //let userUrl = constants.BASE_URL + '/users/' + data.user._id;
    let options = {
      user: data,
      team: data.entry._team.name,
      username: `${data.firstName.capitalizeFirstLetter()}`,
      authorName: `${data.entry._author.firstName.capitalizeFirstLetter()} ${data.entry._author.lastName.capitalizeFirstLetter()}`,
      authorUrl: `${URL}/users/${data.entry._author._id}`,
      groupText: data.entry.userGroup ? `in` : '',
      groupName: data.entry.userGroup ? `${data.entry.userGroup.formattedName.capitalizeFirstLetter()}` : '',
      groupUrl: data.entry.userGroup ? `${URL}/${data.entry.userGroup.name}/entries` : '',
      entryUrl: `${URL}/entries/${data.entry._id}`,
      subject: data.entry.subject
    };

    let subjectGroupText = data.entry.userGroup ? `in ${data.entry.userGroup.formattedName}` : '';

    return renderTemplate('newEntry.html', options)
      .then(result => {
        return sendMail(
          user.email,
          `New entry ${subjectGroupText}`,
          result.html,
          result.text
        );
      })
      .catch(err => {
        return err;
      });
  },

  sendNewEntryWithAnswerEmail: (data) => {
    let answer = htmlToText.fromString(data.entry.answer.text, {
      ignoreImage: true,
      uppercaseHeadings: false,
      wordwrap: false
    });
    let options = {
      user: data,
      team: data.entry._team.name,
      username: `${data.firstName.capitalizeFirstLetter()}`,
      authorName: `${data.entry._author.firstName.capitalizeFirstLetter()} ${data.entry._author.lastName.capitalizeFirstLetter()}`,
      authorUrl: `${URL}/users/${data.entry._author._id}`,
      groupText: data.entry.userGroup ? `in` : '',
      groupName: data.entry.userGroup ? `${data.entry.userGroup.formattedName.capitalizeFirstLetter()}` : '',
      groupUrl: data.entry.userGroup ? `${URL}/${data.entry.userGroup.name}/entries` : '',
      entryUrl: `${URL}/entries/${data.entry._id}`,
      subject: data.entry.subject,
      answer: answer,
      readMore: data.entry.answer.text.length > 257 ? '    read more' : ''
    };

    let subjectGroupText = data.entry.userGroup ? `in ${data.entry.userGroup.formattedName}` : '';

    return renderTemplate('newEntryWithAnswer.html', options)
      .then(result => {
        return sendMail(
          user.email,
          `New entry ${subjectGroupText}`,
          result.html,
          result.text
        );
      })
      .catch(err => {
        return err;
      });
  },

  newAnswerForMyEntryEmail: (data) => {
    let answer = htmlToText.fromString(data.entry.answer.text, {
      ignoreImage: true,
      uppercaseHeadings: false,
      wordwrap: false
    });
    let options = {
      user: data,
      team: data.entry._team.name,
      username: `${data.firstName.capitalizeFirstLetter()}`,
      authorName: `${data.entry.answer.user.firstName.capitalizeFirstLetter()} ${data.entry.answer.user.lastName.capitalizeFirstLetter()}`,
      authorUrl: `${URL}/users/${data.entry._author._id}`,
      groupText: data.entry.userGroup ? `in` : '',
      groupName: data.entry.userGroup ? `${data.entry.userGroup.formattedName.capitalizeFirstLetter()}` : '',
      groupUrl: data.entry.userGroup ? `${URL}/${data.entry.userGroup.name}/entries` : '',
      entryUrl: `${URL}/entries/${data.entry._id}`,
      subject: data.entry.subject,
      answer: answer,
      readMore: data.entry.answer.text.length > 257 ? '    read more' : ''
    };

    return renderTemplate('answerForMyEntry.html', options)
      .then(result => {
        return sendMail(
          user.email,
          `${options.authorName} has just added an answer to the entry you've created`,
          result.html,
          result.text
        );
      })
      .catch(err => {
        return err;
      });
  },

  newAnswer: (data) => {
    let answer = htmlToText.fromString(data.entry.answer.text, {
      ignoreImage: true,
      uppercaseHeadings: false,
      wordwrap: false
    });
    let options = {
      user: data,
      team: data.entry._team.name,
      username: `${data.firstName.capitalizeFirstLetter()}`,
      authorName: `${data.entry.answer.user.firstName.capitalizeFirstLetter()} ${data.entry.answer.user.lastName.capitalizeFirstLetter()}`,
      authorUrl: `${URL}/users/${data.entry.answer.user._id}`,
      groupText: data.entry.userGroup ? `in` : '',
      groupName: data.entry.userGroup ? `${data.entry.userGroup.formattedName.capitalizeFirstLetter()}` : '',
      groupUrl: data.entry.userGroup ? `${URL}/${data.entry.userGroup.name}/entries` : '',
      entryUrl: `${URL}/entries/${data.entry._id}`,
      subject: data.entry.subject,
      answer: answer,
      readMore: data.entry.answer.text.length > 257 ? '    read more' : ''
    };

    return renderTemplate('newAnswer.html', options)
      .then(result => {
        return sendMail(
          user.email,
          `${options.authorName} has just added an new answer`,
          result.html,
          result.text
        );
      })
      .catch(err => {
        return err;
      });
  },

  sendNewVerificationEmail: (data) => {
    let answer = htmlToText.fromString(data.entry.answer.text, {
      ignoreImage: true,
      uppercaseHeadings: false,
      wordwrap: false
    });
    let options = {
      user: data,
      team: data.entry._team.name,
      username: `${data.firstName.capitalizeFirstLetter()}`,
      authorName: `${data.entry.verified.by.firstName.capitalizeFirstLetter()} ${data.entry.verified.by.lastName.capitalizeFirstLetter()}`,
      authorUrl: `${URL}/users/${data.entry.verified.by._id}`,
      groupText: data.entry.userGroup ? `in` : '',
      groupName: data.entry.userGroup ? `${data.entry.userGroup.formattedName.capitalizeFirstLetter()}` : '',
      groupUrl: data.entry.userGroup ? `${URL}/${data.entry.userGroup.name}/entries` : '',
      entryUrl: `${URL}/entries/${data.entry._id}`,
      subject: data.entry.subject,
      answer: answer,
      readMore: data.entry.answer.text.length > 257 ? '    read more' : ''
    };

    return renderTemplate('entryVerification.html', options)
      .then(result => {
        return sendMail(
          user.email,
          `${options.authorName} verified your answer`,
          result.html,
          result.text
        );
      })
      .catch(err => {
        return err;
      });
  }
};
