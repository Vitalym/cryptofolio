/**
 * Created by decipher on 2.3.17.
 */
"use strict";
module.exports = {
  authFailed: (message) => {
    let errorMessage = message || 'Username or password is incorrect';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = 404;
    error.name = 'AuthFailed';
    return error;
  },

  invalidField: (message) => {
    let errorMessage = message || 'One or more fields are invalid';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = 400;
    error.name = 'InvalidField';
    return error;
  },

  invalidId: (message) => {
    let errorMessage = message || 'Id is invalid';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = 400;
    error.name = 'InvalidId';
    return error;
  },

  invalidToken: (message) => {
    let errorMessage = message || 'This verification token is invalid';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = 400;
    error.name = 'InvalidToken';
    return error;
  },

  missingParam: (message) => {
    let errorMessage = message || 'Required param (s) missing';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = 400;
    error.name = 'MissingParam';
    return error;
  },

  noActiveTeam: (message) => {
    let errorMessage = message || 'No active team';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = 401;
    error.name = 'NoTeam';
    return error;
  },

  notAllowed: (message) => {
    let errorMessage = message || 'Not allowed';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = 403;
    error.name = 'NotAllowed';
    return error;
  },

  notFound: (message) => {
    console.log('throwing 404');
    let errorMessage = message || 'Not found';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = 404;
    error.name = 'NotFound';
    return error;
  },

  notVerified: (message) => {
    let errorMessage = message || 'This email is not yet verified. Please check your email';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = 403;
    error.name = 'NotVerified';
    return error;
  },

  unauthorized: (message) => {
    let errorMessage = message || 'Please make sure your request has an Authorization header';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = 401;
    error.name = 'UnauthorizedError';
    return error;
  },

  unique: (message) => {
    let error = new Error(message);
    error.message = message;
    error.status = 403;
    error.name = 'Unique';
    return error;
  },

  customError: (message, status, name) => {
    let errorMessage = message || 'Something went wrong';
    let error = new Error(errorMessage);
    error.message = errorMessage;
    error.status = status;
    error.name = name;
    return error;
  }
};
