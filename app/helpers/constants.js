/**
 * Created by Виталий on 02.02.2017.
 */
module.exports = {

    APP_URL: 'http://localhost:3000/',

    ENTRIES_URL: 'http://localhost:3000/entries/entry/',

    USER_POPULATION: 'currentPortfolio',

    INVESTMENT_POPULATION: 'user',

    NOTIFICATION_POPULATION: 'alert',

    BASE_URL: process.env.NODE_ENV == 'production' ? '' : 'http://localhost:3000/' //TODO prod url here once it's done
};
