/**
 * Created by Виталий on 02.02.2017.
 */
'use strict';
const mongoose = require('mongoose'),
  ObjectId = mongoose.Types.ObjectId,
  async = require('async'),
  crypto = require('crypto');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      colorize: true,
      timestamp: tsFormat,
      level: 'debug'
    }),
    new (require('winston-daily-rotate-file'))({
      filename: `${logDir}/-helpers.log`,
      timestamp: tsFormat,
      datePattern: 'yyyy-MM-dd',
      prepend: true,
      level: env === 'development' ? 'debug' : 'info'
    })
  ]
});


const colorPalette = [
  "rgb(255, 204, 204)", "rgb(255, 230, 204)", "rgb(255, 255, 204)", "rgb(204, 255, 204)", "rgb(204, 255, 230)", "rgb(204, 255, 255)", "rgb(204, 230, 255)", "rgb(204, 204, 255)", "rgb(230, 204, 255)",
  "rgb(255, 153, 153)", "rgb(255, 204, 153)", "rgb(255, 255, 153)", "rgb(153, 255, 153)", "rgb(153, 255, 204)", "rgb(153, 255, 255)", "rgb(153, 204, 255)", "rgb(153, 153, 255)", "rgb(204, 153, 255)",
  "rgb(255, 102, 102)", "rgb(255, 179, 102)", "rgb(255, 255, 102)", "rgb(102, 255, 102)", "rgb(102, 255, 179)", "rgb(102, 255, 255)", "rgb(102, 179, 255)", "rgb(102, 102, 255)", "rgb(179, 102, 255)",
  "rgb(255, 51, 51)", "rgb(255, 153, 51)", "rgb(255, 255, 51)", "rgb(51, 255, 51)", "rgb(51, 255, 153)", "rgb(51, 255, 255)", "rgb(51, 153, 255)", "rgb(51, 51, 255)", "rgb(153, 51, 255)",
  "rgb(255, 0, 0)", "rgb(255, 128, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 128)", "rgb(0, 255, 255)", "rgb(0, 128, 255)", "rgb(0, 0, 255)", "rgb(128, 0, 255)",
  "rgb(245, 0, 0)", "rgb(245, 123, 0)", "rgb(245, 245, 0)", "rgb(0, 245, 0)", "rgb(0, 245, 123)", "rgb(0, 245, 245)", "rgb(0, 123, 245)", "rgb(0, 0, 245)", "rgb(123, 0, 245)",
  "rgb(214, 0, 0)", "rgb(214, 108, 0)", "rgb(214, 214, 0)", "rgb(0, 214, 0)", "rgb(0, 214, 108)", "rgb(0, 214, 214)", "rgb(0, 108, 214)", "rgb(0, 0, 214)", "rgb(108, 0, 214)",
  "rgb(163, 0, 0)", "rgb(163, 82, 0)", "rgb(163, 163, 0)", "rgb(0, 163, 0)", "rgb(0, 163, 82)", "rgb(0, 163, 163)", "rgb(0, 82, 163)", "rgb(0, 0, 163)", "rgb(82, 0, 163)",
  "rgb(92, 0, 0)", "rgb(92, 46, 0)", "rgb(92, 92, 0)", "rgb(0, 92, 0)", "rgb(0, 92, 46)", "rgb(0, 92, 92)", "rgb(0, 46, 92)", "rgb(0, 0, 92)", "rgb(46, 0, 92)",
  "rgb(255, 255, 255)", "rgb(205, 205, 205)", "rgb(178, 178, 178)", "rgb(153, 153, 153)", "rgb(127, 127, 127)", "rgb(102, 102, 102)", "rgb(76, 76, 76)", "rgb(51, 51, 51)", "rgb(25, 25, 25)"
];

module.exports = {
  generateVerificationToken: () => {
    let buf = crypto.randomBytes(48);
    return buf.toString('hex');
  },

  validateId: (id) => {
    return ObjectId.isValid(id);
  },

  //filter entries by permissions
  filterEntriesByPermissions: (entries, user) => {
    let filteredEntries = [];


    if (entries.length) {
      filteredEntries = entries.filter((entry) => {
        if (!entry.userGroup) {
          //if no usergroup set
          return entry;
        } else if (entry.userGroup && !entry.userGroup.private) {
          //entry is public
          return entry;
        } else if (entry.userGroup && entry.userGroup.private) {
          //for(group in user.userGroups)
          if (user.userGroups.length) {
            for (let group of user.userGroups) {//TODO change to async or lodash !!!
              if (group._id == entry.userGroup._id) {
                return entry;
              }
            }
            return entry;
          }
        }
      });
    }


    return filteredEntries;
  },

  //build query for entries based on filters
  buildQuery: (body, team, user) => {
    return new Promise((resolve, reject) => {
      let query = {
        $and: [
          {_team: team}
        ]
      };

      if (body && body.state) {
        if (body.state.status) {
          if (body.state.status === 'all') {
            query.$and.push(
              {archived: false},
              {isInvalid: false}
            );
          } else if (body.state.status === 'answered') {
            query.$and.push(
              {answer: {$ne: null}},
              {archived: false},
              {isInvalid: false}
            );
          } else if (body.state.status === 'unanswered') {
            query.$and.push(
              {answer: null},
              {archived: false},
              {isInvalid: false}
            );
          } else if (body.state.status === 'archived') {
            query.$and.push(
              {archived: true}
            );
          } else if (body.state.status === 'invalid') {
            query.$and.push(
              {isInvalid: true},
              {archived: false}
            );
          } else if (body.state.status === 'verified') {
            query.$and.push(
              {'verified.status': true},
              {archived: false}
            );
          } else if (body.state.status === 'reported') {
            query.$and.push(
              {markedAsInvalidBy: {$gt: []}},
              {archived: false}
            );
          }
        } else {
          query.$and.push(
            {archived: false},
            {isInvalid: false}
          );
        }
        if (body.state.group) {
         if (ObjectId.isValid(body.state.group._id)) {
            query.$and.push(
              {userGroup: body.state.group._id},
              {userGroup: {$in: user.userGroups}} //check that user is a member of a group
            );
          } else {
            query.$and.push(
              {userGroup: {$in: user.userGroups}}
            );
          }
        }
      }
      return resolve(query);
    });
  },

  //light version of query, takes only usergroup
  buildGroupQuery: (body, team, user, status) => {
    return new Promise((resolve, reject) => {
      let query = {
        $and: [
          {_team: team}
        ]
      };

      if (body.state.group) {
        if (ObjectId.isValid(body.state.group._id)) {
          query.$and.push(
            {userGroup: body.state.group._id},
            {userGroup: {$in: user.userGroups}} //check that user is a member of a group
          );
        } else {
          query.$and.push(
            {userGroup: {$in: user.userGroups}}
          );
        }
      }

      if (status === 'answered'){
        query.$and.push(
          {answer: {$ne: null}},
          {archived: false},
          {isInvalid: false}
        );
      } else if (status === 'unanswered'){
        query.$and.push(
          {answer: null},
          {archived: false},
          {isInvalid: false}
        );
      } else if (status === 'invalid') {
        query.$and.push(
          {isInvalid: true},
          {archived: false}
        );
      } else if (status === 'archived') {
        query.$and.push(
          {archived: true}
        );
      } else if (status === 'verified') {
        query.$and.push(
          {'verified.status': true},
          {archived: false}
        );
      } else if (status === 'reported') {
        query.$and.push(
          {markedAsInvalidBy: {$gt: []}},
          {archived: false}
        );
      }
      return resolve(query);
    });
  },

  //build query for users based on selected group
  buildUserQuery: (body, team) => {
    console.log(body);
    let query = {
      $and: [
        {_team: team}
      ]
    };

    if (body.state) {
      if (body.state.group) {
        //we return all users unless the group is private
        if (body.state.group !== 'uncategorized' || body.state.group !== 'all') {
          if (body.state.group.private) {
            query.$and.push(
              {userGroups: body.state.group._id}
            );
          }
        }
      }
    }
    return query;
  },

  getRandomColor () {
    return colorPalette[Math.floor(Math.random() * colorPalette.length)];
  },

  //map entries with attachment to contain attachments only
  mapAttachments (files, origin) {
    return new Promise((resolve, reject) => {
      if (origin == 'entry') {
        return resolve(files);
      } else {
        return async.map(files, (file, next) => {
          // continue
          return next(false, file.attachments);
        }, (err, results) => {
          if (err) {
            logger.err(err);
            return reject(err);
          }
          let flattenedResults = [].concat.apply([], results);
          return resolve(flattenedResults);
          // results is now an array of attachments
        });
      }
    });
  }
};
