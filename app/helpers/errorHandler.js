/**
 * Created by decipher on 28.2.17.
 */
//winston setup
const winston = require('winston');
const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      colorize: true,
      timestamp: tsFormat
    })
  ]
});
logger.level = 'debug';

module.exports = {
  sendDefaultErrorMessage: (err, res) => {
    logger.error(err);
    logger.error(err.message);
    return res.status(500).send({
      success: false,
      error: err,
      message: 'Something went wrong'
    });
    /*return next(err);*/
  }
};
