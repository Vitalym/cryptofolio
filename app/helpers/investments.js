/**
 * Created by Виталий on 04.08.2017.
 */
const mongoose = require('mongoose'),
    async = require('async'),
    _ = require('lodash'),
    request = require('request-promise-native'),
    Investment = mongoose.model('Investment'),
    Currency = mongoose.model('Currency'),
    errorFactory = require('../helpers/errorFactory'),
    errorHandler = require('../helpers/errorHandler');

//winston setup
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = path.join(__dirname, '../../log');
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = new (winston.Logger)({
    transports: [
        // colorize the output to the console
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat,
            level: 'debug'
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-investment-helpers.log`,
            timestamp: tsFormat,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level: env === 'development' ? 'debug' : 'info'
        })
    ]
});

module.exports = {
    convertPriceToPerItem: (price, priceType, amount) => {
        //convert asset price to price per unit
        if (priceType === 'per coin') {
            return price;
        } else if (priceType === 'total') {
            return price / amount
        }
    },

    populateInvestmentsHistory: (investments) => {
        let hrstart = process.hrtime();
        return new Promise((resolve, reject) => {
            async.map(investments,
                (investment, callback) => {
                    process.nextTick(() => {
                        Currency.findOne({id: investment.coin.id})
                            .exec()
                            .then(currency => {
                                if(!currency || !currency.price_usd){
                                    console.log(investment);
                                }
                                let transformedInvestment = Object.assign(investment, {
                                    //TODO transform whatever to usd here
                                    usdAcquisitionCost: investment.amount * investment.usdPricePerItem,
                                    btcAcquisitionCost: investment.amount * investment.btcPricePerItem,
                                    totalTransactionUsdValue: investment.amount * currency.price_usd,
                                    totalTransactionBtcValue: investment.amount * currency.price_btc,
                                    currentPriceBtc: currency.price_btc,
                                    currentPriceUsd: currency.price_usd,
                                    usd_profit_percentage: (((investment.amount * currency.price_usd) - (investment.amount * investment.usdPricePerItem)) / (investment.amount * investment.usdPricePerItem) * 100) || 0,
                                    btc_profit_percentage: (((investment.amount * currency.price_btc) - (investment.amount * investment.btcPricePerItem)) / (investment.amount * investment.btcPricePerItem) * 100) || 0,
                                    profit_usd: ((investment.amount * currency.price_usd) - (investment.amount * investment.usdPricePerItem)),
                                    profit_btc: ((investment.amount * currency.price_btc) - (investment.amount * investment.btcPricePerItem)),
                                    day_change: currency.percent_change_24h
                                });
                                return callback(null, transformedInvestment);
                            })
                            .catch(err => {
                                logger.error(err);
                                return callback(null, investment);
                                //return next(err)
                            });
                    })
                },
                (error, results) => {
                    if (error) {
                        logger.error(err);
                        return reject(error);
                    }
                    let hrend = process.hrtime(hrstart);
                    logger.info("Intermediate execution time 'populateInvestmentsHistory' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                    return resolve(results);
                });
        });
    },

    populateTransactionHistory: (transactions) => {
        let hrstart = process.hrtime();
        return new Promise((resolve, reject) => {
            async.map(transactions,
                (transaction, callback) => {
                    process.nextTick(() => {
                        if (transaction.action === 'sell') {
                            return callback(null, transaction);
                        }
                        Currency.findOne({id: transaction.coin.id})
                            .exec()
                            .then(currency => {

                                let transformedTransaction = Object.assign(transaction, {
                                    //TODO transform whatever to usd here
                                    usdAcquisitionCost: transaction.amount * transaction.usdPricePerItem,
                                    btcAcquisitionCost: transaction.amount * transaction.btcPricePerItem,
                                    totalTransactionUsdValue: transaction.amount * currency.price_usd,
                                    totalTransactionBtcValue: transaction.amount * currency.price_btc,
                                    currentPriceBtc: currency.price_btc,
                                    currentPriceUsd: currency.price_usd,
                                    usd_profit_percentage: (((transaction.amount * currency.price_usd) - (transaction.amount * transaction.usdPricePerItem)) / (transaction.amount * transaction.usdPricePerItem) * 100) || 0,
                                    btc_profit_percentage: (((transaction.amount * currency.price_btc) - (transaction.amount * transaction.btcPricePerItem)) / (transaction.amount * transaction.btcPricePerItem) * 100) || 0,
                                    profit_usd: ((transaction.amount * currency.price_usd) - (transaction.amount * transaction.usdPricePerItem)),
                                    profit_btc: ((transaction.amount * currency.price_btc) - (transaction.amount * transaction.btcPricePerItem)),
                                    day_change: currency.percent_change_24h
                                });
                                return callback(null, transformedTransaction);
                            })
                            .catch(err => {
                                logger.error(err);
                                return callback(null, transaction);
                            });
                    })
                },
                (error, results) => {
                    if (error) {
                        logger.error(error);
                        return reject(error);
                    }
                    let hrend = process.hrtime(hrstart);
                    logger.info("Intermediate execution time 'populateTransactionHistory' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                    return resolve(results);
                });
        });
    },

    mergeUserHoldings: (investments) => {
        //merge investments and calcualte average price
        let hrstart = process.hrtime();
        return new Promise((resolve, reject) => {
            let mergedInvestments = [];
            return async.each(investments, (investment, callback) => {
                    let existing = mergedInvestments.filter((v) => {
                        return v.coin.id === investment.coin.id;
                    });
                    if (existing.length) {
                        //it exists, so we merge
                        let investmentToMerge = existing[0];
                        let existingIndex = mergedInvestments.indexOf(investmentToMerge);
                        //transform the found element
                        let action = investment.action;
                        let totalAmount = (action === 'sell')
                            ? mergedInvestments[existingIndex].totalAmount - investment.amount
                            : mergedInvestments[existingIndex].totalAmount + investment.amount;
                        let itemsUsdAcquisitionCost = (action === 'sell')
                            ? mergedInvestments[existingIndex].itemsUsdAcquisitionCost
                            : mergedInvestments[existingIndex].itemsUsdAcquisitionCost + investment.usdAcquisitionCost;
                        let itemsBtcAcquisitionCost = (action === 'sell')
                            ? mergedInvestments[existingIndex].itemsBtcAcquisitionCost
                            : mergedInvestments[existingIndex].itemsBtcAcquisitionCost + investment.btcAcquisitionCost;
                        let totalBuyAmount = (action === 'sell') //use it to calculate average price, we don't extract sell amount here
                            ? mergedInvestments[existingIndex].totalBuyAmount
                            : mergedInvestments[existingIndex].totalBuyAmount + investment.amount;
                        mergedInvestments[existingIndex] = Object.assign(mergedInvestments[existingIndex], {
                            //sum the price TODO recalculate to same value
                            totalAmount: totalAmount,
                            totalBuyAmount: totalBuyAmount,
                            itemsUsdAcquisitionCost: itemsUsdAcquisitionCost,
                            itemsBtcAcquisitionCost: itemsBtcAcquisitionCost
                        });
                    } else {
                        //we don't check action here, because first in the row should be a buy transaction
                        let holding = Object.assign(investment, {
                            //sum the price TODO recalculate to same value
                            totalAmount: investment.amount,
                            totalBuyAmount: investment.amount,
                            itemsUsdAcquisitionCost: investment.usdAcquisitionCost,
                            itemsBtcAcquisitionCost: investment.btcAcquisitionCost
                        });
                        //it doesn't exist
                        mergedInvestments.push(holding); //todo change from push
                    }
                    callback();
                },
                (err) => {
                    if (err) {
                        return reject(err);
                    }

                    let hrend = process.hrtime(hrstart);
                    logger.info("Intermediate execution time 'mergeUserHoldings' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                    return resolve(mergedInvestments);
                });
        });
    },

    calculateUserHoldings: (investments) => {
        //merge investments and calcualte average price
        let hrstart = process.hrtime();
        return new Promise((resolve, reject) => {
            return async.map(investments, (investment, callback) => {
                    process.nextTick(() => {
                        let averageUsdAcquisitionCost = investment.itemsUsdAcquisitionCost / investment.totalBuyAmount || 0; //TODO test replaced totalAmount with totalBuyAmount
                        let averageBtcAcquisitionCost = investment.itemsBtcAcquisitionCost / investment.totalBuyAmount || 0;
                        let totalUsdSpent = parseFloat(investment.totalAmount) * averageUsdAcquisitionCost;
                        let totalBtcSpent = parseFloat(investment.totalAmount) * averageBtcAcquisitionCost;
                        let totalBtcValue = parseFloat(investment.totalAmount) * parseFloat(investment.currentPriceBtc);
                        let totalUsdValue = parseFloat(investment.totalAmount) * parseFloat(investment.currentPriceUsd);
                        let transformed = Object.assign(investment, {
                            //sum the price TODO recalculate to same value
                            totalUsdValue: totalUsdValue,
                            totalBtcValue: totalBtcValue,
                            totalUsdSpent: totalUsdSpent,
                            totalBtcSpent: totalBtcSpent,
                            averageUsdAcquisitionCost: averageUsdAcquisitionCost,
                            averageBtcAcquisitionCost: averageBtcAcquisitionCost,
                            totalUsdProfitPercentage: (totalUsdValue - totalUsdSpent) / totalUsdSpent * 100 || 0,
                            totalBtcProfitPercentage: (totalBtcValue - totalBtcSpent) / totalBtcSpent * 100 || 0,
                            totalProfitUSD: totalUsdValue - totalUsdSpent,
                            totalProfitBTC: totalBtcValue - totalBtcSpent
                        });
                        return callback(null, transformed);
                    });
                },
                (err) => {
                    if (err) {
                        return reject(err);
                    }

                    let hrend = process.hrtime(hrstart);
                    logger.info("Intermediate execution time 'calculateUserHoldings' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                    return resolve(investments);
                });
        });
    },

    removeZeroValueHoldings: (investments) => {
        //clear zero value holdings (whic were sold completely)
        let hrstart = process.hrtime();
        return new Promise((resolve, reject) => {
            return async.filter(investments, (investment, callback) => {
                        if (investment.totalAmount > 0){
                            return  callback(null, true);

                        } else {
                            return callback(false);
                        }

                },
                (err, results) => {
                    if (err) {
                        return reject(err);
                    }

                    let hrend = process.hrtime(hrstart);
                    logger.info("Intermediate execution time 'removeZeroValueHoldings' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                    return resolve(results);
                });
        });
    },

    mergeTransactionsValue: (transactions) => {
        //merge transactions and calcualte average price
        let hrstart = process.hrtime();
        return new Promise((resolve, reject) => {
            let mergedTransactions = [];

            return async.each(transactions, (transaction, callback) => {
                    let existing = mergedTransactions.filter((v) => {
                        return v.coin.id === transaction.coin.id;
                    });
                    if (existing.length) {
                        //it exists, so we merge
                        let investmentToMerge = existing[0];
                        let existingIndex = mergedTransactions.indexOf(investmentToMerge);
                        //transform the found element
                        let action = transaction.action;
                        let totalAmount = (action === 'sell')
                            ? mergedTransactions[existingIndex].totalAmount - transaction.amount
                            : mergedTransactions[existingIndex].totalAmount + transaction.amount;
                        let itemsUsdAcquisitionCost = (action === 'sell')
                            ? mergedTransactions[existingIndex].itemsUsdAcquisitionCost
                            : mergedTransactions[existingIndex].itemsUsdAcquisitionCost + transaction.usdAcquisitionCost;
                        let itemsBtcAcquisitionCost = (action === 'sell')
                            ? mergedTransactions[existingIndex].itemsBtcAcquisitionCost
                            : mergedTransactions[existingIndex].itemsBtcAcquisitionCost + transaction.btcAcquisitionCost;
                        let totalBuyAmount = (action === 'sell') //use it to calculate average price, we don't extract sell amount here
                            ? mergedTransactions[existingIndex].totalBuyAmount
                            : mergedTransactions[existingIndex].totalBuyAmount + transaction.amount;
                        mergedTransactions[existingIndex] = Object.assign(mergedTransactions[existingIndex], {
                            //sum the price TODO recalculate to same value
                            totalAmount: totalAmount,
                            totalBuyAmount: totalBuyAmount,
                            itemsUsdAcquisitionCost: itemsUsdAcquisitionCost,
                            itemsBtcAcquisitionCost: itemsBtcAcquisitionCost
                        });
                    } else {
                        //we don't check action here, because first in the row should be a buy transaction
                        let holding = Object.assign(transaction, {
                            //sum the price TODO recalculate to same value
                            totalAmount: transaction.amount,
                            totalBuyAmount: transaction.amount,
                            itemsUsdAcquisitionCost: transaction.usdAcquisitionCost,
                            itemsBtcAcquisitionCost: transaction.btcAcquisitionCost
                        });
                        //it doesn't exist
                        mergedTransactions.push(holding); //todo change from push
                    }
                    callback();
                },
                (err) => {
                    if (err) {
                        return reject(err);
                    }

                    let hrend = process.hrtime(hrstart);
                    logger.info("Intermediate execution time 'mergeTransactionsValue' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                    return resolve(mergedTransactions);
                });
        });
    },

    calculateTotalTransactionsValue: (transactions) => {
        //merge transactions and calcualte average price
        let hrstart = process.hrtime();
        return new Promise((resolve, reject) => {
            return async.map(transactions, (transaction, callback) => {
                    process.nextTick(() => {
                        let averageUsdAcquisitionCost = transaction.itemsUsdAcquisitionCost / transaction.totalBuyAmount || 0; //TODO test replaced totalAmount with totalBuyAmount
                        let averageBtcAcquisitionCost = transaction.itemsBtcAcquisitionCost / transaction.totalBuyAmount || 0;
                        let totalUsdSpent = parseFloat(transaction.totalAmount) * averageUsdAcquisitionCost;
                        let totalBtcSpent = parseFloat(transaction.totalAmount) * averageBtcAcquisitionCost;
                        let totalBtcValue = parseFloat(transaction.totalAmount) * parseFloat(transaction.currentPriceBtc);
                        let totalUsdValue = parseFloat(transaction.totalAmount) * parseFloat(transaction.currentPriceUsd);
                        let transformed = Object.assign(transaction, {
                            //sum the price TODO recalculate to same value
                            totalUsdValue: totalUsdValue,
                            totalBtcValue: totalBtcValue,
                            totalUsdSpent: totalUsdSpent,
                            totalBtcSpent: totalBtcSpent,
                            averageUsdAcquisitionCost: averageUsdAcquisitionCost,
                            averageBtcAcquisitionCost: averageBtcAcquisitionCost,
                            totalUsdProfitPercentage: (totalUsdValue - totalUsdSpent) / totalUsdSpent * 100 || 0,
                            totalBtcProfitPercentage: (totalBtcValue - totalBtcSpent) / totalBtcSpent * 100 || 0,
                            totalProfitUSD: totalUsdValue - totalUsdSpent,
                            totalProfitBTC: totalBtcValue - totalBtcSpent
                        });

                        return callback(null, transformed);
                    });
                },
                (err) => {
                    if (err) {
                        return reject(err);
                    }

                    let hrend = process.hrtime(hrstart);
                    logger.info("Intermediate execution time 'calculateTotalTransactionsValue' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                    return resolve(transactions);
                });
        });
    },

    calculateTotalValue: (investments) => {
        //get total portfolio value by sum of each holding value
        let hrstart = process.hrtime();
        return new Promise((resolve, reject) => {
            let totalUSDValue = 0,
                totalUSDSpent = 0,
                totalBTCSpent = 0,
                totalBTCValue = 0,
                totalETHValue = 0;
            return async.each(investments, (investment, callback) => {
                    if (investment.action === 'sell') {
                        return callback();
                    }
                    totalUSDValue += investment.totalUsdValue;
                    totalBTCValue += investment.totalBtcValue;
                    totalUSDSpent += investment.totalUsdSpent;
                    totalBTCSpent += investment.totalBtcSpent;
                    return callback();
                },
                (err) => {
                    if (err) {
                        return reject(err);
                    } else {
                        //calculate BTC and ETH price
                        //TODO replace with redis value
                        Currency.find(
                            {id: 'ethereum'}
                        ).exec()
                            .then(currencies => {
                                // profit: (((investment.amount * currency.price_usd) - (investment.amount * investment.price)) / (investment.amount * investment.price) * 100).toFixed(2),
                                let totalUSDProfit = totalUSDValue - totalUSDSpent;
                                let totalBTCProfit = totalBTCValue - totalBTCSpent;
                                let totalUsdProfitPercentage = (totalUSDValue - totalUSDSpent) / totalUSDSpent * 100;
                                let totalBtcProfitPercentage = (totalBTCValue - totalBTCSpent) / totalBTCSpent * 100;
                                currencies.forEach((currency) => { //TODO move to redis ?
                                    if (currency.id === 'ethereum') {
                                        totalETHValue = totalUSDValue / currency.price_usd
                                    }
                                });
                                let totalValue = {
                                    totalUSDValue,
                                    totalUSDSpent,
                                    totalBTCSpent,
                                    totalUSDProfit,
                                    totalBTCProfit,
                                    totalUsdProfitPercentage,
                                    totalBtcProfitPercentage,
                                    totalETHValue,
                                    totalBTCValue,
                                    currencies
                                };
                                let hrend = process.hrtime(hrstart);
                                logger.info("Intermediate execution time 'calculateTotalValue' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                                return resolve(totalValue);
                            })
                            .catch(err => {
                                logger.error(err);
                                return reject(err);
                                //return next(err)
                            });
                    }
                });
        });
    },

    calculateShare: (investments, totalValue) => {
        let hrstart = process.hrtime();
        return new Promise((resolve, reject) => {
            return async.each(investments, (investment, callback) => {
                    let share = investment.totalUsdValue / totalValue * 100;
                    Object.assign(investment, {
                        share: share
                    });
                    callback();
                },
                (err) => {
                    if (err) {
                        logger.error(err);
                        return reject(err);
                    }

                    let hrend = process.hrtime(hrstart);
                    logger.info("Intermediate execution time 'calculateShare' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                    return resolve(investments);
                })
        })
    },

    addTargetAllocation: (investments, targetAllocation) => {
        return new Promise((resolve, reject) => {
            return async.each(investments, (investment, callback) => {
                    let targetValue = _.filter(targetAllocation, ['coin', investment.coin.id]);
                    if (targetValue.length) {
                        Object.assign(investment, {
                            targetAllocation: +targetValue[0].value
                        });
                        callback();
                    } else {
                        Object.assign(investment, {
                            targetAllocation: 0
                        });
                        callback();
                    }
                },
                (err) => {
                    if (err) {
                        logger.error(err);
                        return reject(err);
                    }

                    return resolve(investments);
                })
        })
    },

    calculateAmountToBuy: (investments, totalUSDValue, totalBTCValue) => {
        //calculate amount to buy after rebalancing
        return new Promise((resolve, reject) => {
            return async.each(investments, (investment, callback) => {
                    if (investment.targetAllocation && !isNaN(investment.targetAllocation)) {
                        if (investment.targetAllocation > 0) {
                            let usdValueAfterRebalance = totalUSDValue * (investment.targetAllocation / 100);
                            let btcValueAfterRebalance = totalBTCValue * (investment.targetAllocation / 100);
                            let usdToBuy = usdValueAfterRebalance - investment.totalUsdValue;
                            let btcToBuy = btcValueAfterRebalance - investment.totalBtcValue;
                            let currentPrice = investment.currentPriceUsd;
                            Object.assign(investment, {
                                usdValueAfterRebalance: usdValueAfterRebalance,
                                btcValueAfterRebalance: btcValueAfterRebalance,
                                valueAfterRebalance: usdValueAfterRebalance / currentPrice,
                                usdToBuy: usdToBuy,
                                btcToBuy: btcToBuy,
                                toBuy: usdToBuy / currentPrice
                            });
                        } else {
                            Object.assign(investment, {
                                usdValueAfterRebalance: 0,
                                btcValueAfterRebalance: 0,
                                valueAfterRebalance: 0,
                                usdToBuy: 0 - investment.totalUsdValue,
                                btcToBuy: 0 - investment.totalBtcValue,
                                toBuy: 0 - (investment.totalUsdValue / currentPrice)
                            });
                        }
                    }

                    callback();
                },
                (err) => {
                    if (err) {
                        logger.error(err);
                        return reject(err);
                    }

                    return resolve(investments);
                })
        })
    },

    getCoinHistory: (coin) => {
        let hrstart = process.hrtime();
        const requestOptions = {
            method: 'GET',
            uri: `http://www.coincap.io/history/${coin}`
        };

        return new Promise((resolve, reject) => {
            return request(requestOptions)
                .then(response => {
                    let history = JSON.parse(response);
                    let hrend = process.hrtime(hrstart);
                    logger.info("Intermediate execution time 'getCoinHistory' (hr): %ds %dms", hrend[0], hrend[1]/1000000);
                    return resolve(history);
                })
                .catch(err => {
                    logger.error(err);
                    return reject(err)
                });
        })
    },

    calculateMaxSellAmount: (currency, amount, userId) => {
        return new Promise((resolve, reject) => {
            //first we need to calculate this assets
            Investment.find({
                $and: [
                    {user: userId},
                    {'coin.id': currency},
                    {action: 'buy'}
                ]
            }).exec()
                .then(transactions => {
                    if (!transactions.length) {
                        let error = errorFactory.notAllowed(`Amount you are trying to sell is bigger than your holdings`);
                        return reject(error);
                    }
                    let mergedTransactions = [];

                    return async.each(transactions, (transaction, callback) => {
                        let existing = mergedTransactions.filter((v) => {
                            return v.coin.id === transaction.coin.id;
                        });
                        if (existing.length) {
                            //it exists, so we merge
                            let transactionToMerge = existing[0];
                            let existingIndex = mergedTransactions.indexOf(transactionToMerge);
                            //transform the found element
                            let totalAmount = mergedTransactions[existingIndex].totalAmount + transaction.amount;

                            mergedTransactions[existingIndex] = Object.assign(mergedTransactions[existingIndex], {
                                //sum the amount
                                totalAmount: totalAmount
                            });
                        } else {
                            let holding = Object.assign(transaction, {
                                //sum the amount
                                totalAmount: transaction.amount
                            });
                            //it doesn't exist
                            mergedTransactions.push(holding);
                        }
                        callback(); //proceed to next
                    }, (err) => {
                        if (err) {
                            logger.error(err);
                            return reject(err);
                        }
                        if (!mergedTransactions.length || mergedTransactions[0].totalAmount < amount) {
                            let error = errorFactory.notAllowed(`Amount you are trying to sell is bigger than your holdings`);
                            return reject(error);
                        } else {
                            return resolve('ok');
                        }

                    });
                })
                .catch(err => {
                    logger.error(err);
                    return reject(err);
                });
        });
    }
};