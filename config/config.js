'use strict';

const path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

const config = {
    development: {
        root: rootPath,
        app: {
            name: 'coinoverwatch'
        },
        port: process.env.PORT || 8080,
        services: {
            currencies: {port: process.env.PORT || 8081}
        },
        db: 'mongodb://localhost/cryptofolio-development',
        secret: 'mySecretToken',
        url: 'http://localhost:3000',
        aws: {
            awsRegion: 'us-west-2',
            awsBucket: "qabot-test",
            awsKey: 'AKIAIIJZV2GWICMKV3IQ',
            awsSecret: "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"
        },
        facebook: {
            FACEBOOK_SECRET: 'e4236309400a276b5a6454b3236d96df',
            clientId: '802613709863946'
        }
    },

    test: {
        root: rootPath,
        app: {
            name: 'coinoverwatch'
        },
        port: process.env.PORT || 8000,
        services: {
            currencies: {port: process.env.PORT || 8081}
        },
        db: 'mongodb://localhost/cryptofolio--test',
        secret: 'mySecretToken',
        url: 'http://localhost:3000',
        aws: {
            awsRegion: 'us-west-2',
            awsBucket: "qabot-test",
            awsKey: 'AKIAIIJZV2GWICMKV3IQ',
            awsSecret: "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"
        }
    },

    production: {
        root: rootPath,
        app: {
            name: 'coinoverwatch'
        },
        port: process.env.PORT || 3000,
        //db: 'mongodb://admin:adminvitmal1991@ds121464.mlab.com:21464/heroku_90hqdx5n',
        db: process.env.MONGO_URL,
        secret: 'mySecretToken',
        url: '',
        aws: {
            awsRegion: 'us-west-2',
            awsBucket: "qabot-test",
            awsKey: 'AKIAIIJZV2GWICMKV3IQ',
            awsSecret: "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"
        }
    }
};

module.exports = config[env];
