'use strict';
const express = require('express');
const cors = require('cors');
const glob = require('glob');
const path = require('path');

const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const loggerMiddleware = require('../app/middleware/logger');
const errorHandler = require('../app/middleware/errorHandler');

//keen tracking
/*
const keenio = require('express-keenio');
keenio.configure({ client: {
    projectId: '59bc43c7c9e77c0001095d3b',
    writeKey: 'CCE644A261A8113D65A2A07A372C063735DA53A289E086442C8E71887E412A9ACCAD4358539253D09497EEACB42BAD9F1E01C73A679C5E197372BD1B65F78CDA919501ECA0BA50344959334142123D04484A0215E10F6821FAB5B7850EB06CCC'}
});
*/

module.exports = function (app, config) {
    const env = process.env.NODE_ENV || 'development';
    app.locals.ENV = env;
    app.locals.ENV_DEVELOPMENT = env == 'development';

    app.use(cors());

    console.log(config.root);

    app.set('views', config.root + '/app/views');
    // app.use(favicon(config.root + '/public/img/favicon.ico'));

    app.use(loggerMiddleware.logger);
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(cookieParser());
    app.use(compress());
    app.use(express.static(config.root + '/client'));
    app.use(methodOverride());

    app.set('secret', config.secret);

    //app.use(keenio.handleAll());

    const routes = glob.sync(config.root + '/app/routes/*.js');
    routes.forEach(route => {
        require(route)(app);
    });

    app.use(express.static(path.join(__dirname, '../client/build/')));
    app.use(express.static(path.join(__dirname, '../client/build/static')));

    app.get('/*', (req, res, next) => {
        res.sendFile(path.join(__dirname, '../client/build/index.html'));
    });

    /* app.get('/', (req, res) => {
       res.send('Hello world');
     });
   */
    /*app.get('/!*', (req, res) => {
      res.sendfile('./views/index.html');
    });*/

    app.use((req, res, next) => {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    /*if (app.get('env') === 'development') {
      app.use(errorHandler.handleError);
    }*/

    app.use(errorHandler.handleError);

    return app;
};
